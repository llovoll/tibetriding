package cn.ctibet.tibetriding.view;

import android.content.Context;
import android.util.AttributeSet;
import android.util.Log;
import android.view.MotionEvent;
import android.view.View;
import android.widget.ListView;
import cn.ctibet.tibetriding.bean.HistoryItemBean;

public class ListViewCompat extends ListView {

    private static final String TAG = "ListViewCompat";

    private SlideView mFocusedItemView;
    private int x1;
    private int y1;
    private int x2;
    private int y2;

    public ListViewCompat(Context context) {
        super(context);
    }

    public ListViewCompat(Context context, AttributeSet attrs) {
        super(context, attrs);
    }

    public ListViewCompat(Context context, AttributeSet attrs, int defStyle) {
        super(context, attrs, defStyle);
    }

    public void shrinkListItem(int position) {
        View item = getChildAt(position);

        if (item != null) {
            try {
                ((SlideView) item).shrink();
            } catch (ClassCastException e) {
                e.printStackTrace();
            }
        }
    }

    @Override
    public boolean onTouchEvent(MotionEvent event) {
        switch (event.getAction()) {
            case MotionEvent.ACTION_DOWN: {
                x1 = (int) event.getX();
                y1 = (int) event.getY();
                int position = pointToPosition(x1, y1);
                Log.e(TAG, "postion=" + position);
                if (position != INVALID_POSITION) {
                    HistoryItemBean data = (HistoryItemBean) getItemAtPosition(position);
                    mFocusedItemView = data.slideView;
                    mFocusedItemView.pos=position;
                    Log.e(TAG, "FocusedItemView=" + mFocusedItemView);
                }
            }
         /*   case MotionEvent.ACTION_UP:
                x2 = (int) event.getX();
                y2 = (int) event.getY();
                if (x1 == x2 && y1 == y2) {
                    System.out.println("===============oooo==============");
                    int position = pointToPosition(x2, y2);
                    listener.itemClick(position);
                }
                break;*/
            default:
                break;
        }

        if (mFocusedItemView != null) {
            mFocusedItemView.onRequireTouchEvent(event);
        }

        return super.onTouchEvent(event);
    }

}
