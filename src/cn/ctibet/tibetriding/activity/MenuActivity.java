package cn.ctibet.tibetriding.activity;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.os.Build;
import android.os.Bundle;
import android.view.KeyEvent;
import android.view.View;
import android.widget.*;
import cn.ctibet.tibetriding.R;
import cn.ctibet.tibetriding.adapter.MainMenuListAdapter;
import cn.ctibet.tibetriding.app.ExitApplication;
import cn.ctibet.tibetriding.bean.MenuBean;
import cn.ctibet.tibetriding.bean.UserInfo;
import cn.ctibet.tibetriding.util.UserInfoUtil;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Administrator on 2015/1/21.
 */
public class MenuActivity extends Activity {
    private ListView listView;
    private Context context;
    private long firstClick;
    private ImageView userImg;
    private RelativeLayout setBtn;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.menu_layout);
        ExitApplication.getInstance().addActivity(this);
        context = MenuActivity.this;
        initView();
    }

    private void initView() {
        userImg = (ImageView) findViewById(R.id.menu_activity_user_img);
        listView = (ListView) findViewById(R.id.menu_list);
        setBtn = (RelativeLayout) findViewById(R.id.menu_layout_setting);
        int[] img = {R.drawable.flag, R.drawable.data, R.drawable.history,
                R.drawable.route, R.drawable.news, R.drawable.friend};
        int[] text = {R.string.menu1, R.string.menu2, R.string.menu3,
                R.string.menu4, R.string.menu5, R.string.menu6};
        List<MenuBean> list = new ArrayList<MenuBean>();
        MenuBean bean = null;
        for (int i = 0; i < img.length; i++) {
            bean = new MenuBean();
            bean.img = img[i];
            bean.name = text[i];
            if (i == UserInfoUtil.getMenuPos(context)) {
                bean.isCheck = true;
            } else {
                bean.isCheck = false;
            }
            list.add(bean);
        }
        final MainMenuListAdapter adapter = new MainMenuListAdapter(context);
        listView.setAdapter(adapter);

        adapter.addList(list);
        adapter.notifyDataSetChanged();

        listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {
                List<MenuBean> list = adapter.getList();
                for (int j = 0; j < list.size(); j++) {
                    if (j == i) {
                        list.get(j).isCheck = true;
                        UserInfoUtil.setMenuPos(context, j);
                    } else {
                        list.get(j).isCheck = false;
                    }
                }
                adapter.addList(list);
                adapter.notifyDataSetChanged();

                switch (i) {
                    case 0:
                        startActivity(new Intent(context, MainActivity.class));
                        break;
                    case 1:
                        startActivity(new Intent(context, SignUpActivity.class));
                        break;
                    case 2:
                        startActivity(new Intent(context, HistoryTrajectoryActivity.class));
                        break;
                    case 4:
                        startActivity(new Intent(context, CyclingInfoActivity.class));
                        break;
                    case 5:
                        startActivity(new Intent(context, NearActivity.class));
                        break;
                }

                if (Build.VERSION.SDK_INT > 5) {
                    overridePendingTransition(R.anim.zoomlrin, R.anim.zoomlrout);
                }
                finish();
            }
        });
        userImg.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                startActivity(new Intent(context, PersonInfoOneActivity.class));
            }
        });
        setBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                startActivity(new Intent(context, SettingActivity.class));
            }
        });
    }

    public boolean onKeyDown(int keyCode, KeyEvent event) {

        if (keyCode == KeyEvent.KEYCODE_BACK) {
            long secondClick = System.currentTimeMillis();
            if (secondClick - firstClick > 5000) {
                Toast.makeText(MenuActivity.this, "再按一次返回键退出程序",
                        Toast.LENGTH_SHORT).show();
                firstClick = secondClick;
                return true;
            } else {
                UserInfoUtil.setMenuPos(context, 0);
                ExitApplication.getInstance().exit();
                return true;
            }
        }
        return super.onKeyDown(keyCode, event);
    }
}