package cn.ctibet.tibetriding.activity;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.graphics.drawable.BitmapDrawable;
import android.os.Bundle;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.PopupWindow;
import android.widget.RelativeLayout;
import android.widget.TextView;
import cn.ctibet.tibetriding.R;

/**
 * Created by Administrator on 2015/3/23.
 */
public class SettingActivity extends Activity implements View.OnClickListener {
    private Context context;
    private RelativeLayout back;
    private TextView back_Text;
    private TextView title;
    private RelativeLayout accout;
    private RelativeLayout remind;
    private RelativeLayout privacy;
    private RelativeLayout cache;
    private RelativeLayout version;
    private PopupWindow popupWindow;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.setting_activity);
        context = SettingActivity.this;
        initView();
    }

    private void initView() {
        back = (RelativeLayout) findViewById(R.id.head_left);
        back_Text = (TextView) findViewById(R.id.head_left_text_btn);
        back_Text.setBackgroundResource(R.drawable.back);
        title = (TextView) findViewById(R.id.head_title);
        title.setText(getResources().getString(R.string.setting));
        back.setOnClickListener(this);

        accout = (RelativeLayout) findViewById(R.id.setting_activity_account_set);
        remind = (RelativeLayout) findViewById(R.id.setting_activity_msg_remind);
        privacy = (RelativeLayout) findViewById(R.id.setting_activity_privacy);
        cache = (RelativeLayout) findViewById(R.id.setting_activity_cache);
        version = (RelativeLayout) findViewById(R.id.setting_activity_version);

        accout.setOnClickListener(this);
        remind.setOnClickListener(this);
        privacy.setOnClickListener(this);
        cache.setOnClickListener(this);
        version.setOnClickListener(this);
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.head_left:
                finish();
                break;
            case R.id.setting_activity_account_set:
                startActivity(new Intent(context, AccountSetActivity.class));
                break;
            case R.id.setting_activity_msg_remind:
                startActivity(new Intent(context, MsgSetActivity.class));
                break;
            case R.id.setting_activity_privacy:
                startActivity(new Intent(context, PrivacySetActivity.class));
                break;
            case R.id.setting_activity_cache:
                initPopWindow(1);
                popupWindow.showAtLocation(view, Gravity.CENTER, 0, 0);
                break;
            case R.id.setting_activity_version:
                initPopWindow(2);
                popupWindow.showAtLocation(view, Gravity.CENTER, 0, 0);
                break;
        }
    }

    private void initPopWindow(int type) {
        View view = LayoutInflater.from(context).inflate(
                R.layout.prompt_item, null);
        TextView content = (TextView)view.findViewById(R.id.prompt_item_content);
        if (type == 1) {
            content.setText("缓存清理成功！");
        } else {
            content.setText("已是最新版本！");
        }
        popupWindow = new PopupWindow(view, LinearLayout.LayoutParams.FILL_PARENT,
                LinearLayout.LayoutParams.FILL_PARENT, true);
        popupWindow.setContentView(view);
        popupWindow.setFocusable(true);
        popupWindow.setOutsideTouchable(false);
        popupWindow.setBackgroundDrawable(new BitmapDrawable());
        view.setFocusable(true);
        view.setFocusableInTouchMode(true);
        view.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (null != popupWindow) {
                    popupWindow.dismiss();
                    popupWindow = null;
                }
            }
        });
    }
}