package cn.ctibet.tibetriding.activity;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.os.Build;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentActivity;
import android.support.v4.app.FragmentManager;
import android.support.v4.view.ViewPager;
import android.view.KeyEvent;
import android.view.View;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;
import cn.ctibet.tibetriding.R;
import cn.ctibet.tibetriding.adapter.SosViewPagerAdapters;
import cn.ctibet.tibetriding.app.ExitApplication;
import cn.ctibet.tibetriding.fragment.DailyExerciseFragment;
import cn.ctibet.tibetriding.fragment.LineRidingFragment;
import cn.ctibet.tibetriding.util.UserInfoUtil;

import java.util.ArrayList;

/**
 * Created by Administrator on 2015/1/28.
 */
public class HistoryTrajectoryActivity extends FragmentActivity implements View.OnClickListener {
    private RelativeLayout rela1;
    private TextView title1;
    private View line1;
    private RelativeLayout rela2;
    private TextView title2;
    private View line2;
    private TextView[] textView;
    private View[] view;
    private ArrayList<Fragment> fragments;
    private ViewPager viewPager;
    private FragmentManager fm;
    private LineRidingFragment lineRidingFragment = new LineRidingFragment();
    private DailyExerciseFragment dailyExerciseFragment = new DailyExerciseFragment();
    private RelativeLayout back;
    private TextView back_Text;
    private TextView title;
    private Context context;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.history_trajectory_activity);
        ExitApplication.getInstance().addActivity(this);
        context = HistoryTrajectoryActivity.this;
        initView();
    }

    private void initView() {
        fm = this.getSupportFragmentManager();
        back = (RelativeLayout) findViewById(R.id.head_left);
        back_Text = (TextView) findViewById(R.id.head_left_text_btn);
        back_Text.setBackgroundResource(R.drawable.home_menu);
        title = (TextView) findViewById(R.id.head_title);
        title.setText(getResources().getString(R.string.history));

        back.setOnClickListener(this);
        rela1 = (RelativeLayout) findViewById(R.id.history_trajectory_activity_rela);
        title1 = (TextView) findViewById(R.id.history_trajectory_activity_name);
        line1 = (View) findViewById(R.id.history_trajectory_activity_line);

        rela2 = (RelativeLayout) findViewById(R.id.history_trajectory_activity_rela1);
        title2 = (TextView) findViewById(R.id.history_trajectory_activity_name1);
        line2 = (View) findViewById(R.id.history_trajectory_activity_line1);
        viewPager = (ViewPager) findViewById(R.id.history_trajectory_activity_viewPager);

        rela1.setOnClickListener(this);
        rela2.setOnClickListener(this);
        setViewpager();
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.head_left:
                startActivity(new Intent(context, MenuActivity.class));
                if (Build.VERSION.SDK_INT > 5) {
                    overridePendingTransition(R.anim.zoomrlin, R.anim.zoomrlout);
                }
                break;

            case R.id.history_trajectory_activity_rela:
                setColor(0);
                viewPager.setCurrentItem(0);
                break;
            case R.id.history_trajectory_activity_rela1:
                setColor(1);
                viewPager.setCurrentItem(1);
                break;
        }
    }

    private void setViewpager() {
        textView = new TextView[]{title1, title2};
        view = new View[]{line1, line2};
        Fragment[] frags = {lineRidingFragment, dailyExerciseFragment};
        fragments = new ArrayList<Fragment>();
        for (int i = 0; i < frags.length; i++) {
            fragments.add(frags[i]);
        }

        viewPager.setOffscreenPageLimit(1);
        viewPager.setAdapter(new SosViewPagerAdapters(fm, fragments));
        viewPager.setCurrentItem(0);
        //setColor(1);
        viewPager.setOnPageChangeListener(new ViewPager.OnPageChangeListener() {
            @Override
            public void onPageScrolled(int i, float v, int i2) {

            }

            @Override
            public void onPageSelected(int position) {
                setColor(position);
            }

            @Override
            public void onPageScrollStateChanged(int i) {

            }
        });
    }


    private void setColor(int position) {
        for (int i = 0; i < view.length; i++) {
            textView[i].setTextColor(getResources().getColor(R.color.gray_e));
            view[i].setBackgroundColor(getResources().getColor(R.color.title_bg));
        }
        textView[position].setTextColor(getResources().getColor(R.color.white));
        view[position].setBackgroundColor(getResources().getColor(R.color.yellow));
    }

    public boolean onKeyDown(int keyCode, KeyEvent event) {

        if (keyCode == KeyEvent.KEYCODE_BACK) {
            startActivity(new Intent(context, MenuActivity.class));
            if (Build.VERSION.SDK_INT > 5) {
                overridePendingTransition(R.anim.zoomrlin, R.anim.zoomrlout);
            }
            return true;
        }
        return super.onKeyDown(keyCode, event);
    }
}