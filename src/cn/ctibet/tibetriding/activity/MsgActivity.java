package cn.ctibet.tibetriding.activity;

import android.app.Activity;
import android.content.Context;
import android.os.Bundle;
import android.support.v4.app.FragmentActivity;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.view.View;
import android.widget.RelativeLayout;
import cn.ctibet.tibetriding.R;
import cn.ctibet.tibetriding.fragment.CommentFragment;
import cn.ctibet.tibetriding.fragment.DirectMsgFragment;
import cn.ctibet.tibetriding.fragment.PraiseFragment;
import cn.ctibet.tibetriding.fragment.RidingFriendsFragment;

/**
 * Created by Administrator on 2015/3/23.
 */
public class MsgActivity extends FragmentActivity implements View.OnClickListener {
    private Context context;
    private RelativeLayout direct_messages;
    private RelativeLayout comment;
    private RelativeLayout praise;
    private View line1;
    private View line2;
    private View line3;
    private View[] view;
    private FragmentManager fm;
    private FragmentTransaction ft;
    private DirectMsgFragment directMsgFragment;
    private CommentFragment commentFragment;
    private PraiseFragment praiseFragment;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.msg_activity);
        context = MsgActivity.this;
        initView();
    }

    private void initView() {
        fm = this.getSupportFragmentManager();
        direct_messages = (RelativeLayout) findViewById(R.id.msg_activity_direct_messages);
        comment = (RelativeLayout) findViewById(R.id.msg_activity_comment);
        praise = (RelativeLayout) findViewById(R.id.msg_activity_praise);

        line1 = (View) findViewById(R.id.msg_activity_line1);
        line2 = (View) findViewById(R.id.msg_activity_line2);
        line3 = (View) findViewById(R.id.msg_activity_line3);
        view = new View[]{line1, line2, line3};
        setLineShow(0);
        loadFragment(1);
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.msg_activity_direct_messages:
                setLineShow(0);
                loadFragment(1);
                break;
            case R.id.msg_activity_comment:
                setLineShow(1);
                loadFragment(2);
                break;
            case R.id.msg_activity_praise:
                setLineShow(2);
                loadFragment(3);
                break;
        }
    }

    private void loadFragment(int pos) {

        ft = fm.beginTransaction();
        if (null != directMsgFragment) {
            ft.hide(directMsgFragment);
        }
        if (null != commentFragment) {
            ft.hide(commentFragment);
        }
        if (null != praiseFragment) {
            ft.hide(praiseFragment);
        }
        switch (pos) {
            case 1:
                if (null == directMsgFragment) {
                    directMsgFragment = new DirectMsgFragment();
                    ft.add(R.id.msg_activity_fragment, directMsgFragment, "");
                } else {
                    ft.show(directMsgFragment);
                }
                break;
            case 2:
                if (null == commentFragment) {
                    commentFragment = new CommentFragment();
                    ft.add(R.id.msg_activity_fragment, commentFragment, "");
                } else {
                    ft.show(commentFragment);
                }
                break;
            case 3:
                if (null == praiseFragment) {
                    praiseFragment = new PraiseFragment();
                    ft.add(R.id.msg_activity_fragment, praiseFragment, "");
                } else {
                    ft.show(praiseFragment);
                }
                break;
        }
        ft.commitAllowingStateLoss();

    }

    private void setLineShow(int position) {
        for (int i = 0; i < view.length; i++) {
            view[i].setVisibility(View.GONE);
        }
        view[position].setVisibility(View.VISIBLE);
    }
}