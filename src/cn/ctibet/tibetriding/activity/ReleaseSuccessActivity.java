package cn.ctibet.tibetriding.activity;

import android.app.Activity;
import android.content.Context;
import android.os.Bundle;
import android.view.View;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import cn.ctibet.tibetriding.R;
import cn.ctibet.tibetriding.app.ExitApplication;

/**
 * 直播发布成功页面
 * Created by Administrator on 2015/1/20.
 */
public class ReleaseSuccessActivity extends Activity implements View.OnClickListener {
    private RelativeLayout back;
    private TextView back_Text;
    private TextView title;
    private Context context;
    private ImageView qqBtn;
    private ImageView weChatBtn;
    private ImageView weiBoBtn;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.release_success_activity);
        ExitApplication.getInstance().addActivity(this);
        context = ReleaseSuccessActivity.this;
        initView();
    }

    private void initView() {
        back = (RelativeLayout) findViewById(R.id.head_left);
        back_Text = (TextView) findViewById(R.id.head_left_text_btn);
        back_Text.setBackgroundResource(R.drawable.back);
        title = (TextView) findViewById(R.id.head_title);
        title.setText(getResources().getString(R.string.release_success));

        qqBtn = (ImageView) findViewById(R.id.release_success_activity_qq);
        weChatBtn = (ImageView) findViewById(R.id.release_success_activity_wechat);
        weiBoBtn = (ImageView) findViewById(R.id.release_success_activity_weibo);

        back.setOnClickListener(this);
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.head_left:
                finish();
                break;
        }
    }
}