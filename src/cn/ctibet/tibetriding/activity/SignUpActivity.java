package cn.ctibet.tibetriding.activity;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.text.TextUtils;
import android.util.Log;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;
import cn.ctibet.tibetriding.R;
import cn.ctibet.tibetriding.bean.Configs;
import cn.ctibet.tibetriding.bean.OrderBean;
import cn.ctibet.tibetriding.util.DialogShowUtil;
import cn.ctibet.tibetriding.util.SyncHttp;
import cn.ctibet.tibetriding.util.SysPrintUtil;
import cn.ctibet.tibetriding.zfbapply.Keys;
import cn.ctibet.tibetriding.zfbapply.Result;
import cn.ctibet.tibetriding.zfbapply.Rsa;
import com.alipay.android.app.sdk.AliPay;

import java.io.UnsupportedEncodingException;
import java.net.URLEncoder;
import java.text.SimpleDateFormat;
import java.util.Date;

/**
 * 组团报名页面
 * Created by Administrator on 2015/1/30.
 */
public class SignUpActivity extends Activity implements View.OnClickListener {
    private RelativeLayout back;
    private TextView back_Text;
    private TextView title;
    private LinearLayout haveInfoLinear;
    private LinearLayout noInfoLinear;
    private TextView changeInfoBtn;
    private Context context;
    private TextView applyBtn;
    private DialogShowUtil dialogShowUtil;
    public static final String TAG = "alipay-sdk";
    private static final int RQF_PAY = 1;
    private static final int RQF_LOGIN = 2;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.sign_up_activity);
        context = SignUpActivity.this;
        initView();
    }

    private void initView() {
        back = (RelativeLayout) findViewById(R.id.head_left);
        back_Text = (TextView) findViewById(R.id.head_left_text_btn);
        back_Text.setBackgroundResource(R.drawable.back);
        title = (TextView) findViewById(R.id.head_title);
        title.setText(getResources().getString(R.string.qxbm));

        haveInfoLinear = (LinearLayout) findViewById(R.id.sign_up_activity_have_info);
        noInfoLinear = (LinearLayout) findViewById(R.id.sign_up_activity_no_info);
        changeInfoBtn = (TextView) findViewById(R.id.sign_up_activity_change_info);

        applyBtn = (TextView) findViewById(R.id.sign_up_activity_apply);
        back.setOnClickListener(this);
        changeInfoBtn.setOnClickListener(this);
        applyBtn.setOnClickListener(this);
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.head_left:
                finish();
                break;
            case R.id.sign_up_activity_change_info:
                startActivity(new Intent(context, ChangeInfoActivity.class));
                break;
            case R.id.sign_up_activity_apply:
                getOrderInfo(100);
                break;
        }
    }

    private void getOrderInfo(final float total_fee) {
        dialogShowUtil = new DialogShowUtil(context, getResources().getString(R.string.get_order));
        dialogShowUtil.dialogShow();
        new Thread(new Runnable() {
            @Override
            public void run() {
                Message msg = new Message();
                try {
                  /*  String str = "/api/PayOrder/" + userInfo.id + "?total_fee=" + total_fee;
                    String jsonData = SyncHttp.httpGet(Configs.HOST, str, userInfo.token);
                    Log.v("json==发送服务器的数据为", Configs.HOST + str);
                    Log.v("json==获取到的服务器的数据为", jsonData);
                    result = JsonUtils.getOrderData(jsonData);
                    if (result.message.equals("success")) {
                        msg.what = Configs.READ_SUCCESS;
                    } else {
                        msg.what = Configs.READ_FAIL;
                    }*/
                } catch (Exception e) {
                    e.printStackTrace();
                    Log.v("异常信息为", e.toString());
                    msg.what = Configs.READ_ERROR;
                }
                mmHandler.sendMessage(msg);
            }
        }).start();
    }

    public Handler mmHandler = new Handler() {
        @Override
        public void handleMessage(Message msg) {
            if (null != dialogShowUtil) {
                dialogShowUtil.dialogDismiss();
            }
            switch (msg.what) {
                case Configs.READ_SUCCESS:
                    //order(result.orderBean);
                    break;
                case Configs.READ_FAIL:
                    Toast.makeText(context, getResources().getString(R.string.order_fail), Toast.LENGTH_SHORT).show();
                    break;
                case Configs.READ_ERROR:
                    Toast.makeText(context, getResources().getString(R.string.order_fail), Toast.LENGTH_SHORT).show();
                    break;
            }
        }
    };

    public void order(OrderBean orderBean) {
        try {
            Log.i("ExternalPartner", "onItemClick");
            String info = getNewOrderInfo(orderBean);
            String sign = Rsa.sign(info, Keys.PRIVATE);
            sign = URLEncoder.encode(sign);
            info += "&sign=\"" + sign + "\"&" + getSignType();
            Log.i("ExternalPartner", "start pay");
            // start the pay.
            // Log.i(TAG, "info = " + info);

            final String orderInfo = info;
            new Thread() {
                public void run() {
                    AliPay alipay = new AliPay(SignUpActivity.this, mHandler);

                    //设置为沙箱模式，不设置默认为线上环境
                    //alipay.setSandBox(true);

                    String result = alipay.pay(orderInfo);

                    Log.i(TAG, "result = " + result);
                    Message msg = new Message();
                    msg.what = RQF_PAY;
                    msg.obj = result;
                    mHandler.sendMessage(msg);
                }
            }.start();
        } catch (Exception ex) {
            SysPrintUtil.pt("支付异常==", ex.toString());
            ex.printStackTrace();
            Toast.makeText(context, R.string.remote_call_failed,
                    Toast.LENGTH_SHORT).show();
        }
    }


    Handler mHandler = new Handler() {
        public void handleMessage(android.os.Message msg) {
            Result result = new Result((String) msg.obj);

            switch (msg.what) {
                case RQF_PAY:
                case RQF_LOGIN: {
                    if (null != result) {
                        Toast.makeText(context, result.getResult(),
                                Toast.LENGTH_SHORT).show();
                    }
                }
                break;
                default:
                    break;
            }
        }

        ;
    };

    private String getNewOrderInfo(OrderBean orderBean) {
        StringBuilder sb = new StringBuilder();
        sb.append("partner=\"");
        sb.append(Keys.DEFAULT_PARTNER);
        sb.append("\"&out_trade_no=\"");
        sb.append(orderBean.out_trade_no);
        sb.append("\"&subject=\"");
        sb.append(orderBean.subject);
        sb.append("\"&body=\"");
        sb.append(orderBean.body);
        sb.append("\"&total_fee=\"");
        sb.append(orderBean.total_fee);
        sb.append("\"&notify_url=\"");

        // 网址需要做URL编码
        sb.append(URLEncoder.encode(orderBean.notify_url));
        sb.append("\"&service=\"mobile.securitypay.pay");
        sb.append("\"&_input_charset=\"UTF-8");
        sb.append("\"&return_url=\"");
        sb.append(URLEncoder.encode("http://m.alipay.com"));
        sb.append("\"&payment_type=\"1");
        sb.append("\"&seller_id=\"");
        sb.append(Keys.DEFAULT_SELLER);

        // 如果show_url值为空，可不传
        // sb.append("\"&show_url=\"");
        sb.append("\"&it_b_pay=\"1m");
        sb.append("\"");

        return new String(sb);
    }

    private String getOutTradeNo() {
        SimpleDateFormat format = new SimpleDateFormat("MMddHHmmss");
        Date date = new Date();
        String key = format.format(date);

        java.util.Random r = new java.util.Random();
        key += r.nextInt();
        key = key.substring(0, 15);
        Log.d(TAG, "outTradeNo: " + key);
        return key;
    }

    private String getSignType() {
        return "sign_type=\"RSA\"";
    }


    private String trustLogin(String partnerId, String appUserId) {
        StringBuilder sb = new StringBuilder();
        sb.append("app_name=\"mc\"&biz_type=\"trust_login\"&partner=\"");
        sb.append(partnerId);
        Log.d("TAG", "UserID = " + appUserId);
        if (!TextUtils.isEmpty(appUserId)) {
            appUserId = appUserId.replace("\"", "");
            sb.append("\"&app_id=\"");
            sb.append(appUserId);
        }
        sb.append("\"");

        String info = sb.toString();

        // 请求信息签名
        String sign = Rsa.sign(info, Keys.PRIVATE);
        try {
            sign = URLEncoder.encode(sign, "UTF-8");
        } catch (UnsupportedEncodingException e) {
            e.printStackTrace();
        }
        info += "&sign=\"" + sign + "\"&" + getSignType();

        return info;
    }

}