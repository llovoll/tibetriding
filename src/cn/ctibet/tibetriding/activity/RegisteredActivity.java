package cn.ctibet.tibetriding.activity;

import android.app.Activity;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentActivity;
import android.support.v4.app.FragmentManager;
import android.support.v4.view.ViewPager;
import android.text.Html;
import android.view.View;
import android.widget.RelativeLayout;
import android.widget.TextView;
import cn.ctibet.tibetriding.R;
import cn.ctibet.tibetriding.adapter.SosViewPagerAdapters;
import cn.ctibet.tibetriding.app.ExitApplication;
import cn.ctibet.tibetriding.fragment.EmailRegistFragment;
import cn.ctibet.tibetriding.fragment.PhoneRegistFragment;

import java.util.ArrayList;

/**
 * Created by Administrator on 2015/1/23.
 */
public class RegisteredActivity extends FragmentActivity implements View.OnClickListener{
    private FragmentManager fm;
    private RelativeLayout back;
    private RelativeLayout phoneBtn;
    private TextView phoneBtn_text;
    private RelativeLayout rela1;
    private TextView title1;
    private View line1;
    private RelativeLayout rela2;
    private TextView title2;
    private View line2;
    private ViewPager viewPager;
    private TextView[] textView;
    private View[] view;
    private ArrayList<Fragment> fragments;
    private PhoneRegistFragment phoneRegistFragment=new PhoneRegistFragment();
    private EmailRegistFragment emailRegistFragment=new EmailRegistFragment();

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.registered_activity);
        ExitApplication.getInstance().addActivity(this);
        initView();
    }

    private void initView() {
        fm = this.getSupportFragmentManager();
        back = (RelativeLayout) findViewById(R.id.head_left);
   /*     phoneBtn = (RelativeLayout) findViewById(R.id.head_right);
        phoneBtn.setVisibility(View.VISIBLE);
        phoneBtn_text = (TextView) findViewById(R.id.head_right_text_btn);
        phoneBtn_text.setBackgroundResource(R.drawable.phone);*/

        rela1 = (RelativeLayout) findViewById(R.id.cycling_photo_album_btn);
        title1 = (TextView) findViewById(R.id.cycling_photo_album);
        line1 = (View) findViewById(R.id.cycling_photo_album_line);

        rela2 = (RelativeLayout) findViewById(R.id.phone_photo_album_btn);
        title2 = (TextView) findViewById(R.id.phone_photo_album);
        line2 = (View) findViewById(R.id.phone_photo_album_line);

        title1.setText(getResources().getString(R.string.phone_reg));
        title2.setText(getResources().getString(R.string.email_reg));


        viewPager = (ViewPager) findViewById(R.id.registered_activity_viewpager);

        back.setOnClickListener(this);
        rela1.setOnClickListener(this);
        rela2.setOnClickListener(this);
        setViewpager();
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.head_left:
                finish();
                break;
            case R.id.cycling_photo_album_btn:
                setColor(0);
                viewPager.setCurrentItem(0);
                break;
            case R.id.phone_photo_album_btn:
                setColor(1);
                viewPager.setCurrentItem(1);
                break;
        }
    }

    private void setViewpager() {
        textView = new TextView[]{title1, title2};
        view = new View[]{line1, line2};
        Fragment[] frags = {phoneRegistFragment, emailRegistFragment};
        fragments = new ArrayList<Fragment>();
        for (int i = 0; i < frags.length; i++) {
            fragments.add(frags[i]);
        }

        viewPager.setOffscreenPageLimit(1);
        viewPager.setAdapter(new SosViewPagerAdapters(fm, fragments));
        viewPager.setCurrentItem(0);
        //setColor(1);
        viewPager.setOnPageChangeListener(new ViewPager.OnPageChangeListener() {
            @Override
            public void onPageScrolled(int i, float v, int i2) {

            }

            @Override
            public void onPageSelected(int position) {
                setColor(position);
            }

            @Override
            public void onPageScrollStateChanged(int i) {

            }
        });
    }


    private void setColor(int position) {
        for (int i = 0; i < view.length; i++) {
            textView[i].setTextColor(getResources().getColor(R.color.gray_e));
            view[i].setBackgroundColor(getResources().getColor(R.color.title_bg));
        }
        textView[position].setTextColor(getResources().getColor(R.color.white));
        view[position].setBackgroundColor(getResources().getColor(R.color.yellow));
    }
}