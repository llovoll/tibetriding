package cn.ctibet.tibetriding.activity;

import android.app.Activity;
import android.content.Context;
import android.os.Bundle;
import android.view.View;
import android.widget.ListView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import cn.ctibet.tibetriding.R;
import cn.ctibet.tibetriding.adapter.RoadLineListAdapter;
import cn.ctibet.tibetriding.adapter.TrackDetailListAdapter;
import cn.ctibet.tibetriding.bean.CyclingImgBean;
import com.amap.api.maps.AMap;
import com.amap.api.maps.MapView;
import com.amap.api.maps.UiSettings;

import java.util.ArrayList;

/**
 * Created by Administrator on 2015/1/29.
 */
public class TrackDetailActivity extends Activity implements View.OnClickListener {
    private ListView listView;
    private ArrayList<CyclingImgBean> list;
    private TrackDetailListAdapter adapter;
    private Context context;
    private AMap aMap;
    private MapView mapView;
    private UiSettings mUiSettings;
    private RelativeLayout back;
    private TextView back_Text;
    private TextView title;
    private RelativeLayout sosBtn;
    private TextView sosBtn_text;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.track_detail_activity);
        context = TrackDetailActivity.this;
        mapView = (MapView) findViewById(R.id.mapView);
        mapView.onCreate(savedInstanceState);// 必须要写
        initView();
    }

    private void initView() {
        if (aMap == null) {
            aMap = mapView.getMap();
        }
        mUiSettings = aMap.getUiSettings();
        mUiSettings.setZoomControlsEnabled(false);//隐藏缩放按钮

        back = (RelativeLayout) findViewById(R.id.head_left);
        back_Text = (TextView) findViewById(R.id.head_left_text_btn);
        back_Text.setBackgroundResource(R.drawable.back);
        title = (TextView) findViewById(R.id.head_title);
        title.setText(getResources().getString(R.string.qxgj));
        back.setOnClickListener(this);

        sosBtn = (RelativeLayout) findViewById(R.id.head_right);
        sosBtn.setVisibility(View.VISIBLE);
        sosBtn_text = (TextView) findViewById(R.id.head_right_text_btn);
        sosBtn_text.setBackgroundResource(R.drawable.share2);

        listView = (ListView) findViewById(R.id.track_detail_activity_list);
        CyclingImgBean bean = null;
        list = new ArrayList<CyclingImgBean>();
        for (int i = 0; i < 8; i++) {
            bean = new CyclingImgBean();
            if (i == 0) {
                bean.isClick = true;
            } else {
                bean.isClick = false;
            }
            if (i <= 3) {
                bean.type = "1";
            } else if (i > 3 && i <= 5) {
                bean.type = "2";
            } else {
                bean.type = "3";
            }
            bean.location = "显示的内容。。。。。。。。。。";
            list.add(bean);
        }
        adapter = new TrackDetailListAdapter(context, list);
        listView.setAdapter(adapter);

        back.setOnClickListener(this);
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.head_left:
                finish();
                break;
        }
    }

    /**
     * 方法必须重写
     */
    @Override
    public void onResume() {
        super.onResume();
        System.out.println("========onResume1========");
        mapView.onResume();
    }

    /**
     * 方法必须重写
     */
    @Override
    public void onPause() {
        super.onPause();
        System.out.println("========onPause1========");
        mapView.onPause();
    }

    /**
     * 方法必须重写
     */
    @Override
    public void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
        mapView.onSaveInstanceState(outState);
    }

    /**
     * 方法必须重写
     */
    @Override
    public void onDestroy() {
        super.onDestroy();
        mapView.onDestroy();
    }

}