package cn.ctibet.tibetriding.activity;

import android.app.Activity;
import android.content.Context;
import android.os.Bundle;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;
import cn.ctibet.tibetriding.R;

/**
 * Created by Administrator on 2015/2/2.
 */
public class ChangeInfoActivity extends Activity implements View.OnClickListener {
    private Context context;
    private RelativeLayout rela1;
    private RelativeLayout rela2;
    private RelativeLayout rela3;
    private LinearLayout baseInfoLinear;
    private LinearLayout phoneLinear;
    private LinearLayout timeLinear;
    private boolean baseInfoTag = true;
    private boolean phoneTag = true;
    private boolean timeTag = true;
    private ImageView img1;
    private ImageView img2;
    private ImageView img3;
    private RelativeLayout back;
    private TextView back_Text;
    private TextView title;
    private RelativeLayout sosBtn;
    private TextView sosBtn_text;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.change_info_activity);
        context = ChangeInfoActivity.this;
        initView();
    }

    private void initView() {
        back = (RelativeLayout) findViewById(R.id.head_left);
        back_Text = (TextView) findViewById(R.id.head_left_text_btn);
        back_Text.setBackgroundResource(R.drawable.back);
        title = (TextView) findViewById(R.id.head_title);
        title.setText(getResources().getString(R.string.bmxx));
        back.setOnClickListener(this);

        sosBtn = (RelativeLayout) findViewById(R.id.head_right);
        sosBtn.setVisibility(View.VISIBLE);
        sosBtn_text = (TextView) findViewById(R.id.head_right_text_btn);
        sosBtn_text.setText(getResources().getString(R.string.compele));
        sosBtn_text.setTextColor(getResources().getColor(R.color.blue4));

        rela1 = (RelativeLayout) findViewById(R.id.change_info_activity_rela1);
        rela2 = (RelativeLayout) findViewById(R.id.change_info_activity_rela2);
        rela3 = (RelativeLayout) findViewById(R.id.change_info_activity_rela3);

        baseInfoLinear = (LinearLayout) findViewById(R.id.change_info_activity_base_info);
        phoneLinear = (LinearLayout) findViewById(R.id.change_info_activity_phone);
        timeLinear = (LinearLayout) findViewById(R.id.change_info_activity_time);

        img1 = (ImageView) findViewById(R.id.change_info_activity_img1);
        img2 = (ImageView) findViewById(R.id.change_info_activity_img2);
        img3 = (ImageView) findViewById(R.id.change_info_activity_img3);

        rela1.setOnClickListener(this);
        rela2.setOnClickListener(this);
        rela3.setOnClickListener(this);
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.head_left:
                finish();
                break;
            case R.id.change_info_activity_rela1:
                if (baseInfoTag) {
                    baseInfoLinear.setVisibility(View.VISIBLE);
                    baseInfoTag = false;
                    img1.setBackgroundResource(R.drawable.arrow_up);
                } else {
                    baseInfoLinear.setVisibility(View.GONE);
                    baseInfoTag = true;
                    img1.setBackgroundResource(R.drawable.arrow_down);
                }
                break;
            case R.id.change_info_activity_rela2:
                if (phoneTag) {
                    phoneLinear.setVisibility(View.VISIBLE);
                    phoneTag = false;
                    img2.setBackgroundResource(R.drawable.arrow_up);
                } else {
                    phoneLinear.setVisibility(View.GONE);
                    phoneTag = true;
                    img2.setBackgroundResource(R.drawable.arrow_down);
                }
                break;
            case R.id.change_info_activity_rela3:
                if (timeTag) {
                    timeLinear.setVisibility(View.VISIBLE);
                    timeTag = false;
                    img3.setBackgroundResource(R.drawable.arrow_up);
                } else {
                    timeLinear.setVisibility(View.GONE);
                    timeTag = true;
                    img3.setBackgroundResource(R.drawable.arrow_down);
                }
                break;
        }
    }
}