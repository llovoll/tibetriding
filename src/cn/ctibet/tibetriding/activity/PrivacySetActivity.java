package cn.ctibet.tibetriding.activity;

import android.app.Activity;
import android.content.Context;
import android.graphics.drawable.BitmapDrawable;
import android.os.Bundle;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.PopupWindow;
import android.widget.RelativeLayout;
import android.widget.TextView;
import cn.ctibet.tibetriding.R;

/**
 * Created by Administrator on 2015/3/23.
 */
public class PrivacySetActivity extends Activity implements View.OnClickListener {
    private Context context;
    private RelativeLayout back;
    private TextView back_Text;
    private TextView title;
    private PopupWindow popupWindow;
    private RelativeLayout info_remind;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.privacy_set_activity);
        context = PrivacySetActivity.this;
        initView();
    }

    private void initView() {
        back = (RelativeLayout) findViewById(R.id.head_left);
        back_Text = (TextView) findViewById(R.id.head_left_text_btn);
        back_Text.setBackgroundResource(R.drawable.back);
        title = (TextView) findViewById(R.id.head_title);
        title.setText(getResources().getString(R.string.privacy_set));
        back.setOnClickListener(this);

        info_remind = (RelativeLayout) findViewById(R.id.message_setting_activity_info_remind);
        info_remind.setOnClickListener(this);
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.head_left:
                finish();
                break;
            case R.id.message_setting_activity_info_remind:
                initPopWindow();
                popupWindow.showAtLocation(view, Gravity.CENTER, 0, 0);
                break;
        }
    }

    private void initPopWindow() {
        View view = LayoutInflater.from(context).inflate(R.layout.distance_item, null);
        popupWindow = new PopupWindow(view, LinearLayout.LayoutParams.FILL_PARENT,
                LinearLayout.LayoutParams.FILL_PARENT, true);
        popupWindow.setContentView(view);
        popupWindow.setFocusable(true);
        popupWindow.setOutsideTouchable(false);
        popupWindow.setBackgroundDrawable(new BitmapDrawable());
        view.setFocusable(true);
        view.setFocusableInTouchMode(true);
        view.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (null != popupWindow) {
                    popupWindow.dismiss();
                }
            }
        });
    }
}