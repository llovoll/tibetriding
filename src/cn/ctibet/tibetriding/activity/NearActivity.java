package cn.ctibet.tibetriding.activity;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentActivity;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.text.Html;
import android.view.View;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import cn.ctibet.tibetriding.R;
import cn.ctibet.tibetriding.db.Live;
import cn.ctibet.tibetriding.fragment.RidingFriendsFragment;

/**
 * Created by Administrator on 2015/3/23.
 */
public class NearActivity extends FragmentActivity implements View.OnClickListener {
    private Context context;
    private FragmentManager fm;
    private RelativeLayout back;
    private RelativeLayout phoneBtn;
    private TextView phoneBtn_text;
    private RelativeLayout rela1;
    private TextView title1;
    private View line1;
    private RelativeLayout rela2;
    private TextView title2;
    private View line2;
    private TextView[] textView;
    private View[] view;
    private RidingFriendsFragment ridingFriendsFragment = null;
    private LiveFragment liveFragment = null;
    private FragmentTransaction ft;
    private TextView back_text;
    private ImageView point;


    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.near_activity);
        initView();
    }

    private void initView() {
        fm = this.getSupportFragmentManager();
        back = (RelativeLayout) findViewById(R.id.head_left);
        back_text = (TextView) findViewById(R.id.head_left_text_btn);
        back_text.setBackgroundResource(R.drawable.home_menu);
        phoneBtn = (RelativeLayout) findViewById(R.id.head_right);
        phoneBtn.setVisibility(View.VISIBLE);
        phoneBtn_text = (TextView) findViewById(R.id.head_right_text_btn);
        phoneBtn_text.setBackgroundResource(R.drawable.message1);
        point = (ImageView) findViewById(R.id.head_right_text_point);
        point.setVisibility(View.VISIBLE);

        rela1 = (RelativeLayout) findViewById(R.id.cycling_photo_album_btn);
        title1 = (TextView) findViewById(R.id.cycling_photo_album);
        line1 = (View) findViewById(R.id.cycling_photo_album_line);

        rela2 = (RelativeLayout) findViewById(R.id.phone_photo_album_btn);
        title2 = (TextView) findViewById(R.id.phone_photo_album);
        line2 = (View) findViewById(R.id.phone_photo_album_line);

        title1.setText(Html.fromHtml("&nbsp 骑友 &nbsp"));
        title2.setText(Html.fromHtml("&nbsp 直播 &nbsp"));
        rela1.setOnClickListener(this);
        rela2.setOnClickListener(this);
        phoneBtn.setOnClickListener(this);
        textView = new TextView[]{title1, title2};
        view = new View[]{line1, line2};
        loadFragment(1);
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.cycling_photo_album_btn:
                setColor(0);
                loadFragment(1);
                break;
            case R.id.phone_photo_album_btn:
                setColor(1);
                loadFragment(2);
                break;
            case R.id.head_right:
                startActivity(new Intent(context, MsgActivity.class));
                break;
        }
    }

    private void setColor(int position) {
        for (int i = 0; i < view.length; i++) {
            textView[i].setTextColor(getResources().getColor(R.color.gray_e));
            view[i].setBackgroundColor(getResources().getColor(R.color.title_bg));
        }
        textView[position].setTextColor(getResources().getColor(R.color.white));
        view[position].setBackgroundColor(getResources().getColor(R.color.yellow));
    }

    private void loadFragment(int pos) {

        ft = fm.beginTransaction();
        if (null != ridingFriendsFragment) {
            ft.hide(ridingFriendsFragment);
        }
        if (null != liveFragment) {
            ft.hide(liveFragment);
        }
        switch (pos) {
            case 1:
                if (null == ridingFriendsFragment) {
                    ridingFriendsFragment = new RidingFriendsFragment();
                    ft.add(R.id.near_activity_fragment, ridingFriendsFragment, "");
                } else {
                    ft.show(ridingFriendsFragment);
                }
                break;
            case 2:
                if (null == liveFragment) {
                    liveFragment = new LiveFragment();
                    ft.add(R.id.near_activity_fragment, liveFragment, "");
                } else {
                    ft.show(liveFragment);
                }
                break;
        }
        ft.commitAllowingStateLoss();

    }

}