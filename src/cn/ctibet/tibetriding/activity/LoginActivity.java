package cn.ctibet.tibetriding.activity;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.RelativeLayout;
import android.widget.TextView;
import cn.ctibet.tibetriding.R;
import cn.ctibet.tibetriding.app.ExitApplication;

/**
 * Created by Administrator on 2015/1/22.
 */
public class LoginActivity extends Activity implements View.OnClickListener {
    private RelativeLayout back;
    private TextView back_Text;
    private TextView title;
    private RelativeLayout registedBtn;
    private TextView registedBtn_text;
    private Context context;
    private TextView loginBtn;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.login_activity);
        ExitApplication.getInstance().addActivity(this);
        context = LoginActivity.this;
        initView();
    }

    private void initView() {
        back = (RelativeLayout) findViewById(R.id.head_left);
        back_Text = (TextView) findViewById(R.id.head_left_text_btn);
        back_Text.setBackgroundResource(R.drawable.back);
        title = (TextView) findViewById(R.id.head_title);
        title.setText(getResources().getString(R.string.login));
        registedBtn = (RelativeLayout) findViewById(R.id.head_right);
        registedBtn.setVisibility(View.VISIBLE);
        registedBtn_text = (TextView) findViewById(R.id.head_right_text_btn);
        registedBtn_text.setText(getResources().getString(R.string.regist));
        registedBtn_text.setTextColor(getResources().getColor(R.color.blue4));
        loginBtn = (TextView) findViewById(R.id.login_activity_loginBtn);

        back.setOnClickListener(this);
        registedBtn.setOnClickListener(this);
        loginBtn.setOnClickListener(this);
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.head_left:
                finish();
                break;
            case R.id.head_right:
                startActivity(new Intent(context, RegisteredActivity.class));
                break;
            case R.id.login_activity_loginBtn:
                startActivity(new Intent(context, MainActivity.class));
                break;
        }
    }
}