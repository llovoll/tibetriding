package cn.ctibet.tibetriding.activity;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.os.Build;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentActivity;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v4.view.ViewPager;
import android.text.Html;
import android.view.View;
import android.widget.RelativeLayout;
import android.widget.TextView;
import cn.ctibet.tibetriding.R;
import cn.ctibet.tibetriding.adapter.SosViewPagerAdapters;
import cn.ctibet.tibetriding.app.ExitApplication;
import cn.ctibet.tibetriding.bean.Configs;
import cn.ctibet.tibetriding.fragment.RoadBookFragment;
import cn.ctibet.tibetriding.fragment.TrajectoryFragment;
import cn.ctibet.tibetriding.impl.Right1ClickListener;
import cn.ctibet.tibetriding.impl.RightClickListener;

import java.util.ArrayList;

/**
 * 踏上征程页面
 * Created by Administrator on 2015/1/28.
 */
public class JourneyActivity extends FragmentActivity implements View.OnClickListener {
    private FragmentManager fm;
    private RelativeLayout back;
    private RelativeLayout phoneBtn;
    private TextView phoneBtn_text;
    private RelativeLayout rela1;
    private TextView title1;
    private View line1;
    private RelativeLayout rela2;
    private TextView title2;
    private View line2;
    private ViewPager viewPager;
    private TextView[] textView;
    private View[] view;
    private ArrayList<Fragment> fragments;
    private RoadBookFragment roadBookFragment = new RoadBookFragment(JourneyActivity.this);
    private TrajectoryFragment trajectoryFragment = new TrajectoryFragment(JourneyActivity.this);
    private Context context;
    private FragmentTransaction ft;
    private boolean isBook = true;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.journey_activity);
        ExitApplication.getInstance().addActivity(this);
        context = JourneyActivity.this;
        initView();
    }

    private void initView() {
        fm = this.getSupportFragmentManager();
        back = (RelativeLayout) findViewById(R.id.head_left);
        phoneBtn = (RelativeLayout) findViewById(R.id.head_right);
        phoneBtn.setVisibility(View.VISIBLE);
        phoneBtn_text = (TextView) findViewById(R.id.head_right_text_btn);
        phoneBtn_text.setBackgroundResource(R.drawable.journey);

        rela1 = (RelativeLayout) findViewById(R.id.cycling_photo_album_btn);
        title1 = (TextView) findViewById(R.id.cycling_photo_album);
        line1 = (View) findViewById(R.id.cycling_photo_album_line);

        rela2 = (RelativeLayout) findViewById(R.id.phone_photo_album_btn);
        title2 = (TextView) findViewById(R.id.phone_photo_album);
        line2 = (View) findViewById(R.id.phone_photo_album_line);

        title1.setText(Html.fromHtml("&nbsp&nbsp 路书 &nbsp&nbsp"));
        title2.setText(Html.fromHtml("&nbsp&nbsp 轨迹 &nbsp&nbsp"));

        viewPager = (ViewPager) findViewById(R.id.journey_activity_viewpager);

        back.setOnClickListener(this);
        phoneBtn.setOnClickListener(this);
        rela1.setOnClickListener(this);
        rela2.setOnClickListener(this);
        setViewpager();
        //loadFragment(0);
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.head_left:
                finish();
                break;
            case R.id.head_right:
                if (isBook) {
                    listener.getClick();
                }else {
                    listener1.getClick1();
                }
                break;
            case R.id.cycling_photo_album_btn:
                setColor(0);
                //loadFragment(0);
                viewPager.setCurrentItem(0);
                isBook = true;
                phoneBtn_text.setBackgroundResource(R.drawable.journey);
                break;
            case R.id.phone_photo_album_btn:
                setColor(1);
                //loadFragment(1);
                viewPager.setCurrentItem(1);
                isBook = false;
                phoneBtn_text.setBackgroundResource(R.drawable.camera);
                break;
        }
    }

    /* private void loadFragment(int position) {

         ft = fm.beginTransaction();
         if (null != roadBookFragment) {
             ft.hide(roadBookFragment);
         }
         if (null != trajectoryFragment) {
             ft.hide(trajectoryFragment);
         }
         switch (position) {
             case 0:
                 if (null == roadBookFragment) {
                     roadBookFragment = new RoadBookFragment();
                     ft.add(R.id.tab_activity_frag, roadBookFragment, "");
                 } else {
                     ft.show(roadBookFragment);
                 }
                 break;
             case 1:
                 if (null == trajectoryFragment) {
                     trajectoryFragment = new TrajectoryFragment();
                     ft.add(R.id.tab_activity_frag, trajectoryFragment, "");
                 } else {
                     ft.show(trajectoryFragment);
                 }

                 break;

         }
         ft.commit();

     }*/
    private void setViewpager() {
        textView = new TextView[]{title1, title2};
        view = new View[]{line1, line2};
        Fragment[] frags = {roadBookFragment, trajectoryFragment};
        fragments = new ArrayList<Fragment>();
        for (int i = 0; i < frags.length; i++) {
            fragments.add(frags[i]);
        }

        viewPager.setOffscreenPageLimit(1);
        viewPager.setAdapter(new SosViewPagerAdapters(fm, fragments));
        viewPager.setCurrentItem(0);
        //setColor(1);
        viewPager.setOnPageChangeListener(new ViewPager.OnPageChangeListener() {
            @Override
            public void onPageScrolled(int i, float v, int i2) {

            }

            @Override
            public void onPageSelected(int position) {
                setColor(position);
            }

            @Override
            public void onPageScrollStateChanged(int i) {

            }
        });
    }


    private void setColor(int position) {

        for (int i = 0; i < view.length; i++) {
            textView[i].setTextColor(getResources().getColor(R.color.gray_e));
            view[i].setBackgroundColor(getResources().getColor(R.color.title_bg));
        }
        textView[position].setTextColor(getResources().getColor(R.color.white));
        view[position].setBackgroundColor(getResources().getColor(R.color.yellow));
    }

    private RightClickListener listener;

    public void setClickTag(RightClickListener listener) {
        this.listener = listener;
    }
    private Right1ClickListener listener1;

    public void setClickTag1(Right1ClickListener listener1) {
        this.listener1 = listener1;
    }
}