package cn.ctibet.tibetriding.activity;

import android.app.Activity;
import android.content.Context;
import android.os.Bundle;
import android.view.View;
import android.widget.ListView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import cn.ctibet.tibetriding.R;
import cn.ctibet.tibetriding.adapter.AppRouteListAdapter;
import cn.ctibet.tibetriding.bean.SosDataBean;

import java.util.ArrayList;
import java.util.List;

/**
 * 已报名的路线
 * Created by Administrator on 2015/3/9.
 */
public class ApplicationRouteActivity extends Activity implements View.OnClickListener {
    private RelativeLayout backBtn;
    private TextView back_Text;
    private TextView title;
    private ListView listView;
    private Context context;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.application_route_activity);
        context = ApplicationRouteActivity.this;
        initView();
    }

    private void initView() {
        backBtn = (RelativeLayout) findViewById(R.id.head_left);
        back_Text = (TextView) findViewById(R.id.head_left_text_btn);
        back_Text.setBackgroundResource(R.drawable.back);
        title = (TextView) findViewById(R.id.head_title);
        title.setText(getResources().getText(R.string.app_route));
        listView = (ListView) findViewById(R.id.application_route_activity_list);
        backBtn.setOnClickListener(this);

        SosDataBean bean = null;
        List<SosDataBean> list = new ArrayList<SosDataBean>();
        for (int i = 0; i < 5; i++) {
            bean = new SosDataBean();
            bean.nickname = "川藏骑行" + i;
            list.add(bean);
        }
        AppRouteListAdapter adapter = new AppRouteListAdapter(context);
        listView.setAdapter(adapter);
        adapter.addList(list, false);
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.head_left:
                finish();
                break;
        }
    }
}