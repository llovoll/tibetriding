package cn.ctibet.tibetriding.activity;

import android.app.Activity;
import android.content.Context;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import cn.ctibet.tibetriding.R;
import net.tsz.afinal.FinalBitmap;

/**
 * Created by Administrator on 2015/3/23.
 */
public class MaxImgActivity extends Activity {
    private ImageView maxImg;
    private FinalBitmap fb;
    private Context context;
    private String imgUrl = "";
    private LinearLayout view;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.max_img_activity);
        context = MaxImgActivity.this;
        initView();
    }

    private void initView() {
        view = (LinearLayout) findViewById(R.id.max_img_activity_linear);
        if (null != getIntent()) {
            imgUrl = getIntent().getStringExtra("url");
            if (null == imgUrl) {
                imgUrl = "";
            }
        }
        fb = FinalBitmap.create(context);
        fb.configLoadingImage(R.drawable.default_local);
        fb.configLoadfailImage(R.drawable.default_local);

        maxImg = (ImageView) findViewById(R.id.max_img_activity_img);
        fb.display(maxImg, imgUrl);

        view.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                finish();
            }
        });
    }
}