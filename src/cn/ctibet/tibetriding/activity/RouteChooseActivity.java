package cn.ctibet.tibetriding.activity;

import android.app.Activity;
import android.content.Context;
import android.graphics.drawable.BitmapDrawable;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.AnimationUtils;
import android.widget.*;
import cn.ctibet.tibetriding.R;
import cn.ctibet.tibetriding.app.ExitApplication;
import cn.ctibet.tibetriding.bean.*;
import cn.ctibet.tibetriding.db.Advisetime;
import cn.ctibet.tibetriding.db.DailyTrackHistory;
import cn.ctibet.tibetriding.db.Way;
import cn.ctibet.tibetriding.util.*;
import cn.ctibet.tibetriding.view.KCalendar;
import net.tsz.afinal.FinalBitmap;

import java.util.ArrayList;
import java.util.List;

/**
 * 线路选择
 * Created by Administrator on 2015/1/26.
 */
public class RouteChooseActivity extends Activity implements View.OnClickListener {
    private LinearLayout linear1;
    private LinearLayout linear2;
    private LayoutInflater mInflater;
    private Context context;
    private String date = null;
    private RelativeLayout back;
    private TextView back_Text;
    private TextView title;
    private RelativeLayout sureBtn;
    private TextView sureBtn_text;
    private List<WayBean> allList;
    private FinalBitmap fb;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.route_choose_activity);
        ExitApplication.getInstance().addActivity(this);
        context = RouteChooseActivity.this;
        initView();
    }

    private void initView() {
        fb = FinalBitmap.create(context);
        fb.configLoadingImage(R.drawable.default_local);
        fb.configLoadfailImage(R.drawable.default_local);
        mInflater = LayoutInflater.from(context);
        back = (RelativeLayout) findViewById(R.id.head_left);
        back_Text = (TextView) findViewById(R.id.head_left_text_btn);
        back_Text.setBackgroundResource(R.drawable.back);
        title = (TextView) findViewById(R.id.head_title);
        title.setText(getResources().getString(R.string.lxxz));
        back.setOnClickListener(this);

        sureBtn = (RelativeLayout) findViewById(R.id.head_right);
        sureBtn.setVisibility(View.VISIBLE);
        sureBtn_text = (TextView) findViewById(R.id.head_right_text_btn);
        sureBtn_text.setText(getResources().getString(R.string.sure));
        sureBtn_text.setTextColor(getResources().getColor(R.color.blue4));

        linear1 = (LinearLayout) findViewById(R.id.route_choose_activity_linear1);
        linear2 = (LinearLayout) findViewById(R.id.route_choose_activity_linear2);

        RouteBean bean = null;
        List<RouteBean> list = new ArrayList<RouteBean>();
        for (int i = 0; i < 5; i++) {
            bean = new RouteBean();
            bean.name = "川藏骑行" + i;
            list.add(bean);
        }
        Way db = new Way(context);
        Advisetime timeDb = new Advisetime(context);
        allList = db.getAllRidingLines();
        if (null != allList && allList.size() > 0) {
            for (int i = 0; i < allList.size(); i++) {
                allList.get(i).advisetimeList = timeDb.getAllAdvisetimes(allList.get(i).Wayid);
                SysPrintUtil.pt("查询的WayId为",allList.get(i).Wayid+"==="+allList.get(i).advisetimeList.size());
            }
        }

        setDataShow(list);
    }


    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.head_left:
                finish();
                break;
        }
    }

    private void setDataShow(List<RouteBean> list) {
        for (int i = 0; i < list.size(); i++) {
            View view = mInflater.inflate(R.layout.route_choose_item1, null);
            TextView name = (TextView) view.findViewById(R.id.route_choose_item1_name);
            name.setText(list.get(i).name);
            linear1.addView(view);
        }
        if (null != allList && allList.size() > 0) {
            WayBean lineBean = null;
            for (int i = 0; i < allList.size(); i++) {
                lineBean = allList.get(i);
                View view = mInflater.inflate(R.layout.route_choose_item2, null);
                CheckBox checkBox = (CheckBox) view.findViewById(R.id.route_choose_item2_check);
                ImageView imgLine = (ImageView) view.findViewById(R.id.route_choose_item2_img1);
                TextView name = (TextView) view.findViewById(R.id.route_choose_item2_name);
                TextView desc = (TextView) view.findViewById(R.id.route_choose_item2_desc);
                LinearLayout img = (LinearLayout) view.findViewById(R.id.route_choose_item2_img);
                fb.display(imgLine, Configs.HOST_IMG + lineBean.Wayimg);
                name.setText(lineBean.Wayname);
                desc.setText(lineBean.Waydesc);
                final WayBean finalLineBean = lineBean;
                img.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        new PopupWindows(context, view, finalLineBean.advisetimeList);
                    }
                });
                linear2.addView(view);
            }
        }
    }
    private void getRouteData(final String routeid) {
        new Thread(new Runnable() {
            @Override
            public void run() {
                Message msg = new Message();
                String str = "?routeid=" + routeid + TimeUtil.getKeyStr();
                try {
                    String jsonData = SyncHttp.httpGet(Configs.HOST + "Way/getRouteInfo", str);
                    SysPrintUtil.pt("上传到服务器数据为", Configs.HOST + "Way/getRouteInfo" + str);
                    SysPrintUtil.pt("json==获取到的服务器的数据为:", jsonData);
                 /*   roadInfo = JsonUtil.getRouteInfoData(jsonData);
                    if (null != roadInfo) {
                        msg.what = Configs.READ_SUCCESS;
                    } else {
                        msg.what = Configs.READ_FAIL;
                    }*/
                } catch (Exception e) {
                    SysPrintUtil.pt("数据异常", e.toString());
                    e.printStackTrace();
                    msg.what = Configs.READ_ERROR;
                }
                nhandler.sendMessage(msg);
            }
        }).start();
    }

    private Handler nhandler = new Handler() {
        @Override
        public void handleMessage(Message msg) {
            switch (msg.what) {
                case Configs.READ_SUCCESS:
                    break;
                case Configs.READ_FAIL:
                    ToastUtil.showToast(context, "暂无数据", 0);
                    break;
                case Configs.READ_ERROR:
                    ToastUtil.showToast(context, "数据异常", 0);
                    break;
            }
        }
    };

    public class PopupWindows extends PopupWindow {

        public PopupWindows(Context mContext, View parent, List<AdvisetimeBean> advisetimeList) {

            View view = View.inflate(mContext, R.layout.popupwindow_calendar,
                    null);
            view.startAnimation(AnimationUtils.loadAnimation(mContext,
                    R.anim.fade_in));
            LinearLayout ll_popup = (LinearLayout) view
                    .findViewById(R.id.ll_popup);
            ll_popup.startAnimation(AnimationUtils.loadAnimation(mContext,
                    R.anim.push_bottom_in_1));

            setWidth(ViewGroup.LayoutParams.FILL_PARENT);
            setHeight(ViewGroup.LayoutParams.FILL_PARENT);
            setBackgroundDrawable(new BitmapDrawable());
            setFocusable(true);
            setOutsideTouchable(true);
            setContentView(view);
            showAtLocation(parent, Gravity.BOTTOM, 0, 0);
            update();

            final TextView popupwindow_calendar_month = (TextView) view
                    .findViewById(R.id.popupwindow_calendar_month);
            final KCalendar calendar = (KCalendar) view
                    .findViewById(R.id.popupwindow_calendar);
            Button popupwindow_calendar_bt_enter = (Button) view
                    .findViewById(R.id.popupwindow_calendar_bt_enter);

            popupwindow_calendar_month.setText(calendar.getCalendarYear() + "年"
                    + calendar.getCalendarMonth() + "月");

            if (null != date) {
                int years = Integer.parseInt(date.substring(0,
                        date.indexOf("-")));
                int month = Integer.parseInt(date.substring(
                        date.indexOf("-") + 1, date.lastIndexOf("-")));
                popupwindow_calendar_month.setText(years + "年" + month + "月");

                calendar.showCalendar(years, month);
                calendar.setCalendarDayBgColor(date,
                        R.drawable.calendar_date_focused);
            }
            AdvisetimeBean advisetimeBean = null;
            List<String> listStr;
            SysPrintUtil.pt("advisetimeList=====",advisetimeList.size()+"");
            if (null != advisetimeList && advisetimeList.size() > 0) {
                for (int i = 0; i < advisetimeList.size(); i++) {
                    advisetimeBean = advisetimeList.get(i);
                    SysPrintUtil.pt("获取到的年份为111111===",advisetimeBean.Begintime
                            +"===="+advisetimeBean.Endtime);
                    listStr = TimeUtil.getEveryDay(advisetimeBean.Begintime, advisetimeBean.Endtime);
                    calendar.setCalendarDaysBgColor(listStr, getResources().getColor(R.color.title_bg));
                    for (int j = 0; j < listStr.size(); j++) {
                        SysPrintUtil.pt("获取到的年份为===",listStr.get(j)+"===="+i);
                    }

                }
            }
           // calendar.setCalendarDaysBgColor(listStr, getResources().getColor(R.color.title_bg));
     /*       List<String> list = new ArrayList<String>(); //设置标记列表
            list.add("2015-01-25");
            list.add("2015-01-26");
            list.add("2015-01-27");
            calendar.setCalendarDaysBgColor(list, getResources().getColor(R.color.title_bg));

            List<String> list1 = new ArrayList<String>(); //设置标记列表
            list1.add("2015-01-01");
            list1.add("2015-01-02");
            list1.add("2015-01-03");
            calendar.setCalendarDaysBgColor(list1, getResources().getColor(R.color.title_bg));*/
            view.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    dismiss();
                }
            });
            //监听所选中的日期
          /*  calendar.setOnCalendarClickListener(new KCalendar.OnCalendarClickListener() {

                public void onCalendarClick(int row, int col, String dateFormat) {
                    int month = Integer.parseInt(dateFormat.substring(
                            dateFormat.indexOf("-") + 1,
                            dateFormat.lastIndexOf("-")));

                    if (calendar.getCalendarMonth() - month == 1//跨年跳转
                            || calendar.getCalendarMonth() - month == -11) {
                        calendar.lastMonth();

                    } else if (month - calendar.getCalendarMonth() == 1 //跨年跳转
                            || month - calendar.getCalendarMonth() == -11) {
                        calendar.nextMonth();

                    } else {
                        calendar.removeAllBgColor();
                        calendar.setCalendarDayBgColor(dateFormat,
                                R.drawable.calendar_date_focused);
                        date = dateFormat;//最后返回给全局 date
                    }
                }
            });*/

            //监听当前月份
            calendar.setOnCalendarDateChangedListener(new KCalendar.OnCalendarDateChangedListener() {
                public void onCalendarDateChanged(int year, int month) {
                    popupwindow_calendar_month
                            .setText(year + "年" + month + "月");
                }
            });

            //上月监听按钮
            RelativeLayout popupwindow_calendar_last_month = (RelativeLayout) view
                    .findViewById(R.id.popupwindow_calendar_last_month);
            popupwindow_calendar_last_month
                    .setOnClickListener(new View.OnClickListener() {

                        public void onClick(View v) {
                            calendar.lastMonth();
                        }

                    });

            //下月监听按钮
            RelativeLayout popupwindow_calendar_next_month = (RelativeLayout) view
                    .findViewById(R.id.popupwindow_calendar_next_month);
            popupwindow_calendar_next_month
                    .setOnClickListener(new View.OnClickListener() {

                        public void onClick(View v) {
                            calendar.nextMonth();
                        }
                    });

            //关闭窗口
            popupwindow_calendar_bt_enter
                    .setOnClickListener(new View.OnClickListener() {

                        public void onClick(View v) {
                            dismiss();
                        }
                    });
        }
    }

}