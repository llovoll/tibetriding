package cn.ctibet.tibetriding.activity;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.net.Uri;
import android.os.Bundle;
import android.provider.MediaStore;
import android.support.v4.app.FragmentActivity;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;
import cn.ctibet.tibetriding.R;
import cn.ctibet.tibetriding.app.ExitApplication;
import cn.ctibet.tibetriding.bean.DailyTrackHistoryBean;
import cn.ctibet.tibetriding.fragment.CyclingFragment;
import cn.ctibet.tibetriding.fragment.PhoneFragment;
import cn.ctibet.tibetriding.impl.DeleteClickListener;
import cn.ctibet.tibetriding.impl.ImgEditClickListener;
import cn.ctibet.tibetriding.util.BitMapUtil;
import cn.ctibet.tibetriding.util.SdCardUtil;
import cn.ctibet.tibetriding.util.UriToFilePath;
import net.tsz.afinal.FinalBitmap;

/**
 * Created by Administrator on 2015/1/19.
 */
public class ImageChooseActivity extends FragmentActivity implements View.OnClickListener {
    private RelativeLayout back;
    private TextView back_Text;
    private RelativeLayout cyclingBtn;
    private RelativeLayout phoneBtn;
    private FragmentManager ft;
    private FragmentTransaction fm;
    private CyclingFragment cyclingFragment;
    private PhoneFragment phoneFragment;
    private TextView cyclingText;
    private static final int GALLEY = 102;
    private Context context;
    private DailyTrackHistoryBean bean;
    private RelativeLayout rightBtn;
    private TextView rightBtn_Text;
    private boolean isCheck = false;
    private LinearLayout deleteBtn;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.image_choose_activity);
        ExitApplication.getInstance().addActivity(this);
        context = ImageChooseActivity.this;
        initView();
    }

    private void initView() {
        if (null != getIntent()) {
            bean = (DailyTrackHistoryBean) getIntent().getSerializableExtra("bean");
        }
        ft = this.getSupportFragmentManager();
        back = (RelativeLayout) findViewById(R.id.head_left);
        back_Text = (TextView) findViewById(R.id.head_left_text_btn);
        back_Text.setBackgroundResource(R.drawable.back);
        cyclingBtn = (RelativeLayout) findViewById(R.id.cycling_photo_album_btn);
        findViewById(R.id.cycling_photo_album_line).setVisibility(View.GONE);
        phoneBtn = (RelativeLayout) findViewById(R.id.phone_photo_album_btn);
        rightBtn = (RelativeLayout) findViewById(R.id.head_right);
        rightBtn_Text = (TextView) findViewById(R.id.head_right_text_btn);
        rightBtn_Text.setBackgroundResource(R.drawable.edit_btn);
        deleteBtn = (LinearLayout) findViewById(R.id.image_choose_activity_delete);

        back.setOnClickListener(this);
        cyclingBtn.setOnClickListener(this);
        phoneBtn.setOnClickListener(this);
        rightBtn.setOnClickListener(this);
        deleteBtn.setOnClickListener(this);
        loadFragment(1);
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.head_left:
                finish();
                break;
            case R.id.head_right:
                if (!isCheck) {
                    rightBtn_Text.setBackgroundResource(R.drawable.make_sure);
                    isCheck = true;
                    deleteBtn.setVisibility(View.VISIBLE);
                } else {
                    rightBtn_Text.setBackgroundResource(R.drawable.edit_btn);
                    isCheck = false;
                    deleteBtn.setVisibility(View.GONE);
                }
                if (null != imgEditClickListener) {
                    imgEditClickListener.getClickStat(isCheck);
                }
                break;
            case R.id.image_choose_activity_delete:
                deleteClickListener.delClick();
                break;
            case R.id.cycling_photo_album_btn:
                loadFragment(1);
                break;
            case R.id.phone_photo_album_btn:
                if (!SdCardUtil.ExistSDCard()) {
                    Toast.makeText(context, getResources()
                            .getString(R.string.null_sdcrad), Toast.LENGTH_SHORT).show();
                    return;
                }
                Intent intent = new Intent(Intent.ACTION_PICK, null);
                intent.setDataAndType(MediaStore.Images.Media.EXTERNAL_CONTENT_URI, "image/*");
                startActivityForResult(intent, GALLEY);
                break;

        }
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        switch (requestCode) {
            case GALLEY:
                if (null != data) {
                    // Bitmap bitmap = BitMapUtil.compressImg(BitMapUtil.getBitmapFromUri(context, data.getData()), 0.5f);
                    Uri uri = data.getData();
                    String img_path = UriToFilePath.getFilePath(ImageChooseActivity.this, uri);

                    Bundle bundle = new Bundle();
                    bundle.putSerializable("bean", bean);
                    Intent intent = new Intent(context, WaterMarkActivity.class);
                    intent.putExtras(bundle);
                    intent.putExtra("Uri", img_path);
                    startActivity(intent);
                }
                break;
        }
    }

    /**
     * 加载子fragment
     */
    private void loadFragment(int pos) {

        fm = ft.beginTransaction();
        if (null != cyclingFragment) {
            fm.hide(cyclingFragment);
        }
        if (null != phoneFragment) {
            fm.hide(phoneFragment);
        }
        switch (pos) {
            case 1:
                if (null == cyclingFragment) {
                    cyclingFragment = new CyclingFragment(bean, ImageChooseActivity.this);
                    fm.add(R.id.image_choose_activity_fragment, cyclingFragment, "");
                } else {
                    fm.show(cyclingFragment);
                }
                break;
            case 2:
                if (null == phoneFragment) {
                    phoneFragment = new PhoneFragment();
                    fm.add(R.id.image_choose_activity_fragment, phoneFragment, "");
                } else {
                    fm.show(phoneFragment);
                }
                break;
        }
        fm.commit();
    }

    private ImgEditClickListener imgEditClickListener;

    public void setImgClick(ImgEditClickListener imgEditClickListener) {
        this.imgEditClickListener = imgEditClickListener;
    }

    private DeleteClickListener deleteClickListener;

    public void setDelClick(DeleteClickListener deleteClickListener) {
        this.deleteClickListener = deleteClickListener;
    }
}