package cn.ctibet.tibetriding.activity;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.provider.MediaStore;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentActivity;
import android.support.v4.app.FragmentManager;
import android.support.v4.view.ViewPager;
import android.text.Html;
import android.view.View;
import android.view.Window;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;
import cn.ctibet.tibetriding.R;
import cn.ctibet.tibetriding.adapter.SosViewPagerAdapters;
import cn.ctibet.tibetriding.app.ExitApplication;
import cn.ctibet.tibetriding.fragment.MyForHelpFragment;
import cn.ctibet.tibetriding.fragment.SosFragment;
import cn.ctibet.tibetriding.util.SdCardUtil;
import cn.ctibet.tibetriding.util.Utils;
import cn.ctibet.tibetriding.view.ScoreDialog;

import java.util.ArrayList;

/**
 * Created by Administrator on 2015/1/22.
 */
public class SosActivity extends FragmentActivity implements View.OnClickListener {
    private ViewPager viewPager;
    private SosFragment sosFragment = new SosFragment();
    private MyForHelpFragment myForHelpFragment = new MyForHelpFragment();
    private ArrayList<Fragment> fragments;
    private FragmentManager fm;
    private RelativeLayout back;
    private RelativeLayout rela1;
    private TextView title1;
    private View line1;
    private RelativeLayout rela2;
    private TextView title2;
    private View line2;
    private TextView[] textView;
    private View[] view;
    private RelativeLayout phoneBtn;
    private TextView phoneBtn_text;
    private Context context;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.sos_activity);
        ExitApplication.getInstance().addActivity(this);
        context = SosActivity.this;
        initView();
    }

    private void initView() {
        fm = this.getSupportFragmentManager();
        back = (RelativeLayout) findViewById(R.id.head_left);
        phoneBtn = (RelativeLayout) findViewById(R.id.head_right);
        phoneBtn.setVisibility(View.VISIBLE);
        phoneBtn_text = (TextView) findViewById(R.id.head_right_text_btn);
        phoneBtn_text.setBackgroundResource(R.drawable.phone);

        rela1 = (RelativeLayout) findViewById(R.id.cycling_photo_album_btn);
        title1 = (TextView) findViewById(R.id.cycling_photo_album);
        line1 = (View) findViewById(R.id.cycling_photo_album_line);

        rela2 = (RelativeLayout) findViewById(R.id.phone_photo_album_btn);
        title2 = (TextView) findViewById(R.id.phone_photo_album);
        line2 = (View) findViewById(R.id.phone_photo_album_line);

        title1.setText(Html.fromHtml("&nbsp&nbsp SOS &nbsp&nbsp"));
        title2.setText(getResources().getString(R.string.my_for_help));


        viewPager = (ViewPager) findViewById(R.id.sos_activity_viewpager);

        back.setOnClickListener(this);
        phoneBtn.setOnClickListener(this);
        rela1.setOnClickListener(this);
        rela2.setOnClickListener(this);
        setViewpager();
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.head_left:
                finish();
                break;
            case R.id.head_right:
                showDialog();
                break;
            case R.id.cycling_photo_album_btn:
                setColor(0);
                viewPager.setCurrentItem(0);
                break;
            case R.id.phone_photo_album_btn:
                setColor(1);
                viewPager.setCurrentItem(1);
                break;
        }
    }

    private void setViewpager() {
        textView = new TextView[]{title1, title2};
        view = new View[]{line1, line2};
        Fragment[] frags = {sosFragment, myForHelpFragment};
        fragments = new ArrayList<Fragment>();
        for (int i = 0; i < frags.length; i++) {
            fragments.add(frags[i]);
        }

        viewPager.setOffscreenPageLimit(0);
        viewPager.setAdapter(new SosViewPagerAdapters(fm, fragments));
        viewPager.setCurrentItem(0);
        //setColor(1);
        viewPager.setOnPageChangeListener(new ViewPager.OnPageChangeListener() {
            @Override
            public void onPageScrolled(int i, float v, int i2) {

            }

            @Override
            public void onPageSelected(int position) {
                setColor(position);
            }

            @Override
            public void onPageScrollStateChanged(int i) {

            }
        });
    }


    private void setColor(int position) {
        for (int i = 0; i < view.length; i++) {
            textView[i].setTextColor(getResources().getColor(R.color.gray_e));
            view[i].setBackgroundColor(getResources().getColor(R.color.title_bg));
        }
        textView[position].setTextColor(getResources().getColor(R.color.white));
        view[position].setBackgroundColor(getResources().getColor(R.color.yellow));
    }

    private void showDialog() {
        final ScoreDialog dialog = new ScoreDialog(this, R.layout.select_camera_dialog1, R.style.dialog_more_style);
        TextView view1 = (TextView) dialog.findViewById(R.id.camera);
        view1.setText("组织方专业救援：13812345678");
        TextView view2 = (TextView) dialog.findViewById(R.id.album);
        view2.setText("报警电话：110");
        TextView view3 = (TextView) dialog.findViewById(R.id.guiji);
        view3.setText("西藏旅游局：6102356");
        dialog.setParamsBotton();
        dialog.show();
        if (!SdCardUtil.ExistSDCard()) {
            Toast.makeText(context, getResources()
                    .getString(R.string.null_sdcrad), Toast.LENGTH_SHORT).show();
            return;
        }
        view1.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                dialog.dismiss();
                Toast.makeText(context, "未实现", Toast.LENGTH_SHORT).show();
            }
        });
        view2.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                dialog.dismiss();
                Toast.makeText(context, "未实现", Toast.LENGTH_SHORT).show();
            }
        });
        view3.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                dialog.dismiss();
                Toast.makeText(context, "未实现", Toast.LENGTH_SHORT).show();
            }
        });
        dialog.findViewById(R.id.back).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                dialog.dismiss();
            }
        });
        Window dialogWindow = dialog.getWindow();
        dialogWindow.setWindowAnimations(R.style.dialog_more_style);
    }
}