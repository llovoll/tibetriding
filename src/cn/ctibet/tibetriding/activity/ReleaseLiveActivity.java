package cn.ctibet.tibetriding.activity;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.net.Uri;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.os.Parcelable;
import android.provider.MediaStore;
import android.text.TextUtils;
import android.view.View;
import android.widget.*;
import cn.ctibet.tibetriding.R;
import cn.ctibet.tibetriding.app.ExitApplication;
import cn.ctibet.tibetriding.bean.*;
import cn.ctibet.tibetriding.db.DailyTrackHistory;
import cn.ctibet.tibetriding.util.*;

import java.util.ArrayList;
import java.util.List;

/**
 * 发布直播
 * Created by Administrator on 2015/1/20.
 */
public class ReleaseLiveActivity extends Activity implements View.OnClickListener {
    private static final int CAMERA = 101;
    private static final int GALLEY = 102;
    private Context context;
    private RelativeLayout back;
    private TextView back_Text;
    private TextView title;
    private RelativeLayout releaseBtn;
    private TextView releaseBtn_text;
    private EditText editText;
    private ImageView imageView;
    private ImageView photoBtn;
    private ImageView cameraBtn;
    private Uri cameraUri;
    private Bitmap bitmap;
    private Bitmap bitmapWater;
    private DailyTrackHistoryBean bean;
    private DialogShowUtil dialogShowUtil;
    private DailyExeBean stat;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.release_live_activity);
        ExitApplication.getInstance().addActivity(this);
        context = ReleaseLiveActivity.this;
        initView();
    }

    private void initView() {
        if (null != getIntent()) {
            bean = (DailyTrackHistoryBean) getIntent().getSerializableExtra("bean");
        }
        bitmapWater = Configs.bit;
        Configs.bit = null;
        back = (RelativeLayout) findViewById(R.id.head_left);
        back_Text = (TextView) findViewById(R.id.head_left_text_btn);
        back_Text.setBackgroundResource(R.drawable.back);
        title = (TextView) findViewById(R.id.head_title);
        title.setText(getResources().getString(R.string.release_live));
        releaseBtn = (RelativeLayout) findViewById(R.id.head_right);
        releaseBtn.setVisibility(View.VISIBLE);
        releaseBtn_text = (TextView) findViewById(R.id.head_right_text_btn);
        releaseBtn_text.setText(getResources().getString(R.string.release));

        editText = (EditText) findViewById(R.id.release_live_activity_edit);
        imageView = (ImageView) findViewById(R.id.release_live_activity_img);
        photoBtn = (ImageView) findViewById(R.id.release_live_activity_photo);
        cameraBtn = (ImageView) findViewById(R.id.release_live_activity_camera);
        imageView.setImageBitmap(bitmapWater);

        back.setOnClickListener(this);
        releaseBtn.setOnClickListener(this);
        photoBtn.setOnClickListener(this);
        cameraBtn.setOnClickListener(this);
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.head_left:
                finish();
                break;
            case R.id.head_right:
                if (TextUtils.isEmpty(editText.getText().toString().trim())) {
                    ToastUtil.showToast(context, "直播内容不能为空", Toast.LENGTH_SHORT);
                } else if (editText.getText().toString().trim().length() > 140) {
                    ToastUtil.showToast(context, "直播内容不能超过140字", Toast.LENGTH_SHORT);
                } else {
                    getReleaseLiveData();
                }
                break;
            case R.id.release_live_activity_photo:
                if (checkSdCard()) return;

                Intent intent = new Intent(Intent.ACTION_PICK, null);
                intent.setDataAndType(MediaStore.Images.Media.EXTERNAL_CONTENT_URI, "image/*");
                startActivityForResult(intent, GALLEY);
                break;
            case R.id.release_live_activity_camera:
                if (checkSdCard()) return;

                Intent camera = new Intent(
                        MediaStore.ACTION_IMAGE_CAPTURE);
                cameraUri = Utils.createImagePathUri(context);
                camera.putExtra(MediaStore.EXTRA_OUTPUT, cameraUri);
                startActivityForResult(camera, CAMERA);
                break;
        }
    }


    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        switch (requestCode) {
            case CAMERA:
                if (resultCode == Activity.RESULT_OK
                        && null == data) {
                    bitmapWater = Utils.compressImage(UriToFilePath.getFilePath(ReleaseLiveActivity.this, cameraUri));
                    imageView.setImageBitmap(bitmapWater);
                }
                break;
            case GALLEY:
                if (null != data) {
                    bitmapWater = Utils.compressImage(UriToFilePath.getFilePath(ReleaseLiveActivity.this, data.getData()));
                    imageView.setImageBitmap(bitmapWater);
                }
                break;
        }
    }

    private boolean checkSdCard() {
        if (!SdCardUtil.ExistSDCard()) {
            Toast.makeText(context, getResources()
                    .getString(R.string.null_sdcrad), Toast.LENGTH_SHORT).show();
            return true;
        }
        return false;
    }

    private void getReleaseLiveData() {
        dialogShowUtil = new DialogShowUtil(context, "正在发布，请稍后");
        dialogShowUtil.dialogShow();
        new Thread(new Runnable() {
            @Override
            public void run() {
                Message msg = new Message();
                Parameter parameter = null;
                List<Parameter> list = new ArrayList<Parameter>();
                String[] name = {"trackhistoryid", "livecontent", "city",
                        "xian", "livelat", "livelng", "liveimg", "key", "time"};
                LLAEntity entity = LLAUtils.parseLLAStr(bean.Trackset);
                String[] value = {bean.Trackhistoryid, editText.getText().toString().trim(),
                        "四川省", "成都市", entity.getLat() + "", entity.getLng() + "",
                        BitmapTransformUtil.bitmaptoBase64(bitmapWater), TimeUtil.getKey(), TimeUtil.getTime()};
                for (int i = 0; i < name.length; i++) {
                    parameter = new Parameter();
                    parameter.setName(name[i]);
                    parameter.setValue(value[i]);
                    list.add(parameter);
                }

                try {
                    String jsonData = SyncHttp.httpPost(Configs.HOST + "Live/PublishLive", list);
                    SysPrintUtil.pt("上传到服务器数据为", Configs.HOST + "Live/PublishLive");
                    SysPrintUtil.pt("json==获取到的服务器的数据为:", jsonData);
                    stat = JsonUtil.getReleaseLiveStat(jsonData);
                    if (stat.code.equals("200")) {
                        msg.what = Configs.READ_SUCCESS;
                    } else {
                        msg.what = Configs.READ_FAIL;
                    }
                } catch (Exception e) {
                    SysPrintUtil.pt("数据异常", e.toString());
                    e.printStackTrace();
                    msg.what = Configs.READ_ERROR;
                }
                nhandler.sendMessage(msg);
            }
        }).start();
    }

    private Handler nhandler = new Handler() {
        @Override
        public void handleMessage(Message msg) {
            dialogShowUtil.dialogDismiss();
            switch (msg.what) {
                case Configs.READ_SUCCESS:
                    startActivity(new Intent(context, ReleaseSuccessActivity.class));
                    break;
                case Configs.READ_FAIL:
                    ToastUtil.showToast(context, stat.msg, Toast.LENGTH_SHORT);
                    break;
                case Configs.READ_ERROR:
                    ToastUtil.showToast(context, "数据异常", Toast.LENGTH_SHORT);
                    break;
            }
        }
    };
}