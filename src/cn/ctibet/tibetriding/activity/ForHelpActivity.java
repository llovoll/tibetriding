package cn.ctibet.tibetriding.activity;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.net.Uri;
import android.os.Bundle;
import android.os.Environment;
import android.os.Handler;
import android.os.Message;
import android.provider.MediaStore;
import android.view.LayoutInflater;
import android.view.View;
import android.view.Window;
import android.widget.*;
import cn.ctibet.tibetriding.R;
import cn.ctibet.tibetriding.app.ExitApplication;
import cn.ctibet.tibetriding.bean.*;
import cn.ctibet.tibetriding.impl.LocationListener;
import cn.ctibet.tibetriding.util.*;
import cn.ctibet.tibetriding.view.MyLayout;
import cn.ctibet.tibetriding.view.ScoreDialog;
import com.amap.api.location.AMapLocation;

import java.io.File;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by Administrator on 2015/1/22.
 */
public class ForHelpActivity extends BaseActivity implements View.OnClickListener, LocationListener {
    private static final int CAMERA = 101;
    private static final int GALLEY = 102;
    private LinearLayout.LayoutParams parm;
    private Context context;
    private LayoutInflater inflater;
    private View addimgView;
    private MyLayout myLayout;
    private ArrayList<PsClassImg> imglist = new ArrayList<PsClassImg>();
    private String picpath = "";
    private Uri cameraUri;
    private RelativeLayout back;
    private TextView back_Text;
    private TextView title;
    private RelativeLayout releaseBtn;
    private TextView releaseBtn_text;
    private DialogShowUtil dialogShowUtil;
    private String zipname;
    private String sd;
    private String sd1;
    private UserInfo userInfo = null;
    private String wayid = "32";
    private EditText sosEdit;
    private double geoLat;
    private double geoLng;
    private DailyExeBean bean;
    private List<Bitmap> bitList = new ArrayList<Bitmap>();

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.for_help_activity);
        ExitApplication.getInstance().addActivity(this);
        context = ForHelpActivity.this;
        inflater = LayoutInflater.from(context);
        init();
        initView();
    }

    private void initView() {
        userInfo = UserInfoUtil.getUserInfo(context);
        back = (RelativeLayout) findViewById(R.id.head_left);
        back_Text = (TextView) findViewById(R.id.head_left_text_btn);
        back_Text.setBackgroundResource(R.drawable.back);
        title = (TextView) findViewById(R.id.head_title);
        title.setText(getResources().getString(R.string.j_help));
        releaseBtn = (RelativeLayout) findViewById(R.id.head_right);
        releaseBtn.setVisibility(View.VISIBLE);
        releaseBtn_text = (TextView) findViewById(R.id.head_right_text_btn);
        releaseBtn_text.setText(getResources().getString(R.string.release));
        releaseBtn.setOnClickListener(this);

        sosEdit = (EditText) findViewById(R.id.release_live_activity_edit);

        back.setOnClickListener(this);

        myLayout = (MyLayout) findViewById(R.id.layout);
        @SuppressWarnings("deprecation")
        int picWidth = (int) ((getWindowManager().getDefaultDisplay()
                .getWidth() - 2 * getResources().getDimension(
                R.dimen.k_line_space)) / 4);
        parm = new LinearLayout.LayoutParams(picWidth, picWidth);
        addimgView = inflater.inflate(R.layout.addimg_item_layout, null);
        ImageView img = (ImageView) addimgView.findViewById(R.id.img);
        img.setBackgroundResource(R.drawable.btn_addimg_selector);
        RelativeLayout layout = (RelativeLayout) addimgView
                .findViewById(R.id.imglayout);
        layout.setLayoutParams(parm);
        addimgView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (imglist.size() > 5) {
                    Toast.makeText(context, "亲，只能添加6张图片O(∩_∩)O~~", Toast.LENGTH_SHORT).show();
                    return;
                }
                showDialog();
            }
        });

        setImglayout();

        dialogShowUtil = new DialogShowUtil(context, "正在上传，请稍后...");

    }

    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.head_left:
                finish();
                break;
            case R.id.head_right:
                if (sosEdit.getText().toString().trim().equals("")) {
                    ToastUtil.showToast(context, "求助信息不能为空", 0);
                } else {
                    sendZip();
                }
                break;
        }
    }

    private void setImglayout() {
        if (!"".equals(picpath)) {
            PsClassImg img = new PsClassImg();
            img.imgpath = picpath;
            imglist.add(img);
            picpath = "";
        }
        myLayout.removeAllViews();
        bitList.clear();
        for (int i = 0; i < imglist.size(); i++) {
            View item = inflater.inflate(R.layout.addimg_item_layout, null);
            ImageView imgImageView = (ImageView) item.findViewById(R.id.img);

            Bitmap scaleBmp = Utils.compressImage(imglist.get(i).imgpath);
            bitList.add(scaleBmp);
            imgImageView
                    .setImageBitmap(scaleBmp);

            RelativeLayout layout = (RelativeLayout) item
                    .findViewById(R.id.imglayout);
            layout.setLayoutParams(parm);
            ImageView delImageView = (ImageView) item.findViewById(R.id.delete);
            delImageView.setVisibility(View.VISIBLE);
            delImageView.setTag(i);
            delImageView.setOnClickListener(new View.OnClickListener() {

                @Override
                public void onClick(View v) {
                    final int pos = (Integer) v.getTag();
                    imglist.remove(pos);
                    setImglayout();
                }
            });
            myLayout.addView(item);
        }
        myLayout.addView(addimgView);
    }


    private void showDialog() {
        final ScoreDialog dialog = new ScoreDialog(this, R.layout.select_camera_dialog1, R.style.dialog_more_style);
        dialog.setParamsBotton();
        dialog.show();
        if (!SdCardUtil.ExistSDCard()) {
            Toast.makeText(context, getResources()
                    .getString(R.string.null_sdcrad), Toast.LENGTH_SHORT).show();
            return;
        }
        dialog.findViewById(R.id.camera).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                dialog.dismiss();
                Intent camera = new Intent(
                        MediaStore.ACTION_IMAGE_CAPTURE);
                cameraUri = Utils.createImagePathUri(context);
                camera.putExtra(MediaStore.EXTRA_OUTPUT, cameraUri);
                startActivityForResult(camera, CAMERA);
            }
        });
        dialog.findViewById(R.id.album).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                dialog.dismiss();
                Intent intent = new Intent(Intent.ACTION_PICK, null);
                intent.setDataAndType(MediaStore.Images.Media.EXTERNAL_CONTENT_URI, "image/*");
                startActivityForResult(intent, GALLEY);
            }
        });
        dialog.findViewById(R.id.guiji).setVisibility(View.GONE);
        dialog.findViewById(R.id.select_line).setVisibility(View.GONE);
        dialog.findViewById(R.id.back).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                dialog.dismiss();
            }
        });
        Window dialogWindow = dialog.getWindow();
        dialogWindow.setWindowAnimations(R.style.dialog_more_style);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        // TODO Auto-generated method stub

        switch (requestCode) {
            case CAMERA:
                if (resultCode == Activity.RESULT_OK
                        && null == data) {
                    picpath = Utils.uriToPath(context, cameraUri);
                    setImglayout();
                }
                break;
            case GALLEY:
                if (null != data) {
                    picpath = Utils.uriToPath(context, data.getData());
                    setImglayout();
                }
                break;
        }
        super.onActivityResult(requestCode, resultCode, data);
    }

    private void sendZip() {
        dialogShowUtil.dialogShow();
        new Thread(new Runnable() {

            @Override
            public void run() {
                Message msg = new Message();
                try {
                    if (imglist.size() > 0) {
                        zipFile();
                    }
                    List<Parameter> params = new ArrayList<Parameter>();
                    params.add(new Parameter("uid", "" + userInfo.userId));
                    params.add(new Parameter("wayid", wayid));
                    params.add(new Parameter("soscontent", sosEdit.getText().toString().trim()));
                    params.add(new Parameter("soslat", geoLat + ""));
                    params.add(new Parameter("soslng", geoLng + ""));
                    params.add(new Parameter("upload", zipname));
                    String result = new SyncHttp().postfile(Configs.HOST + "Isos/addsosmsg", sd1,
                            params);
                    SysPrintUtil.pt("上传文件====", result);
                    bean = JsonUtil.getReleaseLiveStat(result);
                    if (null != bean && bean.msg.equals("200")) {
                        msg.what = Configs.READ_SUCCESS;
                    } else {
                        msg.what = Configs.READ_FAIL;
                    }
                } catch (Exception e) {
                    SysPrintUtil.pt("数据异常", e.toString());
                    e.printStackTrace();
                    msg.what = Configs.READ_ERROR;
                }
                handler.sendMessage(msg);
            }
        }).start();
    }

    private Handler handler = new Handler() {
        public void handleMessage(Message msg) {
            dialogShowUtil.dialogDismiss();
            switch (msg.what) {
                case Configs.READ_SUCCESS:
                    ToastUtil.showToast(context, "上传成功", 0);
                    finish();
                    break;
                case Configs.READ_FAIL:
                    ToastUtil.showToast(context, bean.msg, 0);
                    break;
                case Configs.READ_ERROR:
                    ToastUtil.showToast(context, "数据异常", 0);
                    break;
            }
        }

        ;
    };

    private void zipFile() throws Exception {
        zipname = "" + System.currentTimeMillis();
        sd = Environment.getExternalStorageDirectory() + "/xzqx/img/"
                + zipname;
        sd1 = Environment.getExternalStorageDirectory() + "/xzqx/img/"
                + zipname + ".zip";

     /*   Bitmap map = null;
        String path = "";
        for (int j = 0; j < imglist.size(); j++) {
            path = imglist.get(j).imgpath;
            // if (null != map)
            // map.recycle();
            // BitmapFactory.Options option = new BitmapFactory.Options();
            // option.inJustDecodeBounds = true;
            // map = BitmapFactory.decodeFile(path, option);
            // int h = option.outHeight;
            // int w = option.outWidth;
            // if (h > 100 || w > 100) {
            // BitmapFactory.Options o = new BitmapFactory.Options();
            // o.inSampleSize = 5;
            // map = BitmapFactory.decodeFile(path, o);
            // } else {
            // map = BitmapFactory.decodeFile(path);
            // }
            map = Utils.compressImage(path);
            ImageUtil.saveBitmapToFile(map, sd,
                    path.substring(path.lastIndexOf("/") + 1, path.length()));
        }
        ZipUtil.ZipFolder(sd, sd1);*/

        //String pathString = this.getCacheDir().toString();
        String pathString = sd;
        String archiveString = sd1;
        File zipFile = new File(pathString);
        if (!zipFile.exists()) {
            zipFile.mkdir();
        }
        String[] fileSrcStrings = new String[imglist.size()];
        for (int j = 0; j < imglist.size(); j++) {
            File fileOneFile = new File(imglist.get(j).imgpath);
            if (!fileOneFile.exists()) {
                fileOneFile.mkdir();
            }
            String name = "/image" + j + ".jpeg";
            BitmapUtils.saveBitmap(bitList.get(j), sd, name);
            fileSrcStrings[j] = sd + name;
        }
        if (imglist.size() != 0) {
            ZipControl mZipControl = new ZipControl();
            String commentString = "Androi_Java_Zip_test.";// 压缩包注释
            mZipControl.writeByApacheZipOutputStream(fileSrcStrings, archiveString,
                    commentString);
        }

        bean.zipname = zipname;
        if (null != sd1 && imglist.size() != 0) {
            bean.pic = sd1;
        } else {
            bean.pic = "";
        }
    }

    @Override
    public void getLocationSuccess(AMapLocation aMapLocation) {
        geoLat = aMapLocation.getLatitude();
        geoLng = aMapLocation.getLongitude();
    }

    @Override
    public void getLocationFail(AMapLocation aMapLocation) {
        ToastUtil.showToast(context, "定位失败", 0);
    }
}