package cn.ctibet.tibetriding.activity;

import android.annotation.TargetApi;
import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.drawable.BitmapDrawable;
import android.os.Build;
import android.os.Bundle;
import android.support.v4.view.ViewPager;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.*;
import cn.ctibet.tibetriding.R;
import cn.ctibet.tibetriding.adapter.WaterMarkPagerAdapter;
import cn.ctibet.tibetriding.app.ExitApplication;
import cn.ctibet.tibetriding.bean.Configs;
import cn.ctibet.tibetriding.bean.DailyTrackHistoryBean;
import cn.ctibet.tibetriding.bean.ScreenSize;
import cn.ctibet.tibetriding.util.*;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Administrator on 2015/1/20.
 */
public class WaterMarkActivity extends Activity implements View.OnClickListener {
    private RelativeLayout back;
    private TextView back_Text;
    private TextView title;
    private RelativeLayout shareBtn;
    private TextView shareBtn_text;
    private ViewPager viewPager;
    private LinearLayout linearGroup;
    private String uriStr;
    private Context context;
    private LayoutInflater mInflater;
    private RelativeLayout linearRela;
    private ScreenSize screenSize;
    private String img_path;
    private int pos = 0;
    private Bitmap[] bit;
    private DailyTrackHistoryBean bean;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.water_mark_activity);
        ExitApplication.getInstance().addActivity(this);
        context = WaterMarkActivity.this;
        initView();
    }

    @TargetApi(Build.VERSION_CODES.JELLY_BEAN)
    private void initView() {
        if (null != getIntent()) {
            bean = (DailyTrackHistoryBean) getIntent().getSerializableExtra("bean");
            img_path = getIntent().getStringExtra("Uri");
            if (null == img_path) {
                img_path = "";
            }
        }
        screenSize = ScreenSizeUtil.getScreenSize(context);
        mInflater = LayoutInflater.from(context);
        back = (RelativeLayout) findViewById(R.id.head_left);
        back_Text = (TextView) findViewById(R.id.head_left_text_btn);
        back_Text.setBackgroundResource(R.drawable.back);
        title = (TextView) findViewById(R.id.head_title);
        title.setText(getResources().getString(R.string.yulan));
        shareBtn = (RelativeLayout) findViewById(R.id.head_right);
        shareBtn.setVisibility(View.VISIBLE);
        shareBtn_text = (TextView) findViewById(R.id.head_right_text_btn);
        shareBtn_text.setBackgroundResource(R.drawable.share2);
        back.setOnClickListener(this);
        shareBtn.setOnClickListener(this);

        viewPager = (ViewPager) findViewById(R.id.water_mark_activity_viewPager);
        linearGroup = (LinearLayout) findViewById(R.id.water_mark_activity_linear);
        linearRela = (RelativeLayout) findViewById(R.id.water_mark_activity_rela);
        final List<ImageView> imageViews2 = new ArrayList<ImageView>();

        //Bitmap bitmap1 = Utils.compressImage(img_path);   //获取原图（压缩）
        Bitmap bitmap1 = Utils.compressImage(RotateBitmapUtil.rotateMap(img_path));   //获取原图（压缩）
        View viewMark = mInflater.inflate(R.layout.water_mark_item1, null);
        RelativeLayout markRela = (RelativeLayout) viewMark.findViewById(R.id.water_mark_item1_rela);
        markRela.setBackground(new BitmapDrawable(bitmap1));
        //水印2图
        Bitmap bitmap2 = ViewToBitMap.getBitmapFromView(viewMark, screenSize.screenW, (int) (screenSize.screenH - getResources().getDimension(R.dimen.title_h)));
        View view = null;
        bit = new Bitmap[]{bitmap1, bitmap2};
        List<View> views = new ArrayList<View>();
        for (int i = 0; i < bit.length; i++) {
            ImageView imageView = new ImageView(context);
            RadioGroup.LayoutParams params2 = new RadioGroup.LayoutParams(18, 18);
            params2.setMargins(5, 0, 5, 0);
            imageView.setLayoutParams(params2);
            if (i == 0) {
                imageView.setBackgroundResource(R.drawable.dot_white);
            } else {
                imageView.setBackgroundResource(R.drawable.dot_transparent);
            }
            imageViews2.add(imageView);
            linearGroup.addView(imageView);

            view = mInflater.inflate(R.layout.water_mark_viewpager_item, null);
            ImageView imageView1 = (ImageView) view.findViewById(R.id.water_mark_viewPager_item);
            imageView1.setBackground(new BitmapDrawable(bit[i]));
            views.add(view);
        }

        WaterMarkPagerAdapter adapter = new WaterMarkPagerAdapter(views);
        viewPager.setAdapter(adapter);
        viewPager.setCurrentItem(0);
        viewPager.setOffscreenPageLimit(1);

        viewPager.setOnPageChangeListener(new ViewPager.OnPageChangeListener() {
            @Override
            public void onPageScrolled(int i, float v, int i1) {

            }

            @Override
            public void onPageSelected(int position) {
                for (int i = 0; i < imageViews2.size(); i++) {
                    if (i == position % imageViews2.size()) {
                        pos = i;
                        if (i < imageViews2.size())
                            imageViews2.get(i)
                                    .setBackgroundResource(R.drawable.dot_white);
                    } else {
                        if (i < imageViews2.size())
                            imageViews2.get(i)
                                    .setBackgroundResource(R.drawable.dot_transparent);
                    }
                }
            }

            @Override
            public void onPageScrollStateChanged(int i) {

            }
        });
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.head_left:
                finish();
                break;
            case R.id.head_right:
                SysPrintUtil.pt("滚动到的位子为", pos + "");
                Configs.bit = bit[pos];

                Bundle bundle = new Bundle();
                bundle.putSerializable("bean", bean);
                Intent intent=new Intent(context,ReleaseLiveActivity.class);
                intent.putExtras(bundle);
                startActivity(intent);
                break;
        }
    }
}