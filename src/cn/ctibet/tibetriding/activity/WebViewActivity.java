package cn.ctibet.tibetriding.activity;

import android.app.Activity;
import android.content.Context;
import android.os.Bundle;
import android.view.View;
import android.webkit.WebView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import cn.ctibet.tibetriding.R;

/**
 * Created by Administrator on 2015/2/4.
 */
public class WebViewActivity extends Activity implements View.OnClickListener {
    private Context context;
    private RelativeLayout back;
    private TextView back_Text;
    private TextView title;
    private WebView webView;
    private String webUrl = "";

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.web_view_activity);
        context = WebViewActivity.this;
        initView();
    }

    private void initView() {
        if (null != getIntent()) {
            webUrl = getIntent().getStringExtra("webUrl");
            if (null == webUrl) {
                webUrl = "";
            }
        }
        back = (RelativeLayout) findViewById(R.id.head_left);
        back_Text = (TextView) findViewById(R.id.head_left_text_btn);
        back_Text.setBackgroundResource(R.drawable.back);
        title = (TextView) findViewById(R.id.head_title);
        title.setText(getResources().getString(R.string.lj));
        back.setOnClickListener(this);

        webView = (WebView) findViewById(R.id.web_view_activity_web);
        webView.loadUrl(webUrl);
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.head_left:
                finish();
                break;
        }
    }
}