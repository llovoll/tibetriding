package cn.ctibet.tibetriding.activity;

import android.app.Activity;
import android.content.Context;
import android.os.Bundle;
import android.view.View;
import android.widget.RelativeLayout;
import android.widget.TextView;
import cn.ctibet.tibetriding.R;

/**
 * Created by Administrator on 2015/3/23.
 */
public class AccountSetActivity extends Activity implements View.OnClickListener {
    private Context context;
    private RelativeLayout back;
    private TextView back_Text;
    private TextView title;
    private RelativeLayout sureBtn;
    private TextView sureBtn_text;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.account_set_activity);
        context = AccountSetActivity.this;
        initView();
    }

    private void initView() {
        back = (RelativeLayout) findViewById(R.id.head_left);
        back_Text = (TextView) findViewById(R.id.head_left_text_btn);
        back_Text.setBackgroundResource(R.drawable.back);
        title = (TextView) findViewById(R.id.head_title);
        title.setText(getResources().getString(R.string.account_set));
        back.setOnClickListener(this);

        sureBtn = (RelativeLayout) findViewById(R.id.head_right);
        sureBtn.setVisibility(View.VISIBLE);
        sureBtn_text = (TextView) findViewById(R.id.head_right_text_btn);
        sureBtn_text.setText(getResources().getString(R.string.sure1));
        sureBtn_text.setTextColor(getResources().getColor(R.color.blue4));
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.head_left:
                finish();
                break;
        }
    }
}