package cn.ctibet.tibetriding.activity;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.drawable.BitmapDrawable;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.view.animation.ScaleAnimation;
import android.view.animation.TranslateAnimation;
import android.widget.*;
import cn.ctibet.tibetriding.R;
import cn.ctibet.tibetriding.app.ExitApplication;
import cn.ctibet.tibetriding.bean.Configs;
import cn.ctibet.tibetriding.bean.LLAEntity;
import cn.ctibet.tibetriding.bean.RouteInfoBean;
import cn.ctibet.tibetriding.util.*;
import com.amap.api.maps.AMap;
import com.amap.api.maps.CameraUpdateFactory;
import com.amap.api.maps.MapView;
import com.amap.api.maps.UiSettings;
import com.amap.api.maps.model.LatLng;
import com.amap.api.maps.model.LatLngBounds;
import com.amap.api.maps.model.PolygonOptions;
import com.amap.api.maps.model.PolylineOptions;

import java.util.ArrayList;
import java.util.List;

/**
 * 浏览路书
 * Created by Administrator on 2015/1/26.
 */
public class RoadBookActivity extends Activity implements View.OnClickListener {
    private LinearLayout newsView;
    private TextView pullBtn;
    private boolean viewShow = false;
    private Context context;
    private LinearLayout linearView;
    private MapView mapView;
    private AMap aMap;
    private UiSettings mUiSettings;
    private RelativeLayout back;
    private TextView back_Text;
    private TextView title;
    private RelativeLayout sosBtn;
    private TextView sosBtn_text;
    private ImageView downBtn;
    private PopupWindow popupWindow;
    private RouteInfoBean roadInfo;
    private PolylineOptions options;
    private LatLngBounds.Builder bounds = new LatLngBounds.Builder();

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.road_book_activity);
        ExitApplication.getInstance().addActivity(this);
        context = RoadBookActivity.this;
        mapView = (MapView) findViewById(R.id.mapView);
        mapView.onCreate(savedInstanceState);// 必须要写
        initView();
    }

    private void initView() {
        back = (RelativeLayout) findViewById(R.id.head_left);
        back_Text = (TextView) findViewById(R.id.head_left_text_btn);
        back_Text.setBackgroundResource(R.drawable.back);
        title = (TextView) findViewById(R.id.head_title);
        title.setText(getResources().getString(R.string.road_book));
        back.setOnClickListener(this);

        sosBtn = (RelativeLayout) findViewById(R.id.head_right);
        sosBtn.setVisibility(View.VISIBLE);
        sosBtn_text = (TextView) findViewById(R.id.head_right_text_btn);
        sosBtn_text.setBackgroundResource(R.drawable.journey);

        newsView = (LinearLayout) findViewById(R.id.road_book_activity_news);
        linearView = (LinearLayout) findViewById(R.id.road_book_activity_line);
        pullBtn = (TextView) findViewById(R.id.road_book_activity_pull);
        downBtn = (ImageView) findViewById(R.id.road_book_activity_down);

        if (aMap == null) {
            aMap = mapView.getMap();
        }
        mUiSettings = aMap.getUiSettings();
        mUiSettings.setZoomControlsEnabled(false);//隐藏缩放按钮

        back.setOnClickListener(this);
        pullBtn.setOnClickListener(this);
        sosBtn.setOnClickListener(this);
        downBtn.setOnClickListener(this);
    }

    /**
     * 方法必须重写
     */
    @Override
    protected void onResume() {
        super.onResume();
        mapView.onResume();
    }

    /**
     * 方法必须重写
     */
    @Override
    protected void onPause() {
        super.onPause();
        mapView.onPause();
    }

    /**
     * 方法必须重写
     */
    @Override
    protected void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
        mapView.onSaveInstanceState(outState);
    }

    /**
     * 方法必须重写
     */
    @Override
    protected void onDestroy() {
        super.onDestroy();
        mapView.onDestroy();
    }


    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.head_left:
                finish();
                break;
            case R.id.road_book_activity_pull:
                if (!viewShow) {
                    viewShow = true;
                    /*Animation animation = AnimationUtils.loadAnimation(
                            context, R.anim.dialog_enter);
                    linearView.setAnimation(animation);*/
                    newsView.setVisibility(View.VISIBLE);
                    pullBtn.setBackgroundResource(R.drawable.pull_down_selector);
                } else {
                    viewShow = false;
                   /* Animation animation = AnimationUtils.loadAnimation(
                            context, R.anim.dialog_exit);
                    linearView.setAnimation(animation);*/
                    newsView.setVisibility(View.GONE);
                    pullBtn.setBackgroundResource(R.drawable.pull_up_selector);

                }
                break;
            case R.id.head_right:
                startActivityForResult(new Intent(context, RoadLineActivity.class), 100);
                if (Build.VERSION.SDK_INT > 5) {
                    overridePendingTransition(R.anim.zoomlrin, R.anim.zoomlrout);
                }
                break;
            case R.id.road_book_activity_down:
                initPopWindow();
                popupWindow.showAtLocation(view, Gravity.CENTER, 0, 0);
                break;
        }
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        SysPrintUtil.pt(requestCode + "", "请求码");
        if (requestCode == 100 && null != data) {
            String id = data.getStringExtra("id");
            getRouteInfo(id);
        }
        super.onActivityResult(requestCode, resultCode, data);
    }

    private void initPopWindow() {
        View view = LayoutInflater.from(context).inflate(
                R.layout.pop_view, null);
        TextView title = (TextView) view.findViewById(R.id.pop_view_title);
        TextView content = (TextView) view.findViewById(R.id.pop_view_content);
        TextView sureBtn = (TextView) view.findViewById(R.id.pop_view_sure);
        TextView errorBtn = (TextView) view.findViewById(R.id.pop_view_touch_error);
        title.setText("是否下载离线路书？");
        content.setText("无需网络，随时浏览，省钱省电省流量。建议在wifi环境下载。");
        errorBtn.setText("取消");
        sureBtn.setText("下载");
        popupWindow = new PopupWindow(view, LinearLayout.LayoutParams.FILL_PARENT,
                LinearLayout.LayoutParams.FILL_PARENT, true);
        popupWindow.setContentView(view);
        popupWindow.setFocusable(true);
        popupWindow.setOutsideTouchable(false);
        popupWindow.setBackgroundDrawable(new BitmapDrawable());
        view.setFocusable(true);
        view.setFocusableInTouchMode(true);
        view.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (null != popupWindow) {
                    popupWindow.dismiss();
                }
            }
        });
    }

    private void getRouteInfo(final String routeid) {
        new Thread(new Runnable() {
            @Override
            public void run() {
                Message msg = new Message();
                String str = "?routeid=" + routeid + TimeUtil.getKeyStr();
                try {
                    String jsonData = SyncHttp.httpGet(Configs.HOST + "Way/getRouteInfo", str);
                    SysPrintUtil.pt("上传到服务器数据为", Configs.HOST + "Way/getRouteInfo" + str);
                    SysPrintUtil.pt("json==获取到的服务器的数据为:", jsonData);
                    roadInfo = JsonUtil.getRouteInfoData(jsonData);
                    if (null != roadInfo) {
                        msg.what = Configs.READ_SUCCESS;
                    } else {
                        msg.what = Configs.READ_FAIL;
                    }
                } catch (Exception e) {
                    SysPrintUtil.pt("数据异常", e.toString());
                    e.printStackTrace();
                    msg.what = Configs.READ_ERROR;
                }
                nhandler.sendMessage(msg);
            }
        }).start();
    }

    private Handler nhandler = new Handler() {
        @Override
        public void handleMessage(Message msg) {
            switch (msg.what) {
                case Configs.READ_SUCCESS:
                    List<LLAEntity> list = LLAUtils.parseTrackset1(roadInfo.trackset);
                    setMapLineShow(list);
                    break;
                case Configs.READ_FAIL:
                    ToastUtil.showToast(context, "暂无数据", 0);
                    break;
                case Configs.READ_ERROR:
                    ToastUtil.showToast(context, "数据异常", 0);
                    break;
            }
        }
    };

    private void setMapLineShow(List<LLAEntity> list) {
        List<LatLng> latLngList = new ArrayList<LatLng>();
        options = null;
        options = new PolylineOptions();
        options.color(Color.rgb(67, 214, 233));
        options.width(getResources().getDimension(R.dimen.lines));
        options.setDottedLine(false);
        aMap.clear();
        if (null != list && list.size() > 0) {
            for (int i = 0; i < list.size(); i++) {
                //SysPrintUtil.pt("获取到的经纬度数据为",list.get(i).getLat()+list.get(i).getLng()+"");
                LatLng latLng = new LatLng(list.get(i).getLat(),
                        list.get(i).getLng());
                LatLngBounds b = bounds.include(latLng).build();
                aMap.moveCamera(CameraUpdateFactory.newLatLngBounds(b, 200));
                latLngList.add(latLng);
            }
        }
        options.addAll(latLngList);
        aMap.addPolyline(options);
    }
}