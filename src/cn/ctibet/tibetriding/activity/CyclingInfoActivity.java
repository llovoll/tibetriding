package cn.ctibet.tibetriding.activity;

import android.app.Activity;
import android.content.Context;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentActivity;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;
import android.support.v4.view.ViewPager;
import android.util.TypedValue;
import android.view.View;
import android.widget.RelativeLayout;
import android.widget.TextView;
import cn.ctibet.tibetriding.R;
import cn.ctibet.tibetriding.fragment.CurrItemFragment;
import cn.ctibet.tibetriding.view.PagerSlidingTabStrip;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Administrator on 2015/3/10.
 */
public class CyclingInfoActivity extends FragmentActivity {
    private Context context;
    private RelativeLayout back;
    private TextView back_text;
    private TextView title;
    private PagerSlidingTabStrip tabs;
    private ViewPager pager;
    private List<String> titleList = new ArrayList<String>();
    private MyPagerAdapter adapter;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.cycling_info_activity);
        context=CyclingInfoActivity.this;
        initView();
    }

    private void initView() {
        back = (RelativeLayout) findViewById(R.id.head_left);
        back_text = (TextView) findViewById(R.id.head_left_text_btn);
        back_text.setBackgroundResource(R.drawable.home_menu);
        title = (TextView) findViewById(R.id.head_title);
        title.setText(getResources().getString(R.string.cyc_info));
        back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                finish();
            }
        });

        tabs = (PagerSlidingTabStrip) findViewById(R.id.curriculum_tabs);
        tabs.setIndicatorHeight(6);
        tabs.setIndicatorColor(getResources().getColor(R.color.yellow));
        pager = (ViewPager) findViewById(R.id.pager);
        adapter = new MyPagerAdapter(getSupportFragmentManager());
        final int pageMargin = (int) TypedValue.applyDimension(
                TypedValue.COMPLEX_UNIT_DIP, 4, getResources()
                        .getDisplayMetrics());
        pager.setPageMargin(pageMargin);
        loadData();
    }


    private void loadData() {
        String[] str = {"骑行新闻", "精彩线路", "骑行装备", "骑行日记", "骑行日记",
                "骑行日记", "骑行日记",};
        for (int i = 0; i < str.length; i++) {
            titleList.add(str[i]);
        }
        pager.setAdapter(adapter);
        pager.setOffscreenPageLimit(1);
        tabs.setViewPager(pager);
    }

    public class MyPagerAdapter extends FragmentPagerAdapter {

        public MyPagerAdapter(FragmentManager fm) {
            super(fm);
        }

        @Override
        public CharSequence getPageTitle(int position) {
            return titleList.get(position);
        }

        @Override
        public int getCount() {
            return titleList.size();
        }

        @Override
        public Fragment getItem(int position) {
            return CurrItemFragment.newInstance(position);
        }
    }
}