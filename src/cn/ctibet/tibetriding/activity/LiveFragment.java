package cn.ctibet.tibetriding.activity;

import android.content.Context;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ListView;
import cn.ctibet.tibetriding.R;
import cn.ctibet.tibetriding.adapter.LiveListAdapter;
import cn.ctibet.tibetriding.bean.CyclingImgBean;
import cn.ctibet.tibetriding.view.XListView;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Administrator on 2015/3/23.
 */
public class LiveFragment extends Fragment {
    private Context context;
    private XListView listView;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.live_fragment, null);
        context = getActivity();
        initView(view);
        return view;
    }

    private void initView(View view) {
        listView = (XListView) view.findViewById(R.id.live_fragment_list);
        LiveListAdapter adapter = new LiveListAdapter(context);
        listView.setAdapter(adapter);

        List<CyclingImgBean> list = new ArrayList<CyclingImgBean>();
        CyclingImgBean bean = null;
        for (int i = 0; i < 5; i++) {
            bean = new CyclingImgBean();
            bean.imgname = "张三" + i;
            list.add(bean);
        }

        adapter.addList(list);
        adapter.notifyDataSetChanged();
    }
}
