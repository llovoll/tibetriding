package cn.ctibet.tibetriding.activity;

import java.util.List;

import android.content.Context;
import android.content.Intent;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.support.v4.app.FragmentActivity;
import android.text.TextUtils;
import android.view.KeyEvent;
import android.view.View;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import cn.ctibet.tibetriding.R;
import cn.ctibet.tibetriding.app.ExitApplication;
import cn.ctibet.tibetriding.bean.*;
import cn.ctibet.tibetriding.db.Advisetime;
import cn.ctibet.tibetriding.db.DailyTrackHistory;
import cn.ctibet.tibetriding.db.Way;
import cn.ctibet.tibetriding.util.*;

/*
 主页面父布局
 */
public class MainActivity extends FragmentActivity implements View.OnClickListener {


    private RelativeLayout back;
    private TextView back_Text;
    private TextView title;
    private Context context;
    private long firstClick;
    private RelativeLayout sosBtn;
    private TextView sosBtn_text;
    private TextView startBtn;
    private TextView routeBtn;
    private ImageView roadBookBtn;
    private TextView journeyBtn;
    private UserInfo userInfo;
    private boolean daily_exe = false; //true  表示已有锻炼记录   false  表示无锻炼记录
    private boolean daily_stat = true;  //true  开始   false  暂停
    private String trackhistoryid = "";
    private List<WayBean> list;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        context = MainActivity.this;
        ExitApplication.getInstance().addActivity(this);
        initView();
    }

    private void initView() {
        userInfo = UserInfoUtil.getUserInfo(context);
        getDataStat();
        back = (RelativeLayout) findViewById(R.id.head_left);
        back_Text = (TextView) findViewById(R.id.head_left_text_btn);
        back_Text.setBackgroundResource(R.drawable.home_menu);
        title = (TextView) findViewById(R.id.head_title);
        title.setText(getResources().getString(R.string.xzqx));
        back.setOnClickListener(this);

        sosBtn = (RelativeLayout) findViewById(R.id.head_right);
        sosBtn.setVisibility(View.VISIBLE);
        sosBtn_text = (TextView) findViewById(R.id.head_right_text_btn);
        sosBtn_text.setText(getResources().getString(R.string.sos));
        sosBtn_text.setTextColor(getResources().getColor(R.color.blue4));

        routeBtn = (TextView) findViewById(R.id.activity_main_route);
        startBtn = (TextView) findViewById(R.id.activity_main_start_exercise);
        roadBookBtn = (ImageView) findViewById(R.id.road_bock_btn);
        journeyBtn = (TextView) findViewById(R.id.main_activity_journey);

        routeBtn.setOnClickListener(this);
        startBtn.setOnClickListener(this);
        sosBtn.setOnClickListener(this);
        roadBookBtn.setOnClickListener(this);
        journeyBtn.setOnClickListener(this);

        // startService(new Intent(context, BgRunningService.class));
        goodRoute();
    }

    @Override
    protected void onResume() {
        super.onResume();
        getDataStat();
        if (!daily_exe) {
            startBtn.setText("继续锻炼");
        } else {
            startBtn.setText("开始锻炼");
        }
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.head_left:
                startActivity(new Intent(context, MenuActivity.class));
                if (Build.VERSION.SDK_INT > 5) {
                    overridePendingTransition(R.anim.zoomrlin, R.anim.zoomrlout);
                }
                break;
            case R.id.head_right:
                startActivity(new Intent(context, SosActivity.class));
                break;
            case R.id.activity_main_start_exercise:
                Intent dailyIntent = new Intent(context, DailyExerciseActivity.class);
                dailyIntent.putExtra("tag", daily_exe);
                dailyIntent.putExtra("tag1", daily_stat);
                dailyIntent.putExtra("trackhistoryid", trackhistoryid);
                startActivity(dailyIntent);
                break;
            case R.id.activity_main_route:
                startActivity(new Intent(context, RouteChooseActivity.class));
                break;
            case R.id.road_bock_btn:
                startActivity(new Intent(context, RoadBookActivity.class));
                break;
            case R.id.main_activity_journey:
                startActivity(new Intent(context, JourneyActivity.class));
                break;
        }
    }

    public boolean onKeyDown(int keyCode, KeyEvent event) {

        if (keyCode == KeyEvent.KEYCODE_BACK) {
            startActivity(new Intent(context, MenuActivity.class));
            if (Build.VERSION.SDK_INT > 5) {
                overridePendingTransition(R.anim.zoomrlin, R.anim.zoomrlout);
            }
            return true;
        }
        return super.onKeyDown(keyCode, event);
    }

    private void getDataStat() {
        DailyTrackHistory db1 = new DailyTrackHistory(context);
        DailyTrackHistoryBean bean = db1.getPausedDailyTrackHistory(userInfo.userId);
        trackhistoryid = bean.Trackhistoryid;
        if (TextUtils.isEmpty(bean.Trackhistoryid)) {
            daily_exe = false;
        } else {
            daily_exe = true;
        }
        if (!TextUtils.isEmpty(bean.Trackhistoryid)) {
            daily_stat = true;
        } else {
            daily_stat = false;
        }
    }

    /**
     * 获取精彩路线
     */
    private void goodRoute() {
        new Thread(new Runnable() {
            @Override
            public void run() {
                Message msg = new Message();
                Way db = new Way(context);
                String str = "?addtime=" + db.getRidingLineUpdatetime() + TimeUtil.getKeyStr();
                db = null;
                try {
                    String jsonData = SyncHttp.httpGet(Configs.HOST + "Way/getWayInfo", str);
                    SysPrintUtil.pt("上传到服务器数据为", Configs.HOST + "Way/getWayInfo" + str);
                    SysPrintUtil.pt("json==获取到的服务器的数据为:", jsonData);
                    list = JsonUtil.getRouteData(jsonData);
                    if (null != list && list.size() > 0) {
                        msg.what = Configs.READ_SUCCESS;
                    } else {
                        msg.what = Configs.READ_FAIL;
                    }
                } catch (Exception e) {
                    SysPrintUtil.pt("数据异常", e.toString());
                    e.printStackTrace();
                    msg.what = Configs.READ_ERROR;
                }
                nhandler.sendMessage(msg);
            }
        }).start();
    }

    private Handler nhandler = new Handler() {
        @Override
        public void handleMessage(Message msg) {
            switch (msg.what) {
                case Configs.READ_SUCCESS:
                    Way db = new Way(context);
                    Advisetime timeDb = new Advisetime(context);
                    for (int i = 0; i < list.size(); i++) {
                        db.save(list.get(i));
                        if (null != list.get(i).advisetimeList &&
                                list.get(i).advisetimeList.size() > 0) {
                            for (int j = 0; j < list.get(i).advisetimeList.size(); j++) {
                                timeDb.save(list.get(i).Wayid, list.get(i).advisetimeList);
                            }
                        }
                    }
                    db = null;
                    timeDb = null;
                    break;
                case Configs.READ_FAIL:
                    break;
                case Configs.READ_ERROR:
                    break;
            }
        }
    };
}
