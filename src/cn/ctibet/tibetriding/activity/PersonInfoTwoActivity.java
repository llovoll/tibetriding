package cn.ctibet.tibetriding.activity;

import android.app.Activity;
import android.os.Bundle;
import android.view.View;
import android.widget.RelativeLayout;
import android.widget.TextView;
import cn.ctibet.tibetriding.R;

/**
 * Created by Administrator on 2015/3/6.
 */
public class PersonInfoTwoActivity extends Activity {
    private RelativeLayout backBtn;
    private TextView back_Text;
    private TextView title;
    private RelativeLayout sureBtn;
    private TextView sure_Text;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.person_info_two_activity);
        initView();
    }

    private void initView() {
        backBtn = (RelativeLayout) findViewById(R.id.head_left);
        back_Text = (TextView) findViewById(R.id.head_left_text_btn);
        back_Text.setBackgroundResource(R.drawable.back);
        title = (TextView) findViewById(R.id.head_title);
        title.setText(getResources().getText(R.string.person_info));
        sureBtn = (RelativeLayout) findViewById(R.id.head_right);
        sureBtn.setVisibility(View.VISIBLE);
        sure_Text = (TextView) findViewById(R.id.head_right_text_btn);
        sure_Text.setBackgroundResource(R.drawable.make_sure);
    }
}