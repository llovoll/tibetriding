package cn.ctibet.tibetriding.activity;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import cn.ctibet.tibetriding.R;

/**
 * 个人资料一级页面
 * Created by Administrator on 2015/3/6.
 */
public class PersonInfoOneActivity extends Activity implements View.OnClickListener {
    private LinearLayout sign_route;
    private LinearLayout task_manger;
    private LinearLayout honor;
    private RelativeLayout backBtn;
    private RelativeLayout editBtn;
    private Context context;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.person_info_one_activity);
        context = PersonInfoOneActivity.this;
        initView();
    }

    private void initView() {
        backBtn = (RelativeLayout) findViewById(R.id.person_info_one_activity_back);
        editBtn = (RelativeLayout) findViewById(R.id.person_info_one_activity_edit);
        sign_route = (LinearLayout) findViewById(R.id.person_info_one_activity_sign_route);
        task_manger = (LinearLayout) findViewById(R.id.person_info_one_activity_task_manger);
        honor = (LinearLayout) findViewById(R.id.person_info_one_activity_honor);
        backBtn.setOnClickListener(this);
        editBtn.setOnClickListener(this);
        sign_route.setOnClickListener(this);
        task_manger.setOnClickListener(this);
        honor.setOnClickListener(this);
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.person_info_one_activity_back:
                finish();
                break;
            case R.id.person_info_one_activity_edit:
                startActivity(new Intent(context, PersonInfoTwoActivity.class));
                break;
            case R.id.person_info_one_activity_sign_route:
                startActivity(new Intent(context, ApplicationRouteActivity.class));
                break;
            case R.id.person_info_one_activity_task_manger:
                break;
            case R.id.person_info_one_activity_honor:
                break;
        }
    }
}