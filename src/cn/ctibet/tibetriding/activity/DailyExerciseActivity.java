package cn.ctibet.tibetriding.activity;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.drawable.BitmapDrawable;
import android.location.Location;
import android.net.Uri;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.provider.MediaStore;
import android.text.TextUtils;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.Window;
import android.widget.*;
import cn.ctibet.tibetriding.R;
import cn.ctibet.tibetriding.app.ExitApplication;
import cn.ctibet.tibetriding.bean.*;
import cn.ctibet.tibetriding.db.AlbumDb;
import cn.ctibet.tibetriding.db.DailyTrackHistory;
import cn.ctibet.tibetriding.util.*;
import cn.ctibet.tibetriding.view.ScoreDialog;

import com.amap.api.location.*;
import com.amap.api.maps.AMap;
import com.amap.api.maps.MapView;
import com.amap.api.maps.UiSettings;

import java.io.File;
import java.io.IOException;
import java.sql.Date;
import java.text.SimpleDateFormat;
import java.util.List;
import java.util.Locale;

/**
 * Created by Administrator on 2015/1/19.
 */
public class DailyExerciseActivity extends Activity implements View.OnClickListener
        , AMapLocationListener {
    private static final int CAMERA = 101;
    private static final int GALLEY = 102;
    private MapView mapView;
    private AMap aMap;
    private UiSettings mUiSettings;
    private RelativeLayout back;
    private TextView back_Text;
    private TextView title;
    private RelativeLayout cameraBtn;
    private TextView cameraBtn_text;
    private Context context;
    private PopupWindow popupWindow;
    private RelativeLayout stopBtn;
    private Uri cameraUri;
    private Bitmap bitmap;
    private boolean daily_exe;
    private UserInfo userInfo;
    private DailyExeBean bean;
    private String trackset = "";
    private LocationManagerProxy mLocationManagerProxy;
    private String time = "";
    private String timeCamera = "";
    private String imgPath = "";
    private double geoLat = 0;
    private double geoLng = 0;
    private double altitude = 0;
    private String province = "";
    private String city = "";
    private String wendu = "";
    private DailyTrackHistoryBean bean2;
    private RelativeLayout pauseBtn;
    private ImageView pauseImg;
    private boolean pauseStat = true;
    private DailyExeBean stat;
    private DailyExeBean stat1;
    private DailyExeBean stat2;
    private double speed = 0;
    private TextView speedText;
    private float junSpeed;
    private TextView junSpeedText;
    private TextView lichengText;
    private boolean daily_stat;
    private TextView timeText;
    private TextView haibaText;
    private String trackhistoryid;


    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.daily_exercise_activity);
        context = DailyExerciseActivity.this;
        ExitApplication.getInstance().addActivity(this);
        mapView = (MapView) findViewById(R.id.mapView);
        mapView.onCreate(savedInstanceState);// 必须要写
        init();
        initView();
    }

    private void initView() {
        if (null != getIntent()) {
            daily_exe = getIntent().getBooleanExtra("tag", false);
            daily_stat = getIntent().getBooleanExtra("tag1", false);
            trackhistoryid = getIntent().getStringExtra("trackhistoryid");
        }
        userInfo = UserInfoUtil.getUserInfo(context);
        back = (RelativeLayout) findViewById(R.id.head_left);
        back_Text = (TextView) findViewById(R.id.head_left_text_btn);
        back_Text.setBackgroundResource(R.drawable.back);
        title = (TextView) findViewById(R.id.head_title);
        title.setText(getResources().getString(R.string.daily_exercise));
        cameraBtn = (RelativeLayout) findViewById(R.id.head_right);
        cameraBtn.setVisibility(View.VISIBLE);
        cameraBtn_text = (TextView) findViewById(R.id.head_right_text_btn);
        cameraBtn_text.setBackgroundResource(R.drawable.camera);
        haibaText = (TextView) findViewById(R.id.daily_exercise_activity_haiba);
        stopBtn = (RelativeLayout) findViewById(R.id.daily_exercise_activity_stop);
        pauseBtn = (RelativeLayout) findViewById(R.id.daily_exercise_activity_pause);
        pauseImg = (ImageView) findViewById(R.id.daily_exercise_activity_pauseImg);
        speedText = (TextView) findViewById(R.id.daily_exercise_activity_shisu);
        junSpeedText = (TextView) findViewById(R.id.daily_exercise_activity_jusu);
        lichengText = (TextView) findViewById(R.id.daily_exercise_activity_licheng);
        timeText = (TextView) findViewById(R.id.daily_exercise_activity_time);
        if (aMap == null) {
            aMap = mapView.getMap();
        }
        mUiSettings = aMap.getUiSettings();
        mUiSettings.setZoomControlsEnabled(false);//隐藏缩放按钮

        back.setOnClickListener(this);
        cameraBtn.setOnClickListener(this);
        stopBtn.setOnClickListener(this);
        pauseBtn.setOnClickListener(this);
        if (daily_exe && daily_stat) {
            DailyTrackHistory db1 = new DailyTrackHistory(context);
            DailyTrackHistoryBean dailyBean2 = db1.getDailyTrackHistoryBean(trackhistoryid, userInfo.userId);
            LLAEntity lla = LLAUtils.parseLLAStr(dailyBean2.Trackset);
            lichengText.setText(DistanceUtil.getDistance(geoLat, geoLng, lla.getLat(), lla.getLng()) + "");
            timeText.setText(TimeUtil.getTimePoor(dailyBean2.Endtime, TimeUtil.getTime()));
            junSpeed = (dailyBean2.Totalmileage / 1000) / (Float.valueOf(dailyBean2.Casttime) / 3600);
            junSpeedText.setText("" + junSpeed);
            db1 = null;
            dailyBean2 = null;
        }

        if (!daily_stat) {
            pauseImg.setBackgroundResource(R.drawable.btn_goon);
            pauseStat = false;
        } else {
            pauseImg.setBackgroundResource(R.drawable.pause);
            pauseStat = true;
        }
    }

    /**
     * 初始化定位
     */
    private void init() {

        mLocationManagerProxy = LocationManagerProxy.getInstance(this);

        //此方法为每隔固定时间会发起一次定位请求，为了减少电量消耗或网络流量消耗，
        //注意设置合适的定位时间的间隔，并且在合适时间调用removeUpdates()方法来取消定位请求
        //在定位结束后，在合适的生命周期调用destroy()方法
        //其中如果间隔时间为-1，则定位只定一次
        mLocationManagerProxy.requestLocationData(        //60 * 1000
                LocationProviderProxy.AMapNetwork, -1, 15, this);
        mLocationManagerProxy.setGpsEnable(false);

        mLocationManagerProxy.requestWeatherUpdates(
                LocationManagerProxy.WEATHER_TYPE_LIVE,
                new AMapLocalWeatherListener() {

                    @Override
                    public void onWeatherLiveSearched(AMapLocalWeatherLive arg0) {
                        // TODO Auto-generated method stub
                        wendu = arg0.getTemperature();
                    }

                    @Override
                    public void onWeatherForecaseSearched(
                            AMapLocalWeatherForecast arg0) {
                        // TODO Auto-generated method stub
                    }
                });

    }

    /**
     * 方法必须重写
     */
    @Override
    protected void onResume() {
        super.onResume();
        mapView.onResume();
    }

    /**
     * 方法必须重写
     */
    @Override
    protected void onPause() {
        super.onPause();
        mapView.onPause();
    }

    /**
     * 方法必须重写
     */
    @Override
    protected void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
        mapView.onSaveInstanceState(outState);
    }

    /**
     * 方法必须重写
     */
    @Override
    protected void onDestroy() {
        super.onDestroy();
        mapView.onDestroy();
    }

    @Override
    public void onLocationChanged(AMapLocation aMapLocation) {
       /* int code1 = aMapLocation.getAMapException().getErrorCode();
        SysPrintUtil.pt("骑行相册经纬度222", code1 + "");
        getdailyExeId();*/
        if (aMapLocation != null && aMapLocation.getAMapException().getErrorCode() == 0) {
            //获取位置信息
            geoLat = aMapLocation.getLatitude(); //纬度
            geoLng = aMapLocation.getLongitude();  //纬度
            altitude = aMapLocation.getAltitude();  //海拔
            province = aMapLocation.getProvince();  //省
            city = aMapLocation.getCity();  //市
            speed = aMapLocation.getSpeed() * 3.6;
            speedText.setText("" + speed);
            String address = aMapLocation.getAddress();
            SysPrintUtil.pt("骑行相册经纬度", geoLat + "," + geoLng + "," + altitude +
                    "," + province + "," + city);
            if (0 != geoLat && 0 != geoLng) {
                haibaText.setText(altitude + "M");
                trackset = geoLat + "," + geoLng + "," + altitude;
                if (daily_stat) {
                    DailyTrackHistory db1 = new DailyTrackHistory(context);
                    DailyTrackHistoryBean dailyBean2 = db1.getDailyTrackHistoryBean(trackhistoryid, userInfo.userId);
                    LLAEntity lla = LLAUtils.parseLLAStr(dailyBean2.Trackset);
                    lichengText.setText(DistanceUtil.getDistance(geoLat, geoLng, lla.getLat(), lla.getLng()) + "");
                    timeText.setText(TimeUtil.getTimePoor(dailyBean2.Endtime, TimeUtil.getTime()));
                    junSpeed = (dailyBean2.Totalmileage / 1000) / (Float.valueOf(dailyBean2.Casttime) / 3600);
                    junSpeedText.setText("" + junSpeed);
                    db1 = null;
                    dailyBean2 = null;
                }
                if (!daily_exe) {
                    getdailyExeId();
                }
            }

        }
    }

    @Override
    public void onLocationChanged(Location location) {

    }

    @Override
    public void onStatusChanged(String s, int i, Bundle bundle) {

    }

    @Override
    public void onProviderEnabled(String s) {

    }

    @Override
    public void onProviderDisabled(String s) {

    }

    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.head_left:
                finish();
                break;
            case R.id.head_right:
                showDialog();
                break;
            case R.id.daily_exercise_activity_stop:
                initPopWindow();
                popupWindow.showAtLocation(view, Gravity.CENTER, 0, 0);
                break;
            case R.id.daily_exercise_activity_pause:
                if (pauseStat) {
                    pauseImg.setBackgroundResource(R.drawable.btn_goon);
                    pauseStat = false;
                    getPauseDailyExe();
                } else {
                    pauseImg.setBackgroundResource(R.drawable.pause);
                    pauseStat = true;
                    getStartPauseDailyExe();
                }
                break;
        }
    }

    private void initPopWindow() {
        View view = LayoutInflater.from(context).inflate(
                R.layout.pop_view, null);
        TextView sureBtn = (TextView) view.findViewById(R.id.pop_view_sure);
        TextView errorBtn = (TextView) view.findViewById(R.id.pop_view_touch_error);
        popupWindow = new PopupWindow(view, LinearLayout.LayoutParams.FILL_PARENT,
                LinearLayout.LayoutParams.FILL_PARENT, true);
        popupWindow.setContentView(view);
        popupWindow.setFocusable(true);
        popupWindow.setOutsideTouchable(false);
        popupWindow.setBackgroundDrawable(new BitmapDrawable());
        view.setFocusable(true);
        view.setFocusableInTouchMode(true);
        view.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (null != popupWindow) {
                    popupWindow.dismiss();
                }
            }
        });
        sureBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (null != popupWindow) {
                    popupWindow.dismiss();
                }
                getCompleteDailyExe();
            }
        });
        errorBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (null != popupWindow) {
                    popupWindow.dismiss();
                }
            }
        });
    }

    private void showDialog() {
        final ScoreDialog dialog = new ScoreDialog(this, R.layout.select_camera_dialog1, R.style.dialog_more_style);
        dialog.setParamsBotton();
        dialog.show();
        if (!SdCardUtil.ExistSDCard()) {
            Toast.makeText(context, getResources()
                    .getString(R.string.null_sdcrad), Toast.LENGTH_SHORT).show();
            return;
        }
        dialog.findViewById(R.id.camera).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                dialog.dismiss();
                timeCamera = TimeUtil.getTime();
                Intent camera = new Intent(
                        MediaStore.ACTION_IMAGE_CAPTURE);
                File paths = new File(Configs.APP_PATH + "galley/");
                imgPath = Configs.APP_PATH + "galley/" + timeCamera + ".jpg";
                File file = new File(imgPath);
                if (!paths.exists()) {
                    paths.mkdirs();
                }
                if (!file.exists()) {
                    try {
                        file.createNewFile();
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                }
                cameraUri = Uri.fromFile(file);
                camera.putExtra(MediaStore.EXTRA_OUTPUT, cameraUri);
                startActivityForResult(camera, CAMERA);
            }
        });
        dialog.findViewById(R.id.album).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                dialog.dismiss();
                bean2.city = city;
                bean2.province = province;
                Bundle bundle = new Bundle();
                bundle.putSerializable("bean", bean2);
                Intent intent = new Intent(context, ImageChooseActivity.class);
                intent.putExtras(bundle);
                startActivity(intent);
            }
        });
        dialog.findViewById(R.id.guiji).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                dialog.dismiss();
                bean2.city = city;
                bean2.province = province;
                Bundle bundle = new Bundle();
                bundle.putSerializable("bean", bean2);
                Intent intent = new Intent(context, ReleaseLive2Activity.class);
                intent.putExtras(bundle);
                startActivity(intent);
            }
        });
        dialog.findViewById(R.id.back).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                dialog.dismiss();
            }
        });
        Window dialogWindow = dialog.getWindow();
        dialogWindow.setWindowAnimations(R.style.dialog_more_style);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        switch (requestCode) {
            case CAMERA:
                if (resultCode == Activity.RESULT_OK
                        && null == data) {
                    AlbumDb albumDb = new AlbumDb(context);
                    CyclingImgBean imgBean = new CyclingImgBean();
                    imgBean.location = province + city;
                    imgBean.haiba = altitude + "";
                    imgBean.wendu = wendu;
                    imgBean.time = TimeUtil.timeToStr(Long.valueOf(timeCamera));
                    imgBean.imgname = timeCamera + ".jpg";
                    albumDb.saveAlbum(imgBean);
                    // String img_path = UriToFilePath.getFilePath(DailyExerciseActivity.this, cameraUri);

                    bean2.city = city;
                    bean2.province = province;
                    Bundle bundle = new Bundle();
                    bundle.putSerializable("bean", bean2);
                    Intent intent = new Intent(context, WaterMarkActivity.class);
                    intent.putExtras(bundle);
                    intent.putExtra("Uri", imgPath);
                    startActivity(intent);
                }
                break;
            case GALLEY:
                if (null != data) {
                    bitmap = BitMapUtil.compressImage(BitMapUtil.getBitmapFromUri(context, data.getData()));
                }
                break;
        }
    }

    /**
     * 获取日常锻炼的ID
     */
    private void getdailyExeId() {
        new Thread(new Runnable() {
            @Override
            public void run() {
                Message msg = new Message();
                String str = "?userid=" + userInfo.userId + "&smallimg="
                        + "&trackset=" + trackset + "&begintime=" + TimeUtil.getTime() + TimeUtil.getKeyStr();
                try {
                    String jsonData = SyncHttp.httpGet(Configs.HOST + "Idailytrack/addmsg/type/0", str);
                    SysPrintUtil.pt("上传到服务器数据为", Configs.HOST + "Idailytrack/addmsg/type/0" + str);
                    SysPrintUtil.pt("json==获取到的服务器的数据为:", jsonData);
                    bean = JsonUtil.getdailyExeData(jsonData);
                    if (!TextUtils.isEmpty(bean.trackhistoryid) &&
                            !TextUtils.isEmpty(bean.dailytrackhistoryid)) {
                        msg.what = Configs.READ_SUCCESS;
                    } else {
                    msg.what = Configs.READ_FAIL;
                }
                } catch (Exception e) {
                    SysPrintUtil.pt("数据异常", e.toString());
                    e.printStackTrace();
                    msg.what = Configs.READ_ERROR;
                }
                handler.sendMessage(msg);
            }
        }).start();
    }


    private Handler handler = new Handler() {
        @Override
        public void handleMessage(Message msg) {
            switch (msg.what) {
                case Configs.READ_SUCCESS:
                    ToastUtil.showToast(context, "开始骑行", 0);
                    trackhistoryid = bean.trackhistoryid;
                    DailyTrackHistory db1 = new DailyTrackHistory(context);
                    bean2 = new DailyTrackHistoryBean();
                    bean2.Trackhistoryid = bean.trackhistoryid;
                    bean2.Addtime = TimeUtil.getTime();
                    bean2.Begintime = TimeUtil.getTime();
                    bean2.Endtime = "";
                    bean2.Dailytrackhistoryid = Integer.valueOf(bean.dailytrackhistoryid);
                    bean2.Casttime = "";
                    bean2.Totalmileage = 0;
                    bean2.Userid = userInfo.userId;
                    bean2.Biketype = EnumRidingType.DAILY_EXERCISING.getValue();
                    bean2.Smallimg = "";
                    bean2.Status = EnumRidingStatus.STARTED.getValue();
                    bean2.Trackset = trackset;
                    db1.save(bean2);
                    db1 = null;
                    break;
                case Configs.READ_FAIL:
                    break;
                case Configs.READ_ERROR:
                    break;
            }
        }
    };

    /**
     * 暂停日常锻炼
     */
    private void getPauseDailyExe() {
        new Thread(new Runnable() {
            @Override
            public void run() {
                Message msg = new Message();
                time = TimeUtil.getTime();
                DailyTrackHistory db1 = new DailyTrackHistory(context);
                SysPrintUtil.pt("查询的id为", trackhistoryid + "====" + userInfo.userId);
                DailyTrackHistoryBean bean1 = db1.getDailyTrackHistoryBean(trackhistoryid, userInfo.userId);

                String str = "?userid=" + userInfo.userId + "&smallimg="
                        + "&trackset=" + trackset + "&trackhistoryid=" + bean1.Trackhistoryid
                        + "&endtime=" + time + "&casttime=" + TimeUtil.getTimePoor(bean1.Begintime, time) + "&totalmileage=" + "" +
                        "&dailytrackhistoryid=" + bean1.Dailytrackhistoryid + TimeUtil.getKeyStr();
                try {
                    String jsonData = SyncHttp.httpGet(Configs.HOST + "Idailytrack/addmsg/type/1", str);
                    SysPrintUtil.pt("上传到服务器数据为", Configs.HOST + "Idailytrack/addmsg/type/1" + str);
                    SysPrintUtil.pt("json==获取到的服务器的数据为:", jsonData);
                    stat1 = JsonUtil.getReleaseLiveStat(jsonData);
                    if (stat1.code.equals("200")) {
                        msg.what = Configs.READ_SUCCESS;
                    } else {
                        msg.what = Configs.READ_FAIL;
                    }
                } catch (Exception e) {
                    SysPrintUtil.pt("数据异常", e.toString());
                    e.printStackTrace();
                    msg.what = Configs.READ_ERROR;
                }
                nhandler.sendMessage(msg);
            }
        }).start();
    }

    private Handler nhandler = new Handler() {
        @Override
        public void handleMessage(Message msg) {
            switch (msg.what) {
                case Configs.READ_SUCCESS:
                    DailyTrackHistory db1 = new DailyTrackHistory(context);
                    DailyTrackHistoryBean bean2 = db1.getDailyTrackHistoryBean(trackhistoryid, userInfo.userId);
                    bean2.Smallimg = "";
                    bean2.Trackset = trackset;
                    bean2.Endtime = time;
                    bean2.Casttime = Long.valueOf(bean2.Casttime) +
                            (Long.valueOf(bean2.Endtime) - Long.valueOf(bean2.Begintime)) + "";
                    LLAEntity lla = LLAUtils.parseLLAStr(bean2.Trackset);
                    LLAEntity lla1 = LLAUtils.parseLLAStr(trackset);
                    bean2.Totalmileage = bean2.Totalmileage + (float) DistanceUtil.getDistance(lla.getLat(), lla.getLng(),
                            lla1.getLat(), lla1.getLng());
                    bean2.Status = EnumRidingStatus.PAUSED.getValue();
                    db1.handleStatus(bean2);
                    bean2 = null;
                    db1 = null;
                    ToastUtil.showToast(context, "暂停成功", 0);
                    break;
                case Configs.READ_FAIL:
                    break;
                case Configs.READ_ERROR:
                    break;
            }
        }
    };

    /**
     * 开始已经暂停的日常锻炼
     */
    private void getStartPauseDailyExe() {
        new Thread(new Runnable() {
            @Override
            public void run() {
                Message msg = new Message();
                time = TimeUtil.getTime();

                String str = "?userid=" + userInfo.userId + "&trackhistoryid=" + trackhistoryid + TimeUtil.getKeyStr();
                try {
                    String jsonData = SyncHttp.httpGet(Configs.HOST + "Idailytrack/gostop", str);
                    SysPrintUtil.pt("上传到服务器数据为", Configs.HOST + "Idailytrack/gostop" + str);
                    SysPrintUtil.pt("json==获取到的服务器的数据为:", jsonData);
                    stat = JsonUtil.getReleaseLiveStat(jsonData);
                    if (stat.code.equals("200")) {
                        msg.what = Configs.READ_SUCCESS;
                    } else {
                        msg.what = Configs.READ_FAIL;
                    }
                } catch (Exception e) {
                    SysPrintUtil.pt("数据异常", e.toString());
                    e.printStackTrace();
                    msg.what = Configs.READ_ERROR;
                }
                mchandler.sendMessage(msg);
            }
        }).start();
    }

    private Handler mchandler = new Handler() {
        @Override
        public void handleMessage(Message msg) {
            switch (msg.what) {
                case Configs.READ_SUCCESS:
                    DailyTrackHistory db1 = new DailyTrackHistory(context);
                    DailyTrackHistoryBean bean2 = db1.getDailyTrackHistoryBean(trackhistoryid, userInfo.userId);
                    bean2.Begintime = TimeUtil.getTime();
                    db1.handleStatus(bean2);
                    bean2 = null;
                    db1 = null;
                    ToastUtil.showToast(context, "开始成功", 0);
                    break;
                case Configs.READ_FAIL:
                    break;
                case Configs.READ_ERROR:
                    break;
            }
        }
    };

    /**
     * 完成日常锻炼
     */
    private void getCompleteDailyExe() {
        new Thread(new Runnable() {
            @Override
            public void run() {
                Message msg = new Message();
                time = TimeUtil.getTime();
                DailyTrackHistory db1 = new DailyTrackHistory(context);
                DailyTrackHistoryBean bean1 = db1.getDailyTrackHistoryBean(trackhistoryid, userInfo.userId);

                String str = "?userid=" + userInfo.userId + "&smallimg="
                        + "&trackset=" + trackset + "&trackhistoryid=" + bean1.Trackhistoryid
                        + "&endtime=" + time + "&casttime=" + TimeUtil.getTimePoor(bean1.Begintime, time) + "&totalmileage=" + "" +
                        "&dailytrackhistoryid=" + bean1.Dailytrackhistoryid + TimeUtil.getKeyStr();
                try {
                    String jsonData = SyncHttp.httpGet(Configs.HOST + "Idailytrack/addmsg/type/2", str);
                    SysPrintUtil.pt("上传到服务器数据为", Configs.HOST + "Idailytrack/addmsg/type/2" + str);
                    SysPrintUtil.pt("json==获取到的服务器的数据为:", jsonData);
                    stat2 = JsonUtil.getReleaseLiveStat(jsonData);
                    if (stat2.code.equals("200")) {
                        msg.what = Configs.READ_SUCCESS;
                    } else {
                        msg.what = Configs.READ_FAIL;
                    }
                } catch (Exception e) {
                    SysPrintUtil.pt("数据异常", e.toString());
                    e.printStackTrace();
                    msg.what = Configs.READ_ERROR;
                }
                mhandler.sendMessage(msg);
            }
        }).start();
    }

    private Handler mhandler = new Handler() {
        @Override
        public void handleMessage(Message msg) {
            switch (msg.what) {
                case Configs.READ_SUCCESS:
                    DailyTrackHistory db1 = new DailyTrackHistory(context);
                    DailyTrackHistoryBean bean2 = db1.getDailyTrackHistoryBean(trackhistoryid, userInfo.userId);
                    bean2.Smallimg = "";
                    bean2.Trackset = trackset;
                    bean2.Endtime = time;
                    bean2.Casttime = Long.valueOf(bean2.Casttime) +
                            (Long.valueOf(bean2.Endtime) - Long.valueOf(bean2.Begintime)) + "";
                    LLAEntity lla = LLAUtils.parseLLAStr(bean2.Trackset);
                    LLAEntity lla1 = LLAUtils.parseLLAStr(trackset);
                    bean2.Totalmileage = bean2.Totalmileage + (float) DistanceUtil.getDistance(lla.getLat(), lla.getLng(),
                            lla1.getLat(), lla1.getLng());
                    bean2.Status = EnumRidingStatus.PAUSED.getValue();
                    db1.handleStatus(bean2);
                    bean2 = null;
                    db1 = null;
                    break;
                case Configs.READ_FAIL:
                    break;
                case Configs.READ_ERROR:
                    break;
            }
        }
    };
}