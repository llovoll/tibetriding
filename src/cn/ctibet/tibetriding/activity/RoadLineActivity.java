package cn.ctibet.tibetriding.activity;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.RelativeLayout;
import cn.ctibet.tibetriding.R;
import cn.ctibet.tibetriding.adapter.RoadLineListAdapter;
import cn.ctibet.tibetriding.app.ExitApplication;
import cn.ctibet.tibetriding.bean.Configs;
import cn.ctibet.tibetriding.bean.CyclingImgBean;
import cn.ctibet.tibetriding.bean.RoadLineBean;
import cn.ctibet.tibetriding.bean.ScreenSize;
import cn.ctibet.tibetriding.impl.RoadLineListClickListener;
import cn.ctibet.tibetriding.util.*;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Administrator on 2015/1/27.
 */
public class RoadLineActivity extends Activity implements RoadLineListClickListener {
    private LinearLayout line1;
    private ScreenSize screenSize;
    private Context context;
    private RelativeLayout rela1;
    private ListView listView;
    private RoadLineListAdapter adapter;
    private List<RoadLineBean> roadList;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.road_line_activity);
        ExitApplication.getInstance().addActivity(this);
        context = RoadLineActivity.this;
        initView();
    }

    private void initView() {
        screenSize = ScreenSizeUtil.getScreenSize(context);

        line1 = (LinearLayout) findViewById(R.id.road_line_activity_linear1);
        RelativeLayout.LayoutParams params = (RelativeLayout.LayoutParams) line1.getLayoutParams();
        params.width = screenSize.screenW * 4 / 9;
        line1.setLayoutParams(params);
        rela1 = (RelativeLayout) findViewById(R.id.road_line_activity_rela1);
        LinearLayout.LayoutParams params1 = (LinearLayout.LayoutParams) rela1.getLayoutParams();
        params1.height = screenSize.screenH * 5 / 24;
        rela1.setLayoutParams(params1);
        listView = (ListView) findViewById(R.id.road_line_activity_list);

        adapter = new RoadLineListAdapter(context);
        listView.setAdapter(adapter);
        adapter.setListClickPos(this);

        getLineData();
    }

    @Override
    public void getClickPos(int pos) {
        for (int i = 0; i < roadList.size(); i++) {
            roadList.get(i).isClick = false;
            if (i == pos) {
                roadList.get(i).isClick = true;
            }
        }
        adapter.addList(roadList);
        adapter.notifyDataSetChanged();

      /*  startActivity(new Intent(context, RoadBookActivity.class));
        if (Build.VERSION.SDK_INT > 5) {
            overridePendingTransition(R.anim.zoomrlin, R.anim.zoomrlout);
        }*/
        Intent intent = new Intent();
        intent.putExtra("id", roadList.get(pos).routeid);
        setResult(Activity.RESULT_OK, intent);
        finish();
    }

    private void getLineData() {
        new Thread(new Runnable() {
            @Override
            public void run() {
                Message msg = new Message();
                String str = "?wayid=" + 31 + TimeUtil.getKeyStr();
                try {
                    String jsonData = SyncHttp.httpGet(Configs.HOST + "Way/getRouteList", str);
                    SysPrintUtil.pt("上传到服务器数据为", Configs.HOST + "Way/getRouteList" + str);
                    SysPrintUtil.pt("json==获取到的服务器的数据为:", jsonData);
                    roadList = JsonUtil.getRoadLineData(jsonData);
                    if (null != roadList && roadList.size() > 0) {
                        msg.what = Configs.READ_SUCCESS;
                    } else {
                        msg.what = Configs.READ_FAIL;
                    }
                } catch (Exception e) {
                    SysPrintUtil.pt("数据异常", e.toString());
                    e.printStackTrace();
                    msg.what = Configs.READ_ERROR;
                }
                nhandler.sendMessage(msg);
            }
        }).start();
    }

    private Handler nhandler = new Handler() {
        @Override
        public void handleMessage(Message msg) {
            switch (msg.what) {
                case Configs.READ_SUCCESS:
                    adapter.addList(roadList);
                    adapter.notifyDataSetChanged();
                    break;
                case Configs.READ_FAIL:
                    ToastUtil.showToast(context, "暂无线路数据", 0);
                    break;
                case Configs.READ_ERROR:
                    ToastUtil.showToast(context, "数据异常", 0);
                    break;
            }
        }
    };
}