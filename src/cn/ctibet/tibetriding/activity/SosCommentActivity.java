package cn.ctibet.tibetriding.activity;

import android.app.Activity;
import android.content.Context;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.*;
import cn.ctibet.tibetriding.R;
import cn.ctibet.tibetriding.bean.*;
import cn.ctibet.tibetriding.util.*;
import net.tsz.afinal.FinalBitmap;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Administrator on 2015/3/18.
 */
public class SosCommentActivity extends Activity {
    private RelativeLayout back;
    private TextView back_Text;
    private TextView title;
    private RelativeLayout releaseBtn;
    private TextView releaseBtn_text;
    private LayoutInflater mInflater;
    private LinearLayout linear;
    private UserInfo userInfo;
    private Context context;
    private DailyExeBean bean;
    private SosDataBean sosBean;
    private ImageView headImg;
    private FinalBitmap fb;
    private TextView userName;
    private TextView time;
    private TextView content;
    private String num = Integer.MAX_VALUE + "";
    private List<CommentBean> commentList;
    private EditText comEdit;
    private TextView sendBtn;
    private DailyExeBean stat;
    private int comNum = 0;
    private TextView comNumText;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.sos_comment_activity);
        context = SosCommentActivity.this;
        initView();
    }

    private void initView() {
        fb = FinalBitmap.create(context);
        fb.configLoadingImage(R.drawable.default_local);
        fb.configLoadfailImage(R.drawable.default_local);

        if (null != getIntent()) {
            sosBean = (SosDataBean) getIntent().getSerializableExtra("bean");
        }
        userInfo = UserInfoUtil.getUserInfo(context);
        mInflater = LayoutInflater.from(this);
        back = (RelativeLayout) findViewById(R.id.head_left);
        back_Text = (TextView) findViewById(R.id.head_left_text_btn);
        back_Text.setBackgroundResource(R.drawable.back);
        title = (TextView) findViewById(R.id.head_title);
        title.setText("Ric求助");

        headImg = (ImageView) findViewById(R.id.sos_comment_listitem_headImg);
        userName = (TextView) findViewById(R.id.sos_comment_listitem_name);
        time = (TextView) findViewById(R.id.sos_comment_listitem_time);
        content = (TextView) findViewById(R.id.sos_comment_listitem_content);

        fb.display(headImg, sosBean.userimg);
        userName.setText(sosBean.nickname + " 求助");
        time.setText(sosBean.addtime);
        content.setText(sosBean.soscontent);

        linear = (LinearLayout) findViewById(R.id.sos_comment_activity_linear);
        comNumText = (TextView) findViewById(R.id.sos_comment_activity_comNum);
        linear.setVisibility(View.GONE);

        comEdit = (EditText) findViewById(R.id.sos_comment_activity_edit);
        sendBtn = (TextView) findViewById(R.id.sos_comment_activity_send);

        getCommentData();

        sendBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (TextUtils.isEmpty(comEdit.getText().toString().trim())) {
                    ToastUtil.showToast(context, "评论的内容不能为空", 0);
                } else {
                    sendCommentData();
                }
            }
        });
    }

    private void getCommentData() {
        new Thread(new Runnable() {
            @Override
            public void run() {
                Message msg = new Message();
                String str = "?sid=" + sosBean.sosid + "&num=" + num + "&soscommentid=" + "&userid=" + userInfo.userId + TimeUtil.getKeyStr();
                try {
                    String jsonData = SyncHttp.httpGet(Configs.HOST + "Isos/commentlist", str);
                    SysPrintUtil.pt("上传到服务器数据为", Configs.HOST + "Isos/commentlist" + str);
                    SysPrintUtil.pt("json==获取到的服务器的数据为:", jsonData);
                    commentList = JsonUtil.getSosCommentData(jsonData);
                    if (null != commentList && commentList.size() > 0) {
                        msg.what = Configs.READ_SUCCESS;
                    } else {
                        msg.what = Configs.READ_FAIL;
                    }
                } catch (Exception e) {
                    SysPrintUtil.pt("数据异常", e.toString());
                    e.printStackTrace();
                    msg.what = Configs.READ_ERROR;
                }
                nhandler.sendMessage(msg);
            }
        }).start();
    }

    private Handler nhandler = new Handler() {
        @Override
        public void handleMessage(Message msg) {
            switch (msg.what) {
                case Configs.READ_SUCCESS:
                    linear.setVisibility(View.VISIBLE);
                    setCommentShow(commentList);
                    comNum = commentList.size();
                    comNumText.setText("回应：" + comNum);
                    break;
                case Configs.READ_FAIL:
                    linear.setVisibility(View.GONE);
                    ToastUtil.showToast(context, "暂无评论", 0);
                    break;
                case Configs.READ_ERROR:
                    ToastUtil.showToast(context, "数据异常", 0);
                    break;
            }
        }
    };

    private void setCommentShow(List<CommentBean> commentList) {
        for (int i = 0; i < commentList.size(); i++) {
            CommentBean comBean = commentList.get(i);
            View view = mInflater.inflate(R.layout.comment_item, null);
            ImageView headImg = (ImageView) view.findViewById(R.id.comment_item_headImg);
            TextView userName = (TextView) view.findViewById(R.id.comment_item_userName);
            TextView time = (TextView) view.findViewById(R.id.comment_item_time);
            TextView content = (TextView) view.findViewById(R.id.comment_item_content);
            fb.display(headImg, Configs.HOST + comBean.userimg);
            userName.setText(comBean.username);
            time.setText(comBean.addtime);
            content.setText(comBean.soscomment);
            linear.addView(view);
        }
    }

    private void sendCommentData() {
        new Thread(new Runnable() {
            @Override
            public void run() {
                Message msg = new Message();
                String str = "?sid=" + sosBean.sosid + "&soscomment=" + comEdit.getText().toString().trim()
                        + "&userid=" + userInfo.userId + TimeUtil.getKeyStr();
                try {
                    String jsonData = SyncHttp.httpGet(Configs.HOST + "Isos/soscomment", str);
                    SysPrintUtil.pt("上传到服务器数据为", Configs.HOST + "Isos/soscomment" + str);
                    SysPrintUtil.pt("json==获取到的服务器的数据为:", jsonData);
                    stat = JsonUtil.getReleaseLiveStat(jsonData);
                    if (null != stat && stat.code.equals("200")) {
                        msg.what = Configs.READ_SUCCESS;
                    } else {
                        msg.what = Configs.READ_FAIL;
                    }
                } catch (Exception e) {
                    SysPrintUtil.pt("数据异常", e.toString());
                    e.printStackTrace();
                    msg.what = Configs.READ_ERROR;
                }
                handler.sendMessage(msg);
            }
        }).start();
    }

    private Handler handler = new Handler() {
        @Override
        public void handleMessage(Message msg) {
            switch (msg.what) {
                case Configs.READ_SUCCESS:
                    View view = mInflater.inflate(R.layout.comment_item, null);
                    ImageView headImg = (ImageView) view.findViewById(R.id.comment_item_headImg);
                    TextView userName = (TextView) view.findViewById(R.id.comment_item_userName);
                    TextView time = (TextView) view.findViewById(R.id.comment_item_time);
                    TextView content = (TextView) view.findViewById(R.id.comment_item_content);
                    fb.display(headImg, Configs.HOST + userInfo.headImg);
                    userName.setText(userInfo.name);
                    time.setText(TimeUtil.getCurrTime());
                    content.setText(comEdit.getText().toString().trim());
                    linear.setVisibility(View.VISIBLE);
                    linear.addView(view);
                    comEdit.setText("");
                    comNum++;
                    comNumText.setText("回应：" + comNum);
                    break;
                case Configs.READ_FAIL:
                    linear.setVisibility(View.GONE);
                    ToastUtil.showToast(context, stat.msg, 0);
                    break;
                case Configs.READ_ERROR:
                    ToastUtil.showToast(context, "数据异常", 0);
                    break;
            }
        }
    };
}