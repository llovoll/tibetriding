/*
 * Copyright (C) 2010 The MobileSecurePay Project
 * All right reserved.
 * author: shiqun.shi@alipay.com
 * 
 *  锟斤拷示锟斤拷锟斤拷位锟饺★拷锟饺ｏ拷锟斤拷锟酵猴拷锟斤拷锟斤拷锟斤拷锟絠d
 *  1.锟斤拷锟斤拷锟角┰贾э拷锟斤拷锟斤拷撕诺锟铰贾э拷锟斤拷锟斤拷锟秸�www.alipay.com)
 *  2.锟斤拷锟斤拷锟斤拷碳曳锟斤拷锟�https://b.alipay.com/order/myorder.htm)
 *  3.锟斤拷锟斤拷锟斤拷锟窖拷锟斤拷锟斤拷锟斤拷锟斤拷(pid)锟斤拷锟斤拷锟斤拷锟斤拷询锟斤拷全校锟斤拷锟斤拷(key)锟斤拷
 */

package cn.ctibet.tibetriding.zfbapply;

//
// 锟斤拷慰锟�Android平台锟斤拷全支锟斤拷锟斤拷锟斤拷(msp)应锟矫匡拷锟斤拷锟接匡拷(4.2 RSA锟姐法签锟斤拷)锟斤拷锟街ｏ拷锟斤拷使锟斤拷压锟斤拷锟斤拷锟叫碉拷openssl RSA锟斤拷钥锟斤拷晒锟斤拷撸锟斤拷锟斤拷一锟斤拷RSA锟斤拷私钥锟斤拷
// 锟斤拷锟斤拷签锟斤拷时锟斤拷只锟斤拷要使锟斤拷锟斤拷傻锟絉SA私钥锟斤拷
// Note: 为锟斤拷全锟斤拷锟绞癸拷锟絉SA私钥锟斤拷锟斤拷签锟斤拷牟锟斤拷锟斤拷锟教ｏ拷应锟矫撅拷锟斤拷锟脚碉拷锟教家凤拷锟斤拷锟斤拷锟斤拷去锟斤拷锟叫★拷
public final class Keys {

	//锟斤拷锟斤拷锟斤拷锟斤拷锟絠d锟斤拷锟斤拷2088锟斤拷头锟斤拷16位锟斤拷锟斤拷锟斤拷
	public static final String DEFAULT_PARTNER = "2088611036928222";

	//锟秸匡拷支锟斤拷锟斤拷锟剿猴拷
	public static final String DEFAULT_SELLER = "2088611036928222";

	public static final String PRIVATE = "MIICdwIBADANBgkqhkiG9w0BAQEFAASCAmEwggJdAgEAAoGBALVZQQ4PD0MuBxopFHdZJg5QscEojB9CUqJNuM6jHmDQvFWB6FAFMTlK8Mq/aHgHeBh5nqFIJXy3fx/orJWMmBl8Gycff3cp2NsbMjy1YUDKq9nQCBpwryJitq4dhrNi9axPDWkWOBFa74rGtLaUjhwHCa6G12dzLMYqFEIjd8g/AgMBAAECgYEAr3imZukRdopx2tFG+VhsZ0uTQ3htZM2y0VLs8ByLh1hR81bYp5zujSZDKZrdhc6MWPpNEHeTtkKDIifr3mSEwfhpVV49fgTnE5VXeS3vW4+UJ2mpoZNcR5+m324+d7LmjibIZ1OTruzT7tsIR24czwW8dhbqSl9wZwozQOKdUXkCQQDxBQhcGPfc62D3Zjl0X3WyDqsAjp75xenjWP0SSmxTUGjayfz8zmZDi5o7PUa0EuHs6CHdpLBCQ9q1gyp0+ElLAkEAwJ7FOD6fXGjFHitI0X57pdBv3i1eAZ+03MnyutnACH61rSrEh8oBg5RJ0NJ3UMdDidd6kco2QfBduqHZkH54XQJBAKiKkWb92s51FvDKXmEfpAkosrIICem2WbEu+IHC4297amedGjmbB+4yAF9uoTOphK2LQqtg0BT2qEmH+LelLHMCQGt4qqVlG1leA41/rRNmNOshTylxup/BUhJWgx4J3IFg4VELKbhx10Jo/H6Mz5G8fExuc67gWkGUMVeuckuSpOkCQGcVGAPu+GUGJDIOYd8rLCyxgIM0zO9/JQiaZAFwR2UtDZ7yoaBY4eQ8FGUGxNVnRAQertzOg13Upb08qJ1RVqo=";

	public static final String PUBLIC = "MIGfMA0GCSqGSIb3DQEBAQUAA4GNADCBiQKBgQCLfOiDF+6ucQ6Jr7UWlM3RxuxBiyh5Yz4JZUkz Vbnww/CL1DJ2I/IXG8A6mGeFCW5kPWj8MbWnE8EqIFgfSqGM6BzYD87fI8rYmcX//4e2/PrjvWCB k+f9HnhfbS8o7BAdqe6as2He98Mm5Mv1cUC4xRxxnMV3ByS+qbvoixJ9NwIDAQAB";

}
