package cn.ctibet.tibetriding.fragment;

import android.content.Context;
import android.os.Bundle;
import android.os.Handler;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;
import cn.ctibet.tibetriding.R;
import cn.ctibet.tibetriding.bean.Configs;
import cn.ctibet.tibetriding.util.CheckFormatUtil;
import cn.ctibet.tibetriding.util.TimeCount;

/**
 * Created by Administrator on 2015/1/23.
 */
public class PhoneRegistFragment extends Fragment implements View.OnClickListener {
    private Context context;
    private TextView codeBtn;
    private EditText phoneEdit;
    private TimeCount time;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.phone_regist_fragment, null);
        context = getActivity();
        initView(view);
        return view;
    }

    private void initView(View view) {
        phoneEdit = (EditText) view.findViewById(R.id.phone_regist_fragment_phoneNum);
        codeBtn = (TextView) view.findViewById(R.id.phone_regist_fragment_code);
        time = new TimeCount(60000, 1000, codeBtn);
        codeBtn.setOnClickListener(this);
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.phone_regist_fragment_code:
                String tel = phoneEdit.getText().toString().trim();
                if (!CheckFormatUtil.checkPhone(tel)) {
                    Toast.makeText(context, "请输入正确的手机号码", Toast.LENGTH_SHORT).show();
                } else {
                    time.start();
                }
                break;
        }
    }

}
