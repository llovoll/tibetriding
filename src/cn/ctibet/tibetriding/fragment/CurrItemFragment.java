package cn.ctibet.tibetriding.fragment;

import android.content.Context;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ListView;
import cn.ctibet.tibetriding.R;
import cn.ctibet.tibetriding.adapter.CurrListAdapter;
import cn.ctibet.tibetriding.bean.CyclingImgBean;

import java.util.ArrayList;
import java.util.List;

public class CurrItemFragment extends Fragment {

    private static final String ARG_POSITION = "position";
    private static final String TERID_POSITION = "terId";

    private Context mContext;

    private int position;
    private String terId;
    private ListView mListView;
    private int terPosition = 0;

    public static CurrItemFragment newInstance(int position) {
        CurrItemFragment cFragment = new CurrItemFragment();
        Bundle b = new Bundle();
        b.putInt(ARG_POSITION, position);
        //b.putString(TERID_POSITION, terId);
        cFragment.setArguments(b);
        return cFragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        mContext = getActivity();
        position = getArguments().getInt(ARG_POSITION);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.curriculum_fragment_item, null);
        init(view);
        return view;
    }

    private void init(View view) {
        mListView = (ListView) view.findViewById(R.id.curriculum_item_list);

        CyclingImgBean bean = null;
        List<CyclingImgBean> list = new ArrayList<CyclingImgBean>();
        for (int i = 0; i < 5; i++) {
            bean = new CyclingImgBean();
            bean.imgname = "西藏骑行" + i;
            list.add(bean);
        }
        CurrListAdapter adapter = new CurrListAdapter(mContext);
        mListView.setAdapter(adapter);
        adapter.addList(list);
        adapter.notifyDataSetChanged();
    }

}
