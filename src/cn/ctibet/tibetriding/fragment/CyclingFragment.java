package cn.ctibet.tibetriding.fragment;

import android.content.Context;
import android.content.Intent;
import android.content.res.Resources;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ListView;
import android.widget.Toast;
import cn.ctibet.tibetriding.R;
import cn.ctibet.tibetriding.activity.ImageChooseActivity;
import cn.ctibet.tibetriding.activity.WaterMarkActivity;
import cn.ctibet.tibetriding.adapter.CyclingListAdapter;
import cn.ctibet.tibetriding.bean.*;
import cn.ctibet.tibetriding.db.AlbumDb;
import cn.ctibet.tibetriding.impl.GridClickListener;
import cn.ctibet.tibetriding.util.SysPrintUtil;
import cn.ctibet.tibetriding.util.UserInfoUtil;

import java.util.ArrayList;
import java.util.List;

/**
 * 骑行相册
 * Created by Administrator on 2015/1/19.
 */
public class CyclingFragment extends Fragment implements GridClickListener {
    private ListView listView;
    private Context context;
    private UserInfo userInfo;
    private List<CyclingImgBean> albumList;
    private List<CyclingImgBean> albumList1;
    private CyclingListAdapter adapter;
    private DailyTrackHistoryBean bean;
    private boolean editorStat = false;
    private ImageChooseActivity imageChooseActivity;

    public CyclingFragment(DailyTrackHistoryBean bean, ImageChooseActivity imageChooseActivity) {
        this.bean = bean;
        this.imageChooseActivity = imageChooseActivity;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.cycling_fragment, null);
        context = getActivity();
        initView(view);
        return view;
    }

    private void initView(View view) {
        userInfo = UserInfoUtil.getUserInfo(context);
        listView = (ListView) view.findViewById(R.id.cycling_fragment_list);

        AlbumDb albumDb = new AlbumDb(context);
        albumList = albumDb.getAlbumData(userInfo.userId);
        albumList1 = albumDb.getAlbumGroupIdData(userInfo.userId);
        long haiba = 0;
        long wendu = 0;
        List<CyclingImgBean> cyclingImgBeans = new ArrayList<CyclingImgBean>();
        CyclingImgBean bean = null;
        for (int i = 0; i < albumList1.size(); i++) {
            bean = new CyclingImgBean();
            bean.bitmapList = new ArrayList<BitmapBean>();

            CyclingImgBean imgBean = albumList1.get(i);
            List<CyclingImgBean> list = albumDb.getAlbumGroupData(userInfo.userId, imgBean.time);
            for (int j = 0; j < list.size(); j++) {
                CyclingImgBean imgBean1 = list.get(j);
                try {
                    haiba += Long.valueOf(imgBean1.haiba);
                } catch (Exception e) {
                }
                wendu += Long.valueOf(imgBean1.wendu);
                BitmapBean bitBean = new BitmapBean();
                bitBean.imgPath = imgBean1.imgname;
                bitBean.isCheck = false;
                bean.bitmapList.add(bitBean);
                bitBean = null;
            }
            bean.location = imgBean.location;
            bean.time = imgBean.time;
            bean.haiba = haiba / list.size() + "m";
            bean.wendu = wendu / list.size() + "℃";
            cyclingImgBeans.add(bean);
        }
        if (null != cyclingImgBeans && cyclingImgBeans.size() > 0) {
            adapter = new CyclingListAdapter(context, cyclingImgBeans,imageChooseActivity);
            listView.setAdapter(adapter);
            adapter.setPath(this);
        } else {
            Toast.makeText(context, "暂无相片", Toast.LENGTH_SHORT).show();
        }
    }


    @Override
    public void getPath(BitmapBean bean1) {
        SysPrintUtil.pt("选择的图片的路径为", Configs.APP_PATH + "galley/" + bean1.imgPath);
        Bundle bundle = new Bundle();
        bundle.putSerializable("bean", bean);
        Intent intent = new Intent(context, WaterMarkActivity.class);
        intent.putExtras(bundle);
        intent.putExtra("Uri", Configs.APP_PATH + "galley/" + bean1.imgPath);
        startActivity(intent);
    }
}
