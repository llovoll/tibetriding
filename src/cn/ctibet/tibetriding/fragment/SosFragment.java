package cn.ctibet.tibetriding.fragment;

import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.drawable.BitmapDrawable;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ImageView;
import android.widget.LinearLayout;
import cn.ctibet.tibetriding.R;
import cn.ctibet.tibetriding.activity.ForHelpActivity;
import cn.ctibet.tibetriding.activity.SosCommentActivity;
import cn.ctibet.tibetriding.adapter.CyclingListAdapter;
import cn.ctibet.tibetriding.adapter.SosListAdapter;
import cn.ctibet.tibetriding.bean.*;
import cn.ctibet.tibetriding.db.Advisetime;
import cn.ctibet.tibetriding.db.Way;
import cn.ctibet.tibetriding.impl.LocationListener;
import cn.ctibet.tibetriding.util.*;
import cn.ctibet.tibetriding.view.XListView;
import com.amap.api.location.AMapLocation;

import java.util.ArrayList;
import java.util.List;

/**
 * SOS
 * Created by Administrator on 2015/1/22.
 */
public class SosFragment extends BaseFragment implements XListView.IXListViewListener, LocationListener {
    private XListView listView;
    private Context context;
    private ImageView addBtn;
    private UserInfo userInfo;
    private String num = "10";
    private boolean isLoad = false;
    private SosBean bean;
    private SosListAdapter adapter;
    private LinearLayout progressLinear;
    private String sid = "";
    private double geoLat;
    private double geoLng;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.sos_fragment, null);
        context = getActivity();
        init();//初始化定位数据
        initView(view);
        setLocationListener(this);
        return view;
    }

    private void initView(View view) {
        userInfo = UserInfoUtil.getUserInfo(context);
        listView = (XListView) view.findViewById(R.id.sos_fragment_listview);
        addBtn = (ImageView) view.findViewById(R.id.sos_fragment_addImg);
        progressLinear = (LinearLayout) view.findViewById(R.id.progress_linear);
        progressLinear.setVisibility(View.VISIBLE);
        listView.setPullRefreshEnable(true);
        listView.setPullLoadEnable(true);
        listView.setXListViewListener(this);

        adapter = new SosListAdapter(context);
        listView.setAdapter(adapter);

        addBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                startActivity(new Intent(context, ForHelpActivity.class));
            }
        });

        listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {
                SosDataBean sosBean = bean.list.get(i - 1);
                Bundle bundle = new Bundle();
                bundle.putSerializable("bean", sosBean);
                Intent intent = new Intent(context, SosCommentActivity.class);
                intent.putExtras(bundle);
                startActivity(intent);
            }
        });
        goodSos();
    }

    @Override
    public void onRefresh() {
        sid = "";
        goodSos();
        isLoad = false;
    }

    @Override
    public void onLoadMore() {
        sid = bean.list.get(adapter.getList().size()-1).sosid;
        goodSos();
        isLoad = true;
    }

    private void goodSos() {
        new Thread(new Runnable() {
            @Override
            public void run() {
                Message msg = new Message();
                String str = "?num=" + num + "&sid=" + sid + TimeUtil.getKeyStr();
                try {
                    String jsonData = SyncHttp.httpGet(Configs.HOST + "Isos/soslist", str);
                    SysPrintUtil.pt("上传到服务器数据为", Configs.HOST + "Isos/soslist" + str);
                    SysPrintUtil.pt("json==获取到的服务器的数据为:", jsonData);
                    bean = JsonUtil.getSosData(jsonData);
                    if (null != bean && null != bean.list && bean.list.size() > 0) {
                        msg.what = Configs.READ_SUCCESS;
                    } else {
                        msg.what = Configs.READ_FAIL;
                    }
                } catch (Exception e) {
                    SysPrintUtil.pt("数据异常", e.toString());
                    e.printStackTrace();
                    msg.what = Configs.READ_ERROR;
                }
                nhandler.sendMessage(msg);
            }
        }).start();
    }

    private Handler nhandler = new Handler() {
        @Override
        public void handleMessage(Message msg) {
            progressLinear.setVisibility(View.GONE);
            listView.stopLoadMore();
            listView.stopRefresh();
            switch (msg.what) {
                case Configs.READ_SUCCESS:
                    if (bean.list.size() < 10) {
                        listView.setPullLoadEnable(false);
                    }
                    adapter.addList(bean.list, geoLat, geoLng, isLoad);
                    adapter.notifyDataSetChanged();
                    break;
                case Configs.READ_FAIL:
                    ToastUtil.showToast(context, "暂无数据", 0);
                    break;
                case Configs.READ_ERROR:
                    ToastUtil.showToast(context, "数据异常", 0);
                    break;
            }
        }
    };

    @Override
    public void getLocationSuccess(AMapLocation aMapLocation) {
        geoLat = aMapLocation.getLatitude();
        geoLng = aMapLocation.getLongitude();
    }

    @Override
    public void getLocationFail(AMapLocation aMapLocation) {
        ToastUtil.showToast(context, "定位失败", 0);
    }
}
