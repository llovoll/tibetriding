package cn.ctibet.tibetriding.fragment;

import android.content.Context;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentActivity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import cn.ctibet.tibetriding.R;
import com.amap.api.maps.AMap;
import com.amap.api.maps.MapView;
import com.amap.api.maps.UiSettings;

/**
 * Created by Administrator on 2015/3/23.
 */
public class RidingFriendsFragment extends Fragment {
    private Context context;
    private MapView mapView;
    private AMap aMap;
    private UiSettings mUiSettings;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view=inflater.inflate(R.layout.riding_friends_fragment,null);
        context = getActivity();
        mapView = (MapView) view.findViewById(R.id.mapView);
        mapView.onCreate(savedInstanceState);// 必须要写
        initView(view);
        return view;
    }

    private void initView(View view) {
        if (aMap == null) {
            aMap = mapView.getMap();
        }
        mUiSettings = aMap.getUiSettings();
        mUiSettings.setZoomControlsEnabled(false);//隐藏缩放按钮
    }

    /**
     * 方法必须重写
     */
    @Override
    public void onResume() {
        super.onResume();
        System.out.println("========onResume1========");
        mapView.onResume();
    }

    /**
     * 方法必须重写
     */
    @Override
    public void onPause() {
        super.onPause();
        System.out.println("========onPause1========");
        mapView.onPause();
    }

    /**
     * 方法必须重写
     */
    @Override
    public void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
        mapView.onSaveInstanceState(outState);
    }

    /**
     * 方法必须重写
     */
    @Override
    public void onDestroy() {
        super.onDestroy();
        mapView.onDestroy();
    }
    @Override
    public void onHiddenChanged(boolean hidden) {
        if (hidden) {
            mapView.onPause();
        } else {
            mapView.onResume();
        }
    }
}
