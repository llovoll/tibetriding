package cn.ctibet.tibetriding.fragment;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import cn.ctibet.tibetriding.R;

public class MainFragment extends Fragment {
    private FragmentManager fm;
    private FragmentTransaction ft;

    private View[] btns;

    public MainFragment(FragmentManager fm) {
        this.fm = fm;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.main_fragment, null);
        initView(view);
        return view;
    }

    private void initView(View view) {

    }



}
