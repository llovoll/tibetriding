package cn.ctibet.tibetriding.fragment;

import android.content.Context;
import android.content.Intent;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import cn.ctibet.tibetriding.R;
import cn.ctibet.tibetriding.activity.JourneyActivity;
import cn.ctibet.tibetriding.activity.RoadLineActivity;
import cn.ctibet.tibetriding.bean.Configs;
import cn.ctibet.tibetriding.impl.RightClickListener;
import cn.ctibet.tibetriding.util.*;
import com.amap.api.maps.AMap;
import com.amap.api.maps.MapView;
import com.amap.api.maps.UiSettings;

/**
 * Created by Administrator on 2015/1/28.
 */
public class RoadBookFragment extends Fragment implements View.OnClickListener, RightClickListener {
    private Context context;
    private MapView mapView;
    private LinearLayout newsView;
    private LinearLayout linearView;
    private TextView pullBtn;
    private ImageView downBtn;
    private AMap aMap;
    private UiSettings mUiSettings;
    private boolean viewShow = false;
    private JourneyActivity activity;

    public RoadBookFragment(JourneyActivity activity) {
        this.activity = activity;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.road_book_fragment, null);
        context = getActivity();
        mapView = (MapView) view.findViewById(R.id.mapView);
        mapView.onCreate(savedInstanceState);// 必须要写
        initView(view);
        return view;
    }

    private void initView(View view) {
        newsView = (LinearLayout) view.findViewById(R.id.road_book_fragment_news);
        linearView = (LinearLayout) view.findViewById(R.id.road_book_fragment_line);
        pullBtn = (TextView) view.findViewById(R.id.road_book_fragment_pull);
        downBtn = (ImageView) view.findViewById(R.id.road_book_fragment_down);

        if (aMap == null) {
            aMap = mapView.getMap();
        }
        mUiSettings = aMap.getUiSettings();
        mUiSettings.setZoomControlsEnabled(false);//隐藏缩放按钮

        pullBtn.setOnClickListener(this);
        downBtn.setOnClickListener(this);

        activity.setClickTag(this);
        getRoadBookData();
    }

    /**
     * 方法必须重写
     */
    @Override
    public void onResume() {
        super.onResume();
        System.out.println("========onResume1========");
        mapView.onResume();
    }

    /**
     * 方法必须重写
     */
    @Override
    public void onPause() {
        super.onPause();
        System.out.println("========onPause1========");
        mapView.onPause();
    }

    @Override
    public void onHiddenChanged(boolean hidden) {
        if (hidden) {
            mapView.onPause();
        } else {
            mapView.onResume();
        }
    }

    /**
     * 方法必须重写
     */
    @Override
    public void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
        mapView.onSaveInstanceState(outState);
    }

    /**
     * 方法必须重写
     */
    @Override
    public void onDestroy() {
        super.onDestroy();
        mapView.onDestroy();
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.road_book_fragment_pull:
                if (!viewShow) {
                    viewShow = true;
                    /*Animation animation = AnimationUtils.loadAnimation(
                            context, R.anim.dialog_enter);
                    linearView.setAnimation(animation);*/
                    newsView.setVisibility(View.VISIBLE);
                    pullBtn.setBackgroundResource(R.drawable.pull_down_selector);
                } else {
                    viewShow = false;
                   /* Animation animation = AnimationUtils.loadAnimation(
                            context, R.anim.dialog_exit);
                    linearView.setAnimation(animation);*/
                    newsView.setVisibility(View.GONE);
                    pullBtn.setBackgroundResource(R.drawable.pull_up_selector);

                }
                break;
        }
    }

    @Override
    public void getClick() {
        startActivity(new Intent(context, RoadLineActivity.class));
        if (Build.VERSION.SDK_INT > 5) {
            getActivity().overridePendingTransition(R.anim.zoomlrin, R.anim.zoomlrout);
        }
    }

    private void getRoadBookData() {
        new Thread(new Runnable() {
            @Override
            public void run() {
                Message msg = new Message();
                String str = "?addtime=" + "&wayid=" + 31 + TimeUtil.getKeyStr();
                try {
                    String jsonData = SyncHttp.httpGet(Configs.HOST + "Way/getWayBook", str);
                    SysPrintUtil.pt("上传到服务器数据为", Configs.HOST + "Way/getWayBook" + str);
                    SysPrintUtil.pt("json==获取到的服务器的数据为:", jsonData);
                   /* bean = JsonUtil.getReleaseLiveStat(jsonData);
                    if (null != bean && bean.code.equals("200")) {
                        msg.what = Configs.READ_SUCCESS;
                    } else {
                        msg.what = Configs.READ_FAIL;
                    }*/
                } catch (Exception e) {
                    SysPrintUtil.pt("数据异常", e.toString());
                    e.printStackTrace();
                    msg.what = Configs.READ_ERROR;
                }
                nhandler.sendMessage(msg);
            }
        }).start();
    }

    private Handler nhandler = new Handler() {
        @Override
        public void handleMessage(Message msg) {
            switch (msg.what) {
                case Configs.READ_SUCCESS:
                    ToastUtil.showToast(context, "评论成功", 0);
                    break;
                case Configs.READ_FAIL:
                    ToastUtil.showToast(context, "评论失败", 0);
                    break;
                case Configs.READ_ERROR:
                    ToastUtil.showToast(context, "数据异常", 0);
                    break;
            }
        }
    };
}
