package cn.ctibet.tibetriding.fragment;

import android.content.Context;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ListView;
import cn.ctibet.tibetriding.R;
import cn.ctibet.tibetriding.adapter.DirectMsgListAdapter;
import cn.ctibet.tibetriding.bean.CyclingImgBean;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Administrator on 2015/3/23.
 */
public class DirectMsgFragment extends Fragment {
    private Context context;
    private ListView listView;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.direct_messages_fragment, null);
        context = getActivity();
        initView(view);
        return view;
    }

    private void initView(View view) {
        listView = (ListView) view.findViewById(R.id.direct_messages_fragment_list);
        DirectMsgListAdapter adapter = new DirectMsgListAdapter(context);
        listView.setAdapter(adapter);

        List<CyclingImgBean> list = new ArrayList<CyclingImgBean>();
        CyclingImgBean bean = null;
        for (int i = 0; i < 5; i++) {
            bean = new CyclingImgBean();
            bean.imgname = "张三" + i;
            list.add(bean);
        }

        adapter.addList(list);
        adapter.notifyDataSetChanged();

    }
}
