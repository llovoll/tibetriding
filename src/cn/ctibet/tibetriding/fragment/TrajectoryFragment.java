package cn.ctibet.tibetriding.fragment;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.drawable.BitmapDrawable;
import android.net.Uri;
import android.os.Bundle;
import android.provider.MediaStore;
import android.support.v4.app.Fragment;
import android.view.*;
import android.widget.*;
import cn.ctibet.tibetriding.R;
import cn.ctibet.tibetriding.activity.ImageChooseActivity;
import cn.ctibet.tibetriding.activity.JourneyActivity;
import cn.ctibet.tibetriding.activity.WaterMarkActivity;
import cn.ctibet.tibetriding.impl.Right1ClickListener;
import cn.ctibet.tibetriding.impl.RightClickListener;
import cn.ctibet.tibetriding.util.BitMapUtil;
import cn.ctibet.tibetriding.util.SdCardUtil;
import cn.ctibet.tibetriding.util.UriToFilePath;
import cn.ctibet.tibetriding.util.Utils;
import cn.ctibet.tibetriding.view.ScoreDialog;
import com.amap.api.maps.AMap;
import com.amap.api.maps.MapView;
import com.amap.api.maps.UiSettings;

/**
 * Created by Administrator on 2015/1/28.
 */
public class TrajectoryFragment extends Fragment implements View.OnClickListener, Right1ClickListener {
    private static final int CAMERA = 101;
    private static final int GALLEY = 102;
    private Context context;
    private Uri cameraUri;
    private Bitmap bitmap;
    private PopupWindow popupWindow;
    private RelativeLayout stopBtn;
    private MapView mapView;
    private AMap aMap;
    private UiSettings mUiSettings;
    private JourneyActivity activity;

    public TrajectoryFragment(JourneyActivity activity) {
        this.activity = activity;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.trajectory_fragment, null);
        context = getActivity();
        mapView = (MapView) view.findViewById(R.id.mapView);
        mapView.onCreate(savedInstanceState);// 必须要写
        initView(view);
        return view;
    }

    private void initView(View view) {

        stopBtn = (RelativeLayout) view.findViewById(R.id.daily_exercise_activity_stop);
        if (aMap == null) {
            aMap = mapView.getMap();
        }
        mUiSettings = aMap.getUiSettings();
        mUiSettings.setZoomControlsEnabled(false);//隐藏缩放按钮

        stopBtn.setOnClickListener(this);
        activity.setClickTag1(this);
    }

    /**
     * 方法必须重写
     */
    @Override
    public void onResume() {
        super.onResume();
        System.out.println("========onResume2========");
        mapView.onResume();
    }

    /**
     * 方法必须重写
     */
    @Override
    public void onPause() {
        super.onPause();
        System.out.println("========onPause2========");
        mapView.onPause();
    }

    @Override
    public void onHiddenChanged(boolean hidden) {
        if (hidden) {
            mapView.onPause();
        } else {
            mapView.onResume();
        }
    }

    /**
     * 方法必须重写
     */
    @Override
    public void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
        mapView.onSaveInstanceState(outState);
    }

    /**
     * 方法必须重写
     */
    @Override
    public void onDestroy() {
        super.onDestroy();
        mapView.onDestroy();
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.daily_exercise_activity_stop:
                initPopWindow();
                popupWindow.showAtLocation(view, Gravity.CENTER, 0, 0);
                break;
        }
    }

    private void initPopWindow() {
        View view = LayoutInflater.from(context).inflate(
                R.layout.pop_view, null);
        TextView sureBtn = (TextView) view.findViewById(R.id.pop_view_sure);
        TextView errorBtn = (TextView) view.findViewById(R.id.pop_view_touch_error);
        popupWindow = new PopupWindow(view, LinearLayout.LayoutParams.FILL_PARENT,
                LinearLayout.LayoutParams.FILL_PARENT, true);
        popupWindow.setContentView(view);
        popupWindow.setFocusable(true);
        popupWindow.setOutsideTouchable(false);
        popupWindow.setBackgroundDrawable(new BitmapDrawable());
        view.setFocusable(true);
        view.setFocusableInTouchMode(true);
        view.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (null != popupWindow) {
                    popupWindow.dismiss();
                }
            }
        });
    }


    @Override
    public void getClick1() {
            showDialog();
    }

    private void showDialog() {
        final ScoreDialog dialog = new ScoreDialog(context, R.layout.select_camera_dialog1, R.style.dialog_more_style);
        dialog.setParamsBotton();
        dialog.show();
        if (!SdCardUtil.ExistSDCard()) {
            Toast.makeText(context, getResources()
                    .getString(R.string.null_sdcrad), Toast.LENGTH_SHORT).show();
            return;
        }
        dialog.findViewById(R.id.camera).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                dialog.dismiss();
                Intent camera = new Intent(
                        MediaStore.ACTION_IMAGE_CAPTURE);
                cameraUri = Utils.createImagePathUri(context);
                camera.putExtra(MediaStore.EXTRA_OUTPUT, cameraUri);
                startActivityForResult(camera, CAMERA);
            }
        });
        dialog.findViewById(R.id.album).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                dialog.dismiss();
                startActivity(new Intent(context, ImageChooseActivity.class));
            }
        });
        dialog.findViewById(R.id.guiji).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                dialog.dismiss();
                Toast.makeText(context, "未实现", Toast.LENGTH_SHORT).show();
            }
        });
        dialog.findViewById(R.id.back).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                dialog.dismiss();
            }
        });
        Window dialogWindow = dialog.getWindow();
        dialogWindow.setWindowAnimations(R.style.dialog_more_style);
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        switch (requestCode) {
            case CAMERA:
                if (resultCode == Activity.RESULT_OK
                        && null == data) {
                    String img_path = UriToFilePath.getFilePath(getActivity(), cameraUri);
                    Intent intent = new Intent(context, WaterMarkActivity.class);
                    intent.putExtra("Uri", img_path);
                    startActivity(intent);
                }
                break;
            case GALLEY:
                if (null != data) {
                    bitmap = BitMapUtil.compressImage(BitMapUtil.getBitmapFromUri(context, data.getData()));
                }
                break;
        }
    }

}
