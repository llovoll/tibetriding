package cn.ctibet.tibetriding.fragment;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.*;
import cn.ctibet.tibetriding.R;
import cn.ctibet.tibetriding.activity.TrackDetailActivity;
import cn.ctibet.tibetriding.bean.HistoryItemBean;
import cn.ctibet.tibetriding.impl.SideClickListener;
import cn.ctibet.tibetriding.view.ListViewCompat;
import cn.ctibet.tibetriding.view.SlideView;

import java.util.ArrayList;
import java.util.List;

/**
 * 线路骑行
 * Created by Administrator on 2015/1/28.
 */
public class LineRidingFragment extends Fragment implements SlideView.OnSlideListener, SideClickListener {
    private Context context;
    private ListViewCompat mListView;
    private List<HistoryItemBean> mHistoryItemBeans = new ArrayList<HistoryItemBean>();
    private SlideView mLastSlideViewWithStatusOn;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.line_riding_fragment, null);
        context = getActivity();
        initView(view);
        return view;
    }

    private void initView(View view) {
        mListView = (ListViewCompat) view.findViewById(R.id.line_riding_fragment_listView);

        for (int i = 0; i < 3; i++) {
            HistoryItemBean item = new HistoryItemBean();

            if (i % 3 == 0) {
                item.iconRes = R.drawable.default_local;
                item.title = "川藏骑行";
                item.msg = "青岛爆炸满月：大量鱼虾死亡";
                item.time = "晚上18:18";
            } else if (i % 3 == 1) {
                item.iconRes = R.drawable.default_local;
                item.title = "川藏骑行";
                item.msg = "欢迎你使用微信";
                item.time = "12月18日";
            } else if (i % 3 == 2) {
                item.iconRes = R.drawable.default_local;
                item.title = "川藏骑行";
                item.msg = "川藏骑行";
                item.time = "看见对方s";
            }
            mHistoryItemBeans.add(item);
        }
        mListView.setAdapter(new SlideAdapter());
    }

    @Override
    public void itemClick(int pos) {
        System.out.println("点击的位置为=====" + pos);
        startActivity(new Intent(context, TrackDetailActivity.class));
    }

    private class SlideAdapter extends BaseAdapter {

        private LayoutInflater mInflater;

        SlideAdapter() {
            mInflater = getActivity().getLayoutInflater();
        }

        @Override
        public int getCount() {
            return mHistoryItemBeans.size();
        }

        @Override
        public Object getItem(int position) {
            return mHistoryItemBeans.get(position);
        }

        @Override
        public long getItemId(int position) {
            return position;
        }

        @Override
        public View getView(final int position, View convertView,
                            ViewGroup parent) {
            ViewHolder holder;
            SlideView slideView = (SlideView) convertView;
            if (slideView == null) {
                View itemView = mInflater.inflate(R.layout.line_riding_list_item, null);

                slideView = new SlideView(context);
                slideView.setSideListener(LineRidingFragment.this);
                slideView.setContentView(itemView);

                holder = new ViewHolder(slideView);
                slideView.setOnSlideListener(LineRidingFragment.this);
                slideView.setTag(holder);
            } else {
                holder = (ViewHolder) slideView.getTag();
            }
            HistoryItemBean item = mHistoryItemBeans.get(position);
            item.slideView = slideView;
            item.slideView.shrink();

            holder.img.setImageResource(item.iconRes);
            holder.title.setText(item.title);

            /**
             * 删除的监听
             */
            holder.deleteHolder.setOnClickListener(new View.OnClickListener() {

                @Override
                public void onClick(View v) {
                    // TODO Auto-generated method stub
                    mHistoryItemBeans.remove(position);
                    SlideAdapter.this.notifyDataSetChanged();
                }
            });

            return slideView;
        }
    }


    private static class ViewHolder {
        public ImageView img;
        public TextView title;
        public ViewGroup deleteHolder;

        ViewHolder(View view) {
            img = (ImageView) view.findViewById(R.id.line_riding_list_item_img);
            title = (TextView) view.findViewById(R.id.line_riding_list_item_name);
            deleteHolder = (ViewGroup) view.findViewById(R.id.holder);
        }
    }


    @Override
    public void onSlide(View view, int status) {
        if (mLastSlideViewWithStatusOn != null
                && mLastSlideViewWithStatusOn != view) {
            mLastSlideViewWithStatusOn.shrink();
        }

        if (status == SLIDE_STATUS_ON) {
            mLastSlideViewWithStatusOn = (SlideView) view;
        }
    }
}
