package cn.ctibet.tibetriding.fragment;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import cn.ctibet.tibetriding.R;
import cn.ctibet.tibetriding.activity.ForHelpActivity;
import cn.ctibet.tibetriding.adapter.SosListAdapter;
import cn.ctibet.tibetriding.bean.Configs;
import cn.ctibet.tibetriding.bean.SosBean;
import cn.ctibet.tibetriding.bean.UserInfo;
import cn.ctibet.tibetriding.util.*;
import cn.ctibet.tibetriding.view.XListView;

/**
 * 我的求助
 * Created by Administrator on 2015/1/22.
 */
public class MyForHelpFragment extends Fragment implements XListView.IXListViewListener {
    private Context context;
    private XListView listView;
    private ImageView addBtn;
    private UserInfo userInfo;
    private LinearLayout progressLinear;
    private SosListAdapter adapter;
    private int page=1;
    private boolean isLoad=false;
    private SosBean bean;
    private String num="10";
    private String sid="";

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.my_for_help_fragment, null);
        context = getActivity();
        initView(view);
        return view;
    }
    private void initView(View view) {
        userInfo = UserInfoUtil.getUserInfo(context);
        listView = (XListView) view.findViewById(R.id.sos_fragment_listview);
        addBtn = (ImageView) view.findViewById(R.id.sos_fragment_addImg);
        progressLinear = (LinearLayout) view.findViewById(R.id.progress_linear);
        progressLinear.setVisibility(View.VISIBLE);
        listView.setPullRefreshEnable(true);
        listView.setPullLoadEnable(true);
        listView.setXListViewListener(this);

        adapter = new SosListAdapter(context);
        listView.setAdapter(adapter);

        addBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                startActivity(new Intent(context, ForHelpActivity.class));
            }
        });
        getMyHelpData();
    }

    @Override
    public void onRefresh() {
        sid = "";
        getMyHelpData();
        isLoad = false;
    }

    @Override
    public void onLoadMore() {
        sid = bean.list.get(adapter.getList().size()-1).sosid;
        getMyHelpData();
        isLoad = true;
    }

    private void getMyHelpData() {
        new Thread(new Runnable() {
            @Override
            public void run() {
                Message msg = new Message();
                String str = "?userid=" + userInfo.userId + "&num=" + num + "&sid=" + sid + TimeUtil.getKeyStr();
                try {
                    String jsonData = SyncHttp.httpGet(Configs.HOST + "Isos/mysos", str);
                    SysPrintUtil.pt("上传到服务器数据为", Configs.HOST + "Isos/mysos" + str);
                    SysPrintUtil.pt("json==获取到的服务器的数据为:", jsonData);
                    bean = JsonUtil.getSosData(jsonData);
                    if (null != bean && null != bean.list && bean.list.size() > 0) {
                        msg.what = Configs.READ_SUCCESS;
                    } else {
                        msg.what = Configs.READ_FAIL;
                    }
                } catch (Exception e) {
                    SysPrintUtil.pt("数据异常", e.toString());
                    e.printStackTrace();
                    msg.what = Configs.READ_ERROR;
                }
                nhandler.sendMessage(msg);
            }
        }).start();
    }

    private Handler nhandler = new Handler() {
        @Override
        public void handleMessage(Message msg) {
            progressLinear.setVisibility(View.GONE);
            listView.stopLoadMore();
            listView.stopRefresh();
            switch (msg.what) {
                case Configs.READ_SUCCESS:
                    if (bean.list.size() < 10) {
                        listView.setPullLoadEnable(false);
                    }
                    adapter.addList(bean.list,0,0, isLoad);
                    break;
                case Configs.READ_FAIL:
                    ToastUtil.showToast(context,"暂无数据",0);
                    break;
                case Configs.READ_ERROR:
                    ToastUtil.showToast(context,"数据异常",0);
                    break;
            }
        }
    };
}
