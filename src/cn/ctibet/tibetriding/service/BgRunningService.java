/*
package cn.ctibet.tibetriding.service;

import android.app.ActivityManager;
import android.app.Service;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.location.Location;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.Bundle;
import android.os.IBinder;
import android.util.Log;
import cn.ctibet.tibetriding.bean.Configs;
import cn.ctibet.tibetriding.util.SysPrintUtil;
import com.amap.api.location.AMapLocation;
import com.amap.api.location.AMapLocationListener;
import com.amap.api.location.LocationManagerProxy;
import com.amap.api.location.LocationProviderProxy;

*/
/**
 * package_name: com.ly.huanxing.service
 * Created with IntelliJ IDEA
 * User: Administrator
 * Date: 2014/11/30
 * Time: 12:10
 *//*

public class BgRunningService extends Service implements AMapLocationListener {
    private static final String TAG = "AppStatusService";
    private Context context;
    private ReceiveBroadCast mReceiver;
    private LocationManagerProxy mLocationManagerProxy;

    @Override
    public IBinder onBind(Intent intent) {
        // TODO Auto-generated method stub
        return null;
    }

    @Override
    public void onCreate() {
        super.onCreate();
        init();
        mReceiver = new ReceiveBroadCast();
        IntentFilter mFilter = new IntentFilter();
        mFilter.addAction(Configs.TRAJECTORY_HISTORY);
        registerReceiver(mReceiver, mFilter);
    }

    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {
        // TODO Auto-generated method stub
        context = this;
        System.out.println("启动服务");
        return super.onStartCommand(intent, flags, startId);
    }

    @Override
    public void onDestroy() {
        // TODO Auto-generated method stub
        super.onDestroy();
        System.out.println("终止服务");
        unregisterReceiver(mReceiver);
    }

    public class ReceiveBroadCast extends BroadcastReceiver {


        @Override
        public void onReceive(Context context, Intent intent) {
            String action = intent.getAction();
            if (action.equals(Configs.TRAJECTORY_HISTORY)) {

            }
        }

    }

    */
/**
     * 初始化定位
     *//*

    private void init() {

        mLocationManagerProxy = LocationManagerProxy.getInstance(this);

        //此方法为每隔固定时间会发起一次定位请求，为了减少电量消耗或网络流量消耗，
        //注意设置合适的定位时间的间隔，并且在合适时间调用removeUpdates()方法来取消定位请求
        //在定位结束后，在合适的生命周期调用destroy()方法
        //其中如果间隔时间为-1，则定位只定一次
        mLocationManagerProxy.requestLocationData(
                LocationProviderProxy.AMapNetwork, 60 * 1000, 15, this);
        mLocationManagerProxy.setGpsEnable(false);
    }

    @Override
    public void onLocationChanged(AMapLocation aMapLocation) {
        if (aMapLocation != null && aMapLocation.getAMapException().getErrorCode() == 0) {
            //获取位置信息
            Double geoLat = aMapLocation.getLatitude();
            Double geoLng = aMapLocation.getLongitude();
            Double altitude = aMapLocation.getAltitude();
            SysPrintUtil.pt("骑行相册经纬度111", geoLat + "," + geoLng + "," + altitude);
        }
    }

    @Override
    public void onLocationChanged(Location location) {

    }

    @Override
    public void onStatusChanged(String s, int i, Bundle bundle) {

    }

    @Override
    public void onProviderEnabled(String s) {

    }

    @Override
    public void onProviderDisabled(String s) {

    }
}*/
