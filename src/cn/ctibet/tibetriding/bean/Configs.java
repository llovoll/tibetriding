package cn.ctibet.tibetriding.bean;

import android.graphics.Bitmap;
import android.os.Environment;

/**
 * Created by Administrator on 2015/1/20.
 */
public class Configs {

    //public static final String HOST = "http://192.168.0.135/xizang/index.php/";
    public static final String HOST_IMG = "http://121.40.34.128:8060";
    public static final String HOST = "http://121.40.34.128:8060/index.php/";
    public static final String APP_PATH = Environment
            .getExternalStorageDirectory() + "/xzqx/";
    public static final String SETTING_INFO = "SETTING_INFO";
    public static final String USER_ID = "USER_ID";
    public static final String NAME = "NAME";
    public static final String PASSWORD = "PASSWORD";
    public static final String POS = "POS";
    public static final int READ_SUCCESS = 1001;
    public static final int READ_FAIL = 1002;
    public static final int READ_ERROR = 1003;

    public final static String TRAJECTORY_HISTORY = "cn.ctibet.tibetriding.history";

    /**
     * 判断位置移动的阀值
     */
    public static final float UPDATE_LOCATION_THRESHOLD = 10f;

    public static Bitmap bit = null;
}
