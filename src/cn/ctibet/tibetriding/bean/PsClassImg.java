package cn.ctibet.tibetriding.bean;

import java.io.Serializable;

/**
 * @Project: WzSchool
 * @Title: PsClassImg.java
 * @Package com.wz.model
 * @Description: TODO
 * 
 * @author dyong199046@163.com 代勇
 * @date 2014-7-4 上午11:07:56
 * @Copyright: 2014 呐喊信息技术 All rights reserved.
 * @version V1.0
 */
public class PsClassImg implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	public int id;
	public String imgpath;

}
