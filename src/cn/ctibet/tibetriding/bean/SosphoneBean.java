package cn.ctibet.tibetriding.bean;

/**
 * 求助电话
 * 
 * @author liuzhao
 * @since 2015-2-7
 */
public class SosphoneBean {
	/**
	 * 求助电话ID
	 */
	public int sosphoneid;
	/**
	 * 线路ID
	 */
	public int wayid;
	/**
	 * 机构名称
	 */
	public String name = "";
	/**
	 * 电话
	 */
	public String telephone = "";
	/**
	 * 是否删除
	 */
	public int isdelete;
	/**
	 * 发布时间
	 */
	public String addtime = "";

	@Override
	public String toString() {
		return "SosphoneBean [sosphoneid=" + sosphoneid + ", wayid=" + wayid
				+ ", name=" + name + ", telephone=" + telephone + ", isdelete="
				+ isdelete + ", addtime=" + addtime + "]";
	}

}
