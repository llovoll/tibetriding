package cn.ctibet.tibetriding.bean;

import java.util.ArrayList;
import java.util.List;

/**
 * 线路
 * 
 * @author liuzhao
 * @since 2015-2-5
 */
public class WayBean {
	/**
	 * 线路ID。
	 */
	public int Wayid;
	/**
	 * 线路名称。
	 */
	public String Wayname = "";
	/**
	 * 线路图片。
	 */
	public String Wayimg = "";
	/**
	 * 线路描述。
	 */
	public String Waydesc = "";
	/**
	 * 最长参与时间，用于超期自动完成，单位：天。
	 */
	public int Maxday;
	/**
	 * 最短参与时间，用来作为报名另一条线路的条件，单位：天。
	 */
	public int Minday;
	/**
	 * 发布时间，该值会在添加、修改和删除线路时发生改变，因此需要根据其更新线路数据。
	 */
	public String Addtime = "";
	/**
	 * 线路类型。
	 */
	public int Waytype;
	/**
	 * 是否删除。
	 */
	public int Isdelete;
    public String code="";
    public String msg="";
    public String advisetime="";
    public List<AdvisetimeBean> advisetimeList=null;

    @Override
	public String toString() {
		return "WayBean [Wayid=" + Wayid + ", Wayname=" + Wayname + ", Wayimg="
				+ Wayimg + ", Waydesc=" + Waydesc + ", Maxday=" + Maxday
				+ ", Minday=" + Minday + ", Addtime=" + Addtime + ", Waytype="
				+ Waytype + ", Isdelete=" + Isdelete + "]";
	}

}
