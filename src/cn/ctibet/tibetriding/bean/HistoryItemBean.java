package cn.ctibet.tibetriding.bean;

import cn.ctibet.tibetriding.view.SlideView;

/**
 * Created by Administrator on 2015/1/29.
 */
public class HistoryItemBean {
    public int iconRes;
    public String title;
    public String msg;
    public String time;
    public SlideView slideView;
}
