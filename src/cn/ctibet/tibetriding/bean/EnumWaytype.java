package cn.ctibet.tibetriding.bean;

/**
 * 线路类型
 * 
 * @author liuzhao
 * @since 2015-2-5
 */
public enum EnumWaytype {
	LINE_RIDING(1);

	private int Value;

	EnumWaytype(int value) {
		Value = value;
	}

	public int getValue() {
		return Value;
	}
}
