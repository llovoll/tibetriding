package cn.ctibet.tibetriding.bean;

/**
 * Created by Administrator on 2015/3/19.
 */
public class RouteInfoBean {
    public String routeid="";
    public String wayid="";
    public String beginprovince="";
    public String begincity="";
    public String beginroutename="";
    public String beginlng="";
    public String beginlat="";
    public String daynum="";
    public String parentid="";
    public String addtime="";
    public String endlng="";
    public String endlat="";
    public String trackset="";
    public String endprovince="";
    public String endcity="";
    public String endroutename="";
    public String code="";
    public String msg="";
}
