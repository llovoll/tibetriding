package cn.ctibet.tibetriding.bean;

/**
 * 删除状态
 * 
 * @author liuzhao
 * @since 2015-2-5
 */
public enum EnumIsDeleted {
	UNDELETED(0), DELETED(1);

	private int Value;

	EnumIsDeleted(int value) {
		Value = value;
	}

	public int getValue() {
		return Value;
	}
}
