package cn.ctibet.tibetriding.bean;

/**
 * 骑行状态:开始、暂停和完成
 * 
 * @author liuzhao
 * @since 2015-1-25
 */
public enum EnumRidingStatus {
	STARTED(0), PAUSED(1), FINISHED(2);

	private int Value;

	EnumRidingStatus(int value) {
		Value = value;
	}

	public int getValue() {
		return Value;
	}
}
