package cn.ctibet.tibetriding.bean;

/**
 * 企业类型
 * 
 * @author liuzhao
 * @since 2015-2-5
 */
public enum EnumCompanyType {
	STAY(1), RESTAURANT(2),SERVICE(3),EQUIPMENT(4);

	private int Value;

	EnumCompanyType(int value) {
		Value = value;
	}

	public int getValue() {
		return Value;
	}

}
