package cn.ctibet.tibetriding.bean;

/**
 * 行程描述
 * 
 * @author liuzhao
 * @since 2015-2-6
 */
public class RoutedescBean {
	/**
	 * 行程描述ID
	 */
	public int Routedescid;
	/**
	 * 行程ID
	 */
	public int Routeid;
	/**
	 * 标题
	 */
	public String Title = "";
	/**
	 * 描述内容，HTML结构
	 */
	public String Content = "";
	/**
	 * 发布时间
	 */
	public String Addtime = "";

	@Override
	public String toString() {
		return "RoutedescBean [Routedescid=" + Routedescid + ", Routeid="
				+ Routeid + ", Title=" + Title + ", Content=" + Content
				+ ", Addtime=" + Addtime + "]";
	}

}
