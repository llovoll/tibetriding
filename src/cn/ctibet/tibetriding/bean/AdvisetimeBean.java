package cn.ctibet.tibetriding.bean;

/**
 * 建议出行时间
 * 
 * @author liuzhao
 * @since 2015-2-5
 */
public class AdvisetimeBean {
	/**
	 * 时间ID
	 */
	public int Waytimeid;
	/**
	 * 线路ID
	 */
	public int Wayid;
	/**
	 * 开始时间
	 */
	public String Begintime = "";
	/**
	 * 截止时间
	 */
	public String Endtime = "";
	/**
	 * 月份
	 */
	public int Month;
	/**
	 * 原价
	 */
	public float Price;
	/**
	 * 优惠价
	 */
	public float Cutprice;

	@Override
	public String toString() {
		return "AdvisetimeBean [Waytimeid=" + Waytimeid + ", Wayid=" + Wayid
				+ ", Begintime=" + Begintime + ", Endtime=" + Endtime
				+ ", Month=" + Month + ", Price=" + Price + ", Cutprice="
				+ Cutprice + "]";
	}

}
