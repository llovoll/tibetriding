package cn.ctibet.tibetriding.bean;

import android.graphics.Bitmap;
import net.tsz.afinal.FinalBitmap;

import java.util.List;

/**
 * Created by Administrator on 2015/1/19.
 */
public class CyclingImgBean {
    public int id=0;
    public String location="";
    public String haiba="";
    public String wendu="";
    public String time="";
    public List<BitmapBean> bitmapList=null;
    public List<Bitmap> bitmapList1=null;
    public String imgname="";
    public String type="";
    public boolean isClick=false;
}
