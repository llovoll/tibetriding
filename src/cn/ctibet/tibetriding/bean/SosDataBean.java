package cn.ctibet.tibetriding.bean;

import java.io.Serializable;

/**
 * Created by Administrator on 2015/2/6.
 */
public class SosDataBean implements Serializable{
    public String sosid="";
    public String wayid="";
    public String userid="";
    public String sostype="";
    public String soscontent="";
    public String sosimg="";
    public String soslat="";
    public String soslng="";
    public String addtime="";
    public String nickname="";
    public String typename="";
    public String aplynum="";
    public String userimg="";
    public String countComment="";
}
