package cn.ctibet.tibetriding.bean;

/**
 * 商家
 * 
 * @author liuzhao
 * @since 2015-2-6
 */
public class CompanyBean {
	/**
	 * 商家ID
	 */
	public int companyid;
	/**
	 * 商家所在位置经度
	 */
	public String companylng = "";
	/**
	 * 商家所在位置纬度
	 */
	public String companylat = "";
	/**
	 * 商家名称
	 */
	public String companyname = "";
	/**
	 * 商家类型
	 */
	public String companytype = "";
	/**
	 * 商家电话
	 */
	public String companytel = "";
	/**
	 * 联系地址
	 */
	public String companyaddr = "";
	/**
	 * 商家描述
	 */
	public String companydesc = "";
	/**
	 * 发布时间
	 */
	public String addtime = "";
	/**
	 * 是否删除
	 */
	public int isdelete;
	/**
	 * 所在省
	 */
	public String province = "";
	/**
	 * 所在市
	 */
	public String city = "";
	/**
	 * 所在县
	 */
	public String xian = "";

	@Override
	public String toString() {
		return "CompanyBean [companyid=" + companyid + ", companylng="
				+ companylng + ", companylat=" + companylat + ", companyname="
				+ companyname + ", companytype=" + companytype
				+ ", companytel=" + companytel + ", companyaddr=" + companyaddr
				+ ", companydesc=" + companydesc + ", addtime=" + addtime
				+ ", isdelete=" + isdelete + ", province=" + province
				+ ", city=" + city + ", xian=" + xian + "]";
	}
}
