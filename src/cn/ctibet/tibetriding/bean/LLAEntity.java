package cn.ctibet.tibetriding.bean;

/**
 * LLA实体
 * 
 * @author liuzhao
 * @since 2015-2-2
 */
public class LLAEntity {
	private double Lat;
	private double Lng;
	private double Alt;

	public LLAEntity() {
		// TODO Auto-generated constructor stub
	}

	/**
	 * 构造LLA实体
	 * 
	 * @param lng
	 *            经度
	 * @param lat
	 *            纬度
	 * @param alt
	 *            海拔
	 */
	public LLAEntity(double lng, double lat, double alt) {
		this();
		Lat = lat;
		Lng = lng;
		Alt = alt;
	}

	/**
	 * 经度、纬度
	 * 
	 * @param lng
	 *            经度
	 * @param lat
	 *            纬度
	 */
	public LLAEntity(double lng, double lat) {
		this(lng, lat, 0);
	}

	/**
	 * 获取纬度
	 * 
	 * @return 纬度
	 */
	public double getLat() {
		return Lat;
	}

	/**
	 * 设置纬度
	 * 
	 * @param lat
	 *            纬度
	 */
	public void setLat(double lat) {
		Lat = lat;
	}

	/**
	 * 获取经度
	 * 
	 * @return 经度
	 */
	public double getLng() {
		return Lng;
	}

	/**
	 * 设置经度
	 * 
	 * @param lng
	 *            经度
	 */
	public void setLng(double lng) {
		Lng = lng;
	}

	/**
	 * 获取海拔
	 * 
	 * @return 海拔
	 */
	public double getAlt() {
		return Alt;
	}

	/**
	 * 设置海拔
	 * 
	 * @param alt
	 *            海拔
	 */
	public void setAlt(double alt) {
		Alt = alt;
	}

	@Override
	public String toString() {
		return Lng + "," + Lat + "," + Alt;
	}
}
