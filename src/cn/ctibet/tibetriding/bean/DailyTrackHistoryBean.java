package cn.ctibet.tibetriding.bean;

import java.io.Serializable;

/**
 * 日常锻炼
 * 
 * @author liuzhao
 * @since 2015-2-3
 */
public class DailyTrackHistoryBean implements Serializable{
	/**
	 * 日常锻炼ID
	 */
	public int Dailytrackhistoryid;
	/**
	 * 历史轨迹ID
	 */
	public String Trackhistoryid;
	/**
	 * 用户ID
	 */
	public String Userid;
	/**
	 * 缩略图
	 */
	public String Smallimg = "";
	/**
	 * 骑行类型，有“日常锻炼”和“线路骑行”两种，对应的值见枚举“EnumRidingType”
	 */
	public int Biketype;
	/**
	 * 轨迹数据，如“经度1,纬度1,海拔1|经度2,纬度2,海拔2|经度3,纬度3,海拔3...”
	 */
	public String Trackset = "";
	/**
	 * 骑行状态，有“开始”、“暂停”和“完成”三种，对应的值见枚举“EnumRidingStatus”
	 */
	public int Status;
	/**
	 * 总里程。单位：KM
	 */
	public float Totalmileage;
	/**
	 * 总耗时。单位：小时
	 */
	public String Casttime = "";
	/**
	 * 开始时间。单位：毫秒数。
	 */
	public String Begintime = "";
	/**
	 * 截止时间。单位：毫秒数
	 */
	public String Endtime = "";
	/**
	 * 添加时间。单位：毫秒数
	 */
	public String Addtime = "";
    public String city="";
    public String province="";
}
