package cn.ctibet.tibetriding.bean;

/**
 * 行程表
 * 
 * @author liuzhao
 * @since 2015-2-5
 */
public class DbRouteBean {
	/**
	 * 行程ID
	 */
	public int Routeid;
	/**
	 * 线路ID
	 */
	public int Wayid;
	/**
	 * 起点所属省
	 */
	public String Beginprovince = "";
	/**
	 * 起点所在市
	 */
	public String Begincity = "";
	/**
	 * 起点所在县（区）
	 */
	public String Beginroutename = "";
	/**
	 * 起点经度
	 */
	public String Beginlng = "";
	/**
	 * 起点纬度
	 */
	public String Beginlat = "";
	/**
	 * 当前行程位于第几天
	 */
	public String Daynum = "";
	/**
	 * 上一个行程的ID
	 */
	public int Parentid;
	/**
	 * 添加时间
	 */
	public String Addtime = "";
	/**
	 * 终点经度
	 */
	public String Endlng = "";
	/**
	 * 终点纬度
	 */
	public String Endlat = "";
	/**
	 * 行程轨迹数据，结构如“经度1,纬度1|经度2,纬度2|...”
	 */
	public String Trackset = "";
	/**
	 * 终点所在省
	 */
	public String Endprovince = "";
	/**
	 * 终点所在市
	 */
	public String Endcity = "";
	/**
	 * 终点所在县（区）
	 */
	public String Endroutename = "";

	@Override
	public String toString() {
		return "DbRouteBean [Routeid=" + Routeid + ", Wayid=" + Wayid
				+ ", Beginprovince=" + Beginprovince + ", Begincity="
				+ Begincity + ", Beginroutename=" + Beginroutename
				+ ", Beginlng=" + Beginlng + ", Beginlat=" + Beginlat
				+ ", Daynum=" + Daynum + ", Parentid=" + Parentid
				+ ", Addtime=" + Addtime + ", Endlng=" + Endlng + ", Endlat="
				+ Endlat + ", Trackset=" + Trackset + ", Endprovince="
				+ Endprovince + ", Endcity=" + Endcity + ", Endroutename="
				+ Endroutename + "]";
	}

}
