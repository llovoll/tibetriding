package cn.ctibet.tibetriding.bean;

/**
 * 骑行类型：日常锻炼和线路骑行
 * 
 * @author liuzhao
 * @since 2015-1-29
 */
public enum EnumRidingType {
	DAILY_EXERCISING(1), LINE_RIDING(2);

	private int Value;

	EnumRidingType(int value) {
		Value = value;
	}

	public int getValue() {
		return Value;
	}
}
