package cn.ctibet.tibetriding.bean;

/**
 * 直播内容
 * 
 * @author liuzhao
 * @since 2015-2-2
 */
public class LiveBean {
	/**
	 * 直播内容ID
	 */
	private int Liveid;
	/**
	 * 历史轨迹ID
	 */
	private int Trackhistoryid;
	/**
	 * 直播位置纬度
	 */
	private String Livelat = "";
	/**
	 * 直播位置经度
	 */
	private String Livelng = "";

	public LiveBean() {

	}

	/**
	 * 构造直播内容
	 * 
	 * @param liveid
	 *            直播内容ID
	 * @param trackhistoryid
	 *            历史轨迹ID
	 * @param livelat
	 *            直播位置纬度
	 * @param livelng
	 *            直播位置经度
	 */
	public LiveBean(int liveid, int trackhistoryid, String livelat,
			String livelng) {
		this();
		Liveid = liveid;
		Trackhistoryid = trackhistoryid;
		Livelat = livelat;
		Livelng = livelng;
	}

	/**
	 * 获取直播内容ID
	 * 
	 * @return 直播内容ID
	 */
	public int getLiveid() {
		return Liveid;
	}

	/**
	 * 设置直播内容ID
	 * 
	 * @param liveid
	 *            直播内容ID
	 */
	public void setLiveid(int liveid) {
		Liveid = liveid;
	}

	/**
	 * 获取历史轨迹ID
	 * 
	 * @return 历史轨迹ID
	 */
	public int getTrackhistoryid() {
		return Trackhistoryid;
	}

	/**
	 * 设置历史轨迹ID
	 * 
	 * @param trackhistoryid
	 *            历史轨迹ID
	 */
	public void setTrackhistoryid(int trackhistoryid) {
		Trackhistoryid = trackhistoryid;
	}

	/**
	 * 获取直播位置纬度
	 * 
	 * @return 直播位置纬度
	 */
	public String getLivelat() {
		return Livelat;
	}

	/**
	 * 设置直播位置纬度
	 * 
	 * @param livelat
	 *            直播位置纬度
	 */
	public void setLivelat(String livelat) {
		Livelat = livelat;
	}

	/**
	 * 获取直播位置经度
	 * 
	 * @return 直播位置经度
	 */
	public String getLivelng() {
		return Livelng;
	}

	/**
	 * 获取直播位置经度
	 * 
	 * @param livelng
	 *            直播位置经度
	 */
	public void setLivelng(String livelng) {
		Livelng = livelng;
	}

	@Override
	public String toString() {
		return "LiveBean [Liveid=" + Liveid + ", Trackhistoryid="
				+ Trackhistoryid + ", Livelat=" + Livelat + ", Livelng="
				+ Livelng + "]";
	}
}
