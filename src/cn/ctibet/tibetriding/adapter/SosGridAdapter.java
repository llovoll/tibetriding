package cn.ctibet.tibetriding.adapter;

import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import cn.ctibet.tibetriding.R;
import cn.ctibet.tibetriding.activity.MaxImgActivity;
import cn.ctibet.tibetriding.bean.Configs;
import cn.ctibet.tibetriding.util.SysPrintUtil;
import net.tsz.afinal.FinalBitmap;

import java.util.List;

/**
 * Created by Administrator on 2015/1/22.
 */
public class SosGridAdapter extends BaseAdapter {
    private LayoutInflater inflater;
    private Context context;
    private List<String> list = null;
    private int srceenWidth;
    private String id;
    private int mPosition = 0;
    private FinalBitmap fb;

    public SosGridAdapter(Context context, List<String> list,
                          int SrceenWidth) {
        this.context = context;
        this.list = list;
        this.srceenWidth = SrceenWidth;
        inflater = (LayoutInflater) context
                .getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        fb = FinalBitmap.create(context);
        fb.configLoadingImage(R.drawable.default_local);
        fb.configLoadfailImage(R.drawable.default_local);

    }

    public int getPosition() {
        return mPosition;
    }

    public int getCount() {
        return list.size();
    }

    public Object getItem(int position) {
        return list.get(position);
    }

    public long getItemId(int position) {
        return position;
    }

    public View getView(final int position, View convertView, ViewGroup parent) {
        convertView = inflater.inflate(R.layout.grid_img_item, null);
        ImageView img = (ImageView) convertView
                .findViewById(R.id.grid_img_item_img);
        RelativeLayout.LayoutParams linear = (RelativeLayout.LayoutParams) img
                .getLayoutParams();
        linear.width = (srceenWidth - 270) / 3;
        linear.height = linear.width;
        img.setLayoutParams(linear);

        if (null != list && list.size() > 0) {
            SysPrintUtil.pt("sos列表图片地址", Configs.HOST_IMG + list.get(position));
            fb.display(img, Configs.HOST_IMG + list.get(position));
            img.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    Intent intent = new Intent(context, MaxImgActivity.class);
                    intent.putExtra("url", Configs.HOST_IMG + list.get(position));
                    context.startActivity(intent);
                }
            });
        }

        return convertView;
    }
}
