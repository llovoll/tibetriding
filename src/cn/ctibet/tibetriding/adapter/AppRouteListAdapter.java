package cn.ctibet.tibetriding.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.*;
import cn.ctibet.tibetriding.R;
import cn.ctibet.tibetriding.bean.ScreenSize;
import cn.ctibet.tibetriding.bean.SosDataBean;
import cn.ctibet.tibetriding.util.ScreenSizeUtil;
import cn.ctibet.tibetriding.util.TimeUtil;
import net.tsz.afinal.FinalBitmap;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Administrator on 2015/3/9.
 */
public class AppRouteListAdapter extends BaseAdapter {

    private LayoutInflater inflater;
    private Context context;
    private List<SosDataBean> list = new ArrayList<SosDataBean>();
    private FinalBitmap fb;
    private ScreenSize screenSize;

    public AppRouteListAdapter(Context context) {
        this.context = context;
        inflater = (LayoutInflater) context
                .getSystemService(Context.LAYOUT_INFLATER_SERVICE);
    }

    public void addList(List<SosDataBean> list, boolean isLoad) {
        if (isLoad) {
            for (int i = 0; i < list.size(); i++) {
                this.list.add(list.get(i));
            }
        } else {
            this.list = list;
        }
    }

    public List<SosDataBean> getList() {
        return list;
    }

    public int getCount() {
        return list.size();
    }

    public Object getItem(int position) {
        return list.get(position);
    }

    public long getItemId(int position) {
        return position;
    }

    public View getView(final int position, View convertView, ViewGroup parent) {
        ViewHolder viewHolder = null;
        if (null == viewHolder) {
            convertView = inflater.inflate(R.layout.app_route_list_item, null);
            viewHolder = new ViewHolder();
            viewHolder.name = (TextView) convertView.findViewById(R.id.app_route_list_item_name);
            convertView.setTag(viewHolder);
        } else {
            viewHolder = (ViewHolder) convertView.getTag();
        }
        if (null != list && list.size() != 0) {
            SosDataBean bean = list.get(position);
            viewHolder.name.setText(bean.nickname);
        }
        return convertView;
    }

    class ViewHolder {
        public TextView name;
    }
}
