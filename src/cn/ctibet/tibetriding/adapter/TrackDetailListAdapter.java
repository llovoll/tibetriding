package cn.ctibet.tibetriding.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import cn.ctibet.tibetriding.R;
import cn.ctibet.tibetriding.bean.CyclingImgBean;
import cn.ctibet.tibetriding.bean.ScreenSize;
import cn.ctibet.tibetriding.util.ScreenSizeUtil;
import net.tsz.afinal.FinalBitmap;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Administrator on 2015/1/29.
 */
public class TrackDetailListAdapter extends BaseAdapter {

    private LayoutInflater inflater;
    private Context context;
    private List<CyclingImgBean> list = new ArrayList<CyclingImgBean>();
    private FinalBitmap fb;
    private ScreenSize screenSize;

    public TrackDetailListAdapter(Context context, List<CyclingImgBean> list) {
        this.context = context;
        this.list = list;
        screenSize = ScreenSizeUtil.getScreenSize(context);
        fb = FinalBitmap.create(context);
        fb.configLoadingImage(R.drawable.default_local);
        fb.configLoadfailImage(R.drawable.default_local);
        inflater = (LayoutInflater) context
                .getSystemService(Context.LAYOUT_INFLATER_SERVICE);
    }

    public void addList(List<CyclingImgBean> list) {
        this.list = list;
    }

    public List<CyclingImgBean> getList() {
        return list;
    }

    public int getCount() {
        return list.size();
    }

    public Object getItem(int position) {
        return list.get(position);
    }

    public long getItemId(int position) {
        return position;
    }

    public View getView(final int position, View convertView, ViewGroup parent) {
        ViewHolder viewHolder = null;
        if (null == viewHolder) {
            convertView = inflater.inflate(R.layout.track_detail_list_item, null);
            viewHolder = new ViewHolder();
            viewHolder.title = (TextView) convertView.findViewById(R.id.track_detail_list_item_title);
            // viewHolder.img = (ImageView) convertView.findViewById(R.id.sos_fragment_listitem_img);
            viewHolder.dot = (ImageView) convertView.findViewById(R.id.track_detail_list_item_dot);
            viewHolder.content = (TextView) convertView.findViewById(R.id.track_detail_list_item_content);
            convertView.setTag(viewHolder);
        } else {
            viewHolder = (ViewHolder) convertView.getTag();
        }
        if (null != list && list.size() > 0) {
            String str = list.get(position).type;
            if (position >= 1) {
                String str1 = list.get(position - 1).type;
                if (str.equals(str1)) {
                    viewHolder.title.setVisibility(View.GONE);
                    viewHolder.dot.setVisibility(View.GONE);
                } else {
                    viewHolder.title.setVisibility(View.VISIBLE);
                    viewHolder.dot.setVisibility(View.VISIBLE);
                    viewHolder.title.setText("成都" + list.get(position).type);
                }
            } else {
                viewHolder.title.setVisibility(View.VISIBLE);
                viewHolder.dot.setVisibility(View.VISIBLE);
                viewHolder.title.setText("成都" + list.get(position).type);
            }

            viewHolder.content.setText(list.get(position).location);

        }
        return convertView;
    }

    class ViewHolder {
        public TextView title;
        public TextView content;
        public ImageView dot;
    }
}
