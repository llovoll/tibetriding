package cn.ctibet.tibetriding.adapter;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.net.Uri;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import cn.ctibet.tibetriding.R;
import cn.ctibet.tibetriding.bean.BitmapBean;
import cn.ctibet.tibetriding.bean.Configs;
import cn.ctibet.tibetriding.util.RotateBitmapUtil;
import cn.ctibet.tibetriding.util.SysPrintUtil;
import net.tsz.afinal.FinalBitmap;

import java.io.File;
import java.io.IOException;
import java.util.List;

/**
 * Created by Administrator on 2014/9/19.
 */
public class CyclingGridAdapter extends BaseAdapter {
    private LayoutInflater inflater;
    private Context context;
    private List<BitmapBean> list = null;
    private int srceenWidth;
    private String id;
    private int mPosition = 0;
    private FinalBitmap fb;

    public CyclingGridAdapter(Context context, List<BitmapBean> list,
                              int SrceenWidth) {
        this.context = context;
        this.list = list;
        this.srceenWidth = SrceenWidth;
        inflater = (LayoutInflater) context
                .getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        fb = FinalBitmap.create(context);
        fb.configLoadingImage(R.drawable.default_local);
        fb.configLoadfailImage(R.drawable.default_local);

    }

    public void addList(List<BitmapBean> list) {
        this.list = list;
    }

    public List<BitmapBean> getAllList() {
        return list;
    }

    public int getPosition() {
        return mPosition;
    }

    public int getCount() {
        return list.size();
    }

    public Object getItem(int position) {
        return list.get(position);
    }

    public long getItemId(int position) {
        return position;
    }

    public View getView(final int position, View convertView, ViewGroup parent) {
        convertView = inflater.inflate(R.layout.grid_img_item, null);
        ImageView img = (ImageView) convertView
                .findViewById(R.id.grid_img_item_img);
        ImageView checkImg = (ImageView) convertView
                .findViewById(R.id.grid_img_item_img2);
        RelativeLayout rela = (RelativeLayout) convertView
                .findViewById(R.id.grid_img_item_rela);
        RelativeLayout.LayoutParams linear = (RelativeLayout.LayoutParams) rela
                .getLayoutParams();
        linear.width = (srceenWidth - 24) / 3;
        linear.height = linear.width;
        rela.setLayoutParams(linear);

        if (null != list && list.size() > 0) {
            if (list.get(position).isCheck) {
                checkImg.setVisibility(View.VISIBLE);
            } else {
                checkImg.setVisibility(View.GONE);
            }
            SysPrintUtil.pt("骑行相册名称", list.get(position).imgPath);
            //File paths = new File(Configs.APP_PATH + "galley/");
            String imgPath = Configs.APP_PATH + "galley/" + list.get(position).imgPath;
           /* File file = new File(imgPath);
            if (!paths.exists()) {
                paths.mkdirs();
            }
            if (!file.exists()) {
                try {
                    file.createNewFile();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }*/
            Bitmap bitmap = RotateBitmapUtil.rotateMap(imgPath);
            img.setImageBitmap(bitmap);
           /* Uri cameraUri = Uri.fromFile(file);
            fb.display(img, cameraUri + "");*/
        }

        return convertView;
    }

}
