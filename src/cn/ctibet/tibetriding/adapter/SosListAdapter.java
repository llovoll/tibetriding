package cn.ctibet.tibetriding.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.*;
import cn.ctibet.tibetriding.R;
import cn.ctibet.tibetriding.bean.CyclingImgBean;
import cn.ctibet.tibetriding.bean.ScreenSize;
import cn.ctibet.tibetriding.bean.SosDataBean;
import cn.ctibet.tibetriding.util.ScreenSizeUtil;
import cn.ctibet.tibetriding.util.TimeUtil;
import net.tsz.afinal.FinalBitmap;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Administrator on 2015/1/22.
 */
public class SosListAdapter extends BaseAdapter {

    private LayoutInflater inflater;
    private Context context;
    private List<SosDataBean> list = new ArrayList<SosDataBean>();
    private FinalBitmap fb;
    private ScreenSize screenSize;
    private double geoLat;
    private double geoLng;

    public SosListAdapter(Context context) {
        this.context = context;
        screenSize = ScreenSizeUtil.getScreenSize(context);
        fb = FinalBitmap.create(context);
        fb.configLoadingImage(R.drawable.default_local);
        fb.configLoadfailImage(R.drawable.default_local);
        inflater = (LayoutInflater) context
                .getSystemService(Context.LAYOUT_INFLATER_SERVICE);
    }

    public void addList(List<SosDataBean> list, double geoLat, double geoLng, boolean isLoad) {
        this.geoLat = geoLat;
        this.geoLng = geoLng;
        if (isLoad) {
            for (int i = 0; i < list.size(); i++) {
                this.list.add(list.get(i));
            }
        } else {
            this.list = list;
        }
    }

    public List<SosDataBean> getList() {
        return list;
    }

    public int getCount() {
        return list.size();
    }

    public Object getItem(int position) {
        return list.get(position);
    }

    public long getItemId(int position) {
        return position;
    }

    public View getView(final int position, View convertView, ViewGroup parent) {
        ViewHolder viewHolder = null;
        if (null == viewHolder) {
            convertView = inflater.inflate(R.layout.sos_fragment_list_item, null);
            viewHolder = new ViewHolder();
            viewHolder.img = (ImageView) convertView.findViewById(R.id.sos_fragment_listitem_img);
            viewHolder.headImg = (ImageView) convertView.findViewById(R.id.sos_fragment_listitem_headImg);
            viewHolder.name = (TextView) convertView.findViewById(R.id.sos_fragment_listitem_name);
            viewHolder.num = (TextView) convertView.findViewById(R.id.sos_fragment_listitem_newsNum);
            viewHolder.time = (TextView) convertView.findViewById(R.id.sos_fragment_listitem_time);
            viewHolder.content = (TextView) convertView.findViewById(R.id.sos_fragment_listitem_content);
            viewHolder.newsNumRela = (RelativeLayout) convertView.findViewById(R.id.sos_fragment_listitem_rela);
            viewHolder.grid_img = (GridView) convertView.findViewById(R.id.sos_fragment_listitem_item_gridView);
            viewHolder.juliRela = (RelativeLayout) convertView.findViewById(R.id.sos_fragment_listitem_juli_rela);
            convertView.setTag(viewHolder);
        } else {
            viewHolder = (ViewHolder) convertView.getTag();
        }
        if (null != list && list.size() != 0) {
            SosDataBean bean = list.get(position);
            //fb.display(viewHolder.headImg,);
            viewHolder.time.setText(bean.addtime);
            viewHolder.content.setText(bean.soscontent);
            if (bean.sostype.equals("3")) {
                viewHolder.img.setImageResource(R.drawable.men);
                viewHolder.headImg.setVisibility(View.VISIBLE);
                viewHolder.newsNumRela.setVisibility(View.VISIBLE);
                viewHolder.juliRela.setVisibility(View.VISIBLE);
                viewHolder.name.setText(bean.nickname + "  求助");
                viewHolder.num.setText(bean.aplynum);
            } else if (bean.sostype.equals("1")) {
                viewHolder.img.setImageResource(R.drawable.notice);
                viewHolder.headImg.setVisibility(View.GONE);
                viewHolder.newsNumRela.setVisibility(View.GONE);
                viewHolder.juliRela.setVisibility(View.GONE);
                viewHolder.name.setText("政府通告");
            } else if (bean.sostype.equals("2")) {
                viewHolder.img.setImageResource(R.drawable.inform);
                viewHolder.headImg.setVisibility(View.GONE);
                viewHolder.newsNumRela.setVisibility(View.GONE);
                viewHolder.juliRela.setVisibility(View.GONE);
                viewHolder.name.setText("系统通知");
            }
            List<String> strList=null;
            if (!bean.sosimg.equals("")) {
                strList = new ArrayList<String>();
                if (bean.sosimg.contains(",")) {
                    String[] imgs = bean.sosimg.split(",");
                    for (int i = 0; i < imgs.length; i++) {
                        strList.add(imgs[i]);
                    }
                } else {
                    strList.add(bean.sosimg);
                }
            }
            if (null != strList && strList.size() > 0) {
                SosGridAdapter adapter = new SosGridAdapter(context, strList, screenSize.screenW);
                viewHolder.grid_img.setAdapter(adapter);
            }
        }
        return convertView;
    }

    class ViewHolder {
        public GridView grid_img;
        public ImageView headImg;
        public ImageView img;
        public RelativeLayout newsNumRela;
        public RelativeLayout juliRela;
        public TextView name;
        public TextView time;
        public TextView num;
        public TextView content;
    }
}

