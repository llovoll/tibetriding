package cn.ctibet.tibetriding.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.*;
import cn.ctibet.tibetriding.R;
import cn.ctibet.tibetriding.bean.CyclingImgBean;
import cn.ctibet.tibetriding.bean.RoadLineBean;
import cn.ctibet.tibetriding.bean.ScreenSize;
import cn.ctibet.tibetriding.impl.RoadLineListClickListener;
import cn.ctibet.tibetriding.util.ScreenSizeUtil;
import net.tsz.afinal.FinalBitmap;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Administrator on 2015/1/27.
 */
public class RoadLineListAdapter extends BaseAdapter {

    private LayoutInflater inflater;
    private Context context;
    private List<RoadLineBean> list = new ArrayList<RoadLineBean>();
    private FinalBitmap fb;
    private ScreenSize screenSize;

    public RoadLineListAdapter(Context context) {
        this.context = context;
        this.list = list;
        screenSize = ScreenSizeUtil.getScreenSize(context);
        fb = FinalBitmap.create(context);
        fb.configLoadingImage(R.drawable.default_local);
        fb.configLoadfailImage(R.drawable.default_local);
        inflater = (LayoutInflater) context
                .getSystemService(Context.LAYOUT_INFLATER_SERVICE);
    }

    public void addList(List<RoadLineBean> list) {
        this.list = list;
    }

    public List<RoadLineBean> getList() {
        return list;
    }

    public int getCount() {
        return list.size();
    }

    public Object getItem(int position) {
        return list.get(position);
    }

    public long getItemId(int position) {
        return position;
    }

    public View getView(final int position, View convertView, ViewGroup parent) {
        ViewHolder viewHolder = null;
        if (null == viewHolder) {
            convertView = inflater.inflate(R.layout.road_line_list_item, null);
            viewHolder = new ViewHolder();
            viewHolder.linear = (LinearLayout) convertView.findViewById(R.id.road_line_list_item_linear);
            viewHolder.name = (TextView) convertView.findViewById(R.id.road_line_list_item_name);
            viewHolder.title = (TextView) convertView.findViewById(R.id.road_line_list_item_title);
            viewHolder.img = (ImageView) convertView.findViewById(R.id.sos_fragment_listitem_img);
            viewHolder.linear.setTag(position);
            convertView.setTag(viewHolder);
        } else {
            viewHolder = (ViewHolder) convertView.getTag();
        }
        if (null != list && list.size() > 0) {
            String str = list.get(position).daynum;
            if (position >= 1) {
                String str1 = list.get(position - 1).daynum;
                if (str.equals(str1)) {
                    viewHolder.title.setVisibility(View.GONE);
                } else {
                    viewHolder.title.setVisibility(View.VISIBLE);
                    viewHolder.title.setText("DAY" + list.get(position).daynum);
                }
            } else {
                viewHolder.title.setVisibility(View.VISIBLE);
                viewHolder.title.setText("DAY" + list.get(position).daynum);
            }
            if (list.get(position).isClick) {
                viewHolder.img.setBackgroundResource(R.drawable.dot_orange);
                viewHolder.name.setTextColor(context.getResources().getColor(R.color.orange1));
            } else {
                viewHolder.img.setBackgroundResource(R.drawable.dot_gray);
                viewHolder.name.setTextColor(context.getResources().getColor(R.color.gray_l));
            }
            viewHolder.name.setText(list.get(position).beginroutename);
            viewHolder.linear.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    int pos = (Integer) view.getTag();
                    listener.getClickPos(pos);
                }
            });
        }
        return convertView;
    }

    class ViewHolder {
        public TextView name;
        public TextView title;
        public LinearLayout linear;
        public ImageView img;
    }

    private RoadLineListClickListener listener;

    public void setListClickPos(RoadLineListClickListener listener) {
        this.listener = listener;
    }
}
