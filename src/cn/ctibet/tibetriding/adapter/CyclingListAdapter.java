package cn.ctibet.tibetriding.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.*;
import cn.ctibet.tibetriding.R;
import cn.ctibet.tibetriding.activity.ImageChooseActivity;
import cn.ctibet.tibetriding.bean.*;
import cn.ctibet.tibetriding.db.AlbumDb;
import cn.ctibet.tibetriding.impl.DeleteClickListener;
import cn.ctibet.tibetriding.impl.GridClickListener;
import cn.ctibet.tibetriding.impl.ImgEditClickListener;
import cn.ctibet.tibetriding.util.*;
import net.tsz.afinal.FinalBitmap;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Administrator on 2015/1/19.
 */
public class CyclingListAdapter extends BaseAdapter implements ImgEditClickListener, DeleteClickListener {

    private LayoutInflater inflater;
    private Context context;
    private List<CyclingImgBean> list = new ArrayList<CyclingImgBean>();
    private FinalBitmap fb;
    private ScreenSize screenSize;
    private ImageChooseActivity imageChooseActivity;
    private boolean editStat = false;
    private CyclingGridAdapter adapter;
    private UserInfo userInfo;

    public CyclingListAdapter(Context context, List<CyclingImgBean> list, ImageChooseActivity imageChooseActivity) {
        this.context = context;
        this.list = list;
        this.imageChooseActivity = imageChooseActivity;
        screenSize = ScreenSizeUtil.getScreenSize(context);
        fb = FinalBitmap.create(context);
        fb.configLoadingImage(R.drawable.default_local);
        fb.configLoadfailImage(R.drawable.default_local);
        inflater = (LayoutInflater) context
                .getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        userInfo = UserInfoUtil.getUserInfo(context);
        imageChooseActivity.setImgClick(this);
        imageChooseActivity.setDelClick(this);
    }

    public List<CyclingImgBean> getList() {
        return list;
    }

    public int getCount() {
        return list.size();
    }

    public Object getItem(int position) {
        return list.get(position);
    }

    public long getItemId(int position) {
        return position;
    }

    public View getView(final int position, View convertView, ViewGroup parent) {
        ViewHolder viewHolder = null;
        if (null == viewHolder) {
            convertView = inflater.inflate(R.layout.cycling_item, null);
            viewHolder = new ViewHolder();
            viewHolder.location = (TextView) convertView.findViewById(R.id.cycling_item_location);
            viewHolder.haiba = (TextView) convertView.findViewById(R.id.cycling_item_haiba);
            viewHolder.wendu = (TextView) convertView.findViewById(R.id.cycling_item_wendu);
            viewHolder.time = (TextView) convertView.findViewById(R.id.cycling_item_time);
            viewHolder.grid_img = (GridView) convertView.findViewById(R.id.cycling_item_gridView);
            convertView.setTag(viewHolder);
        } else {
            viewHolder = (ViewHolder) convertView.getTag();
        }
        if (null != list && list.size() != 0) {
            final CyclingImgBean bean = list.get(position);
            viewHolder.location.setText(bean.location);
            viewHolder.haiba.setText(bean.haiba);
            viewHolder.wendu.setText(bean.wendu);
            viewHolder.time.setText(bean.time);
            SysPrintUtil.pt("相册Adapter", bean.bitmapList.size() + "");
            if (null != bean.bitmapList && bean.bitmapList.size() > 0) {
                adapter = new CyclingGridAdapter(context, bean.bitmapList, screenSize.screenW);
                viewHolder.grid_img.setAdapter(adapter);
                viewHolder.grid_img.setOnItemClickListener(new AdapterView.OnItemClickListener() {
                    @Override
                    public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {
                        if (editStat) {
                            List<BitmapBean> bitList = adapter.getAllList();
                            if (bitList.get(i).isCheck) {
                                bitList.get(i).isCheck = false;
                            } else {
                                bitList.get(i).isCheck = true;
                            }
                            adapter.addList(bitList);
                            adapter.notifyDataSetChanged();
                        } else {
                            gridClickListener.getPath(bean.bitmapList.get(i));
                        }
                    }
                });
            }
        }
        return convertView;
    }

    @Override
    public void getClickStat(boolean stat) {
        editStat = stat;
        if (!editStat) {
            List<BitmapBean> bitList = adapter.getAllList();
            for (int i = 0; i < bitList.size(); i++) {
                bitList.get(i).isCheck = false;
            }
            adapter.addList(bitList);
            adapter.notifyDataSetChanged();
        }
    }

    @Override
    public void delClick() {
        List<BitmapBean> bitList = adapter.getAllList();
        List<BitmapBean> checkList = new ArrayList<BitmapBean>();
        List<BitmapBean> noCheckList = new ArrayList<BitmapBean>();
        boolean tag = false;
        for (int i = 0; i < bitList.size(); i++) {
            if (bitList.get(i).isCheck) {
                checkList.add(bitList.get(i));
            } else {
                noCheckList.add(bitList.get(i));
            }
        }
        if (checkList.size() == 0) {
            ToastUtil.showToast(context, "请选择需要删除的图片", 0);
        } else {
            AlbumDb db = new AlbumDb(context);
            for (int i = 0; i < checkList.size(); i++) {
                db.delAlbumItemData(checkList.get(i).imgPath, userInfo.userId);
                FileUtils.deleteFile(Configs.APP_PATH + "galley/" + checkList.get(i).imgPath);
            }
            adapter.addList(noCheckList);
            adapter.notifyDataSetChanged();
            if (noCheckList.size() == 0) {
                list.clear();
                notifyDataSetChanged();
            }
        }
    }

    class ViewHolder {
        public TextView location;
        public TextView haiba;
        public TextView wendu;
        public TextView time;
        public GridView grid_img;
    }

    private GridClickListener gridClickListener;

    public void setPath(GridClickListener gridClickListener) {
        this.gridClickListener = gridClickListener;
    }
}
