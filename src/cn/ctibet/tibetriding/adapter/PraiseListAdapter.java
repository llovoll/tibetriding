package cn.ctibet.tibetriding.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;
import cn.ctibet.tibetriding.R;
import cn.ctibet.tibetriding.bean.CyclingImgBean;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Administrator on 2015/3/23.
 */
public class PraiseListAdapter extends BaseAdapter {

    private List<CyclingImgBean> list = new ArrayList<CyclingImgBean>();

    private Context context = null;

    private LayoutInflater inflater = null;

    public PraiseListAdapter(Context context) {
        this.context = context;
        inflater = LayoutInflater.from(context);
    }

    public void addList(List<CyclingImgBean> list) {
        this.list = list;
    }

    public List<CyclingImgBean> getAll() {
        return list;
    }

    @Override
    public int getCount() {
        return list.size();
    }

    @Override
    public Object getItem(int position) {
        return list.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup arg2) {
        Holder holder = null;
        if (null == holder) {
            holder = new Holder();
            convertView = inflater.inflate(R.layout.praise_list_item, null);
            holder.name = (TextView) convertView.findViewById(R.id.praise_list_item_name);
            convertView.setTag(holder);
        } else {
            holder = (Holder) convertView.getTag();
        }
        if (null != list && list.size() > 0) {
            holder.name.setText(list.get(position).imgname);
        }

        return convertView;
    }

    class Holder {
        public TextView name;
    }
}
