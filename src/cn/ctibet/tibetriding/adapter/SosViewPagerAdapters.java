package cn.ctibet.tibetriding.adapter;

import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;

import java.util.List;

/**
 * @author dingwenlong
 * @version V1.0
 * @Project: game
 * @Title:
 * @Package com.inwhoop.gameproduct.adapter
 * @Description: TODO
 * @date 2014/4/16   20:12
 * @Copyright: 2014 成都呐喊信息技术 All rights reserved.
 */
public class SosViewPagerAdapters extends FragmentPagerAdapter {

    private List<Fragment> fragmentsList;

    public SosViewPagerAdapters(FragmentManager fm) {
        super(fm);
    }

    public SosViewPagerAdapters(FragmentManager fm, List<Fragment> fragments) {
        super(fm);
        this.fragmentsList = fragments;
    }

    @Override
    public int getCount() {
        return fragmentsList.size();
    }

    @Override
    public Fragment getItem(int arg0) {
        return fragmentsList.get(arg0);
    }

    @Override
    public int getItemPosition(Object object) {
        return super.getItemPosition(object);
    }
}
