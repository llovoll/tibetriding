package cn.ctibet.tibetriding.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import cn.ctibet.tibetriding.R;
import cn.ctibet.tibetriding.bean.MenuBean;

import java.util.ArrayList;
import java.util.List;

/**
 * @author dingwenlong
 * @version V1.0
 * @Project: zhixin_2.0
 * @Title:
 * @Package com.inwhoop.zhixin.adapter
 * @Description: 主页面左侧栏listAdapter
 * @date 2014/4/8   10:24
 * @Copyright: 2014 成都呐喊信息技术 All rights reserved.
 */
public class MainMenuListAdapter extends BaseAdapter {
    private final LayoutInflater inflater;
    private Context context;
    private List<MenuBean> list=new ArrayList<MenuBean>();

    public MainMenuListAdapter(Context context) {
        this.context = context;
        inflater = (LayoutInflater) context
                .getSystemService(Context.LAYOUT_INFLATER_SERVICE);
    }

    public void addList(List<MenuBean> list) {
        this.list = list;
    }

    public List<MenuBean> getList() {
        return list;
    }

    @Override
    public int getCount() {
        return list.size();
    }

    @Override
    public Object getItem(int position) {
        return list.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        ViewHolder viewHolder;
        if (null == convertView) {
            convertView = inflater.inflate(R.layout.item_menu, null);
            viewHolder = new ViewHolder();
            viewHolder.menu_img = (ImageView) convertView.findViewById(R.id.menuitem_image);
            viewHolder.menu_text = (TextView) convertView.findViewById(R.id.menuitem_text);
            convertView.setTag(viewHolder);
        } else {
            viewHolder = (ViewHolder) convertView.getTag();
        }
        if (null != list && list.size() > 0) {
            MenuBean bean = list.get(position);
            viewHolder.menu_img.setBackgroundResource(bean.img);
            viewHolder.menu_text.setText(bean.name);
            viewHolder.menu_text.setTextColor(context.getResources().getColor(R.color.gray_h));
            if (bean.isCheck) {
                switch (position) {
                    case 0:
                        viewHolder.menu_img.setBackgroundResource(R.drawable.flag_selected);
                        viewHolder.menu_text.setTextColor(context.getResources().getColor(R.color.orange));
                        break;
                    case 1:
                        viewHolder.menu_img.setBackgroundResource(R.drawable.data_selected);
                        viewHolder.menu_text.setTextColor(context.getResources().getColor(R.color.orange));
                        break;
                    case 2:
                        viewHolder.menu_img.setBackgroundResource(R.drawable.history_selected);
                        viewHolder.menu_text.setTextColor(context.getResources().getColor(R.color.orange));
                        break;
                    case 3:
                        viewHolder.menu_img.setBackgroundResource(R.drawable.route_selected);
                        viewHolder.menu_text.setTextColor(context.getResources().getColor(R.color.orange));
                        break;
                    case 4:
                        viewHolder.menu_img.setBackgroundResource(R.drawable.news_selected);
                        viewHolder.menu_text.setTextColor(context.getResources().getColor(R.color.orange));
                        break;
                    case 5:
                        viewHolder.menu_img.setBackgroundResource(R.drawable.friend_selected);
                        viewHolder.menu_text.setTextColor(context.getResources().getColor(R.color.orange));
                        break;
                }
            }
        }

        return convertView;
    }

    class ViewHolder {
        public ImageView menu_img;
        public TextView menu_text;
    }
}