package cn.ctibet.tibetriding.impl;

import com.amap.api.location.AMapLocation;

/**
 * Created by Administrator on 2015/3/18.
 */
public interface LocationListener {
    void getLocationSuccess(AMapLocation aMapLocation);

    void getLocationFail(AMapLocation aMapLocation);
}
