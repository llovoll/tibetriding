package cn.ctibet.tibetriding.impl;

import cn.ctibet.tibetriding.bean.BitmapBean;

/**
 * Created by Administrator on 2015/2/4.
 */
public interface GridClickListener {
    void getPath(BitmapBean path);
}
