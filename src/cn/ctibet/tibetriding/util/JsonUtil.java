package cn.ctibet.tibetriding.util;

import android.text.TextUtils;
import cn.ctibet.tibetriding.bean.*;
import cn.ctibet.tibetriding.db.Advisetime;
import org.json.JSONArray;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Administrator on 2015/1/20.
 */
public class JsonUtil {
    public static DailyExeBean getdailyExeData(String jsonData) throws Exception {
        DailyExeBean bean = new DailyExeBean();
        JSONObject obj = new JSONObject(jsonData);
        bean.code = obj.getString("code");
        bean.msg = obj.getString("msg");
        JSONObject obj1 = new JSONObject(bean.msg);
        bean.trackhistoryid = obj1.getString("trackhistoryid");
        bean.dailytrackhistoryid = obj1.getString("dailytrackhistoryid");
        return bean;
    }

    public static DailyExeBean getReleaseLiveStat(String jsonData) throws Exception {
        DailyExeBean bean = new DailyExeBean();
        JSONObject obj = new JSONObject(jsonData);
        bean.code = obj.getString("code");
        bean.msg = obj.getString("msg");
        return bean;
    }

    /**
     * 获取精彩线路数据
     *
     * @param jsonData
     * @return
     * @throws Exception
     */
    public static List<WayBean> getRouteData(String jsonData) throws Exception {
        WayBean bean = new WayBean();
        List<WayBean> list = null;
        JSONObject obj = new JSONObject(jsonData);
        bean.code = obj.getString("code");
        bean.msg = obj.getString("msg");
        if (null != bean.msg && !bean.msg.equals("") && !bean.msg.equals("[]")) {
            JSONArray jsonArray = new JSONArray(bean.msg);
            list = new ArrayList<WayBean>();
            for (int i = 0; i < jsonArray.length(); i++) {
                bean = new WayBean();
                JSONObject temp = jsonArray.getJSONObject(i);
                bean.Wayid = temp.getInt("wayid");
                bean.Wayname = temp.getString("wayname");
                bean.Wayimg = temp.getString("wayimg");
                bean.Waydesc = temp.getString("waydesc");
                bean.Maxday = temp.getInt("maxday");
                bean.Minday = temp.getInt("minday");
                bean.Addtime = temp.getString("addtime");
                bean.Waytype = temp.getInt("waytype");
                bean.Isdelete = temp.getInt("isdelete");
                bean.advisetime = temp.getString("advisetime");
                SysPrintUtil.pt("解析的数据为", bean.advisetime);
                if (null != bean.advisetime && !TextUtils.isEmpty(bean.advisetime)
                        && !bean.advisetime.equals("[]")) {
                    JSONArray jsonArray1 = new JSONArray(bean.advisetime);
                    bean.advisetimeList = new ArrayList<AdvisetimeBean>();
                    AdvisetimeBean bean1 = null;
                    for (int j = 0; j < jsonArray1.length(); j++) {
                        bean1 = new AdvisetimeBean();
                        JSONObject temp1 = jsonArray1.getJSONObject(j);
                        bean1.Waytimeid = temp1.getInt("waytimeid");
                        bean1.Wayid = temp1.getInt("wayid");
                        bean1.Begintime = temp1.getString("begintime");
                        bean1.Endtime = temp1.getString("endtime");
                        bean1.Month = temp1.getInt("month");
                        bean1.Price = temp1.getInt("price");
                        bean1.Cutprice = temp1.getInt("cutprice");
                        bean.advisetimeList.add(bean1);
                    }
                }
                list.add(bean);
            }
        }
        return list;
    }

    public static SosBean getSosData(String jsonData) throws Exception {
        SosBean bean = new SosBean();
        SosDataBean bean1 = null;
        JSONObject obj = new JSONObject(jsonData);
        bean.code = obj.getString("code");
        bean.msg = obj.getString("msg");
        if (null != bean.msg && !bean.msg.equals("")
                && !bean.msg.equals("[]") && bean.msg.contains("[")) {
            JSONArray jsonArray = new JSONArray(bean.msg);
            bean.list = new ArrayList<SosDataBean>();
            for (int i = 0; i < jsonArray.length(); i++) {
                bean1 = new SosDataBean();
                JSONObject temp = jsonArray.getJSONObject(i);
                bean1.sosid = temp.getString("sosid");
                bean1.wayid = temp.getString("wayid");
                bean1.userid = temp.getString("userid");
                bean1.sostype = temp.getString("sostype");
                bean1.soscontent = temp.getString("soscontent");
                bean1.sosimg = temp.getString("sosimg");
                bean1.soslat = temp.getString("soslat");
                bean1.soslng = temp.getString("soslng");
                bean1.addtime = temp.getString("addtime");
                bean1.userimg = temp.getString("userimg");
                bean1.countComment = temp.getString("countComment");
                bean.list.add(bean1);
            }
        }
        return bean;
    }

    public static List<RoadLineBean> getRoadLineData(String jsonData) throws Exception {
        SosBean bean = new SosBean();
        RoadLineBean bean1 = null;
        List<RoadLineBean> list = null;
        JSONObject obj = new JSONObject(jsonData);
        bean.code = obj.getString("code");
        bean.msg = obj.getString("msg");
        if (null != bean.msg && !bean.msg.equals("")
                && !bean.msg.equals("[]") && bean.msg.contains("[")) {
            JSONArray jsonArray = new JSONArray(bean.msg);
            list = new ArrayList<RoadLineBean>();
            for (int i = 0; i < jsonArray.length(); i++) {
                bean1 = new RoadLineBean();
                JSONObject temp = jsonArray.getJSONObject(i);
                if (i==0){
                    bean1.isClick=true;
                }else {
                    bean1.isClick=false;
                }
                bean1.routeid = temp.getString("routeid");
                bean1.beginroutename = temp.getString("beginroutename");
                bean1.daynum = temp.getString("daynum");
                list.add(bean1);
            }
        }
        return list;
    }

    public static RouteInfoBean getRouteInfoData(String jsonData) throws Exception {
        RouteInfoBean bean = new RouteInfoBean();
        JSONObject obj = new JSONObject(jsonData);
        bean.code = obj.getString("code");
        bean.msg = obj.getString("msg");
        JSONObject obj1 = new JSONObject(bean.msg);
        bean.routeid = obj1.getString("routeid");
        bean.wayid = obj1.getString("wayid");
        bean.beginprovince = obj1.getString("beginprovince");
        bean.begincity = obj1.getString("begincity");
        bean.beginroutename = obj1.getString("beginroutename");
        bean.beginlng = obj1.getString("beginlng");
        bean.beginlat = obj1.getString("beginlat");
        bean.daynum = obj1.getString("daynum");
        bean.parentid = obj1.getString("parentid");
        bean.addtime = obj1.getString("addtime");
        bean.endlng = obj1.getString("endlng");
        bean.endlat = obj1.getString("endlat");
        bean.trackset = obj1.getString("trackset");
        bean.endprovince = obj1.getString("endprovince");
        bean.endcity = obj1.getString("endcity");
        bean.endroutename = obj1.getString("endroutename");
        return bean;
    }

    public static List<CommentBean> getSosCommentData(String jsonData) throws Exception {
        SosBean bean = new SosBean();
        CommentBean bean1 = null;
        List<CommentBean> list = null;
        JSONObject obj = new JSONObject(jsonData);
        bean.code = obj.getString("code");
        bean.msg = obj.getString("msg");
        if (null != bean.msg && !bean.msg.equals("")
                && !bean.msg.equals("[]") && bean.msg.contains("[")) {
            JSONArray jsonArray = new JSONArray(bean.msg);
            list = new ArrayList<CommentBean>();
            for (int i = 0; i < jsonArray.length(); i++) {
                bean1 = new CommentBean();
                JSONObject temp = jsonArray.getJSONObject(i);
                bean1.sosid = temp.getString("sosid");
                bean1.soscommentid = temp.getString("soscommentid");
                bean1.userid = temp.getString("userid");
                bean1.soscomment = temp.getString("soscomment");
                bean1.addtime = temp.getString("addtime");
                bean1.username = temp.getString("username");
                list.add(bean1);
            }
        }
        return list;
    }
}
