package cn.ctibet.tibetriding.util;

import android.content.Context;
import android.content.SharedPreferences;
import cn.ctibet.tibetriding.bean.Configs;
import cn.ctibet.tibetriding.bean.UserInfo;

/**
 * @version V1.0
 * @Title:
 * @Package com.inwhoop.zhixin.util
 * @Description: TODO
 * @date 2014/4/11 17:50
 * @Copyright: 2014 成都呐喊信息技术 All rights reserved.
 */
public class UserInfoUtil {

    public static void rememberUserInfo(final Context context,
                                        final UserInfo userInfo) {
        new Thread(new Runnable() {
            @Override
            public void run() {
                rememberUser(context, userInfo);
            }
        }).start();
    }

    private static void rememberUser(Context context, UserInfo userInfo) {
        SharedPreferences setting = context.getSharedPreferences(
                Configs.SETTING_INFO, 0);
        setting.edit().putString(Configs.USER_ID, userInfo.userId)
                .putString(Configs.NAME, userInfo.name)
                .putString(Configs.PASSWORD, userInfo.pwd)
                .commit();
    }

    public static UserInfo getUserInfo(Context context) {
        UserInfo userInfo = new UserInfo();
        SharedPreferences setting = context.getSharedPreferences(
                Configs.SETTING_INFO, 0);
        userInfo.userId = setting.getString(Configs.USER_ID, "1");
        userInfo.name = setting.getString(Configs.NAME, "");
        userInfo.pwd = setting.getString(Configs.PASSWORD, "");
        return userInfo;
    }

    public static void setMenuPos(Context context, int pos) {
        SharedPreferences setting = context.getSharedPreferences(
                Configs.SETTING_INFO, 0);
        setting.edit().putInt(Configs.POS, pos)
                .commit();
    }

    public static int getMenuPos(Context context) {
        SharedPreferences setting = context.getSharedPreferences(
                Configs.SETTING_INFO, 0);
        return setting.getInt(Configs.POS, 0);
    }

    public static void clear(Context context) {
        SharedPreferences sp = context.getSharedPreferences(
                Configs.SETTING_INFO, Context.MODE_PRIVATE);
        sp.edit().clear().commit();
    }
}
