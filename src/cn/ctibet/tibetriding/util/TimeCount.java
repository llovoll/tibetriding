package cn.ctibet.tibetriding.util;

import android.os.CountDownTimer;
import android.text.Html;
import android.widget.TextView;

/**
 * 自定义计时类
 * 
 * @Project: Dream
 * @Title: TimeCount.java
 * @Package com.inwhoop.dream.utils
 * @Description: TODO
 * 
 * @author ligang@inwhoop.com
 * @date 2014-9-1 下午3:42:17
 * @Copyright: 2014 成都呐喊信息 All rights reserved.
 * @version V1.0
 */
public class TimeCount extends CountDownTimer {

	private TextView checking;

	public TimeCount(long millisInFuture, long countDownInterval,
					 TextView checking) {
		super(millisInFuture, countDownInterval);
		this.checking = checking;
	}

	public TimeCount(long millisInFuture, long countDownInterval) {
		super(millisInFuture, countDownInterval);// 参数依次为总时长,和计时的时间间隔
	}

	@Override
	public void onTick(long millisUntilFinished) {
		checking.setClickable(false);
		String str = "<font color='blue'>" + millisUntilFinished / 1000
				+ "</font>" + "<font color= 'white'>s后重新获取</font>";
		checking.setText(Html.fromHtml(str));
	}

	@Override
	public void onFinish() {
		checking.setText("重新验证");
		checking.setClickable(true);
	}

}
