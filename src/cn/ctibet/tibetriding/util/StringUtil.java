package cn.ctibet.tibetriding.util;

import java.util.ArrayList;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import android.text.TextUtils;
import cn.ctibet.tibetriding.bean.Configs;

import com.amap.api.maps.AMapUtils;
import com.amap.api.maps.model.LatLng;

/**
 * 字符串处理
 * 
 * @author liuzhao
 * @since 2015-2-5
 */
public class StringUtil {

	/**
	 * 获取html内容中的所有图片地址(即：src的值)
	 * 
	 * @param htmlContent
	 *            html串
	 * @return 地址列表
	 */
	public static List<String> parseImgSrcs(String htmlContent) {
		List<String> result = new ArrayList<String>();

		if (!TextUtils.isEmpty(htmlContent)) {
			Pattern imgPattern = Pattern.compile("<(img[^>]*)>");
			Pattern srcPattern = Pattern.compile("src=\"([^\"]+)\"");

			Matcher imgMatcher = imgPattern.matcher(htmlContent);
			while (imgMatcher.find()) {
				// 若找到<img>标签，则执行
				Matcher srcMatcher = srcPattern.matcher(imgMatcher.group());
				if (srcMatcher.find()) {
					String url = srcMatcher.group();
					if (!TextUtils.isEmpty(url)) {
						result.add(url.substring(4).replace("\"", ""));
					}
				}
			}
		}

		return result;
	}

	/**
	 * 根据上次位置和当前位置进行判断是否属于位置移动
	 * 
	 * @param pre
	 *            上次位置
	 * @param cur
	 *            当前位置
	 * @return true为已经移动，否则为静止。
	 */
	public static boolean isTravelling(LatLng pre, LatLng cur) {
		float distance = AMapUtils.calculateLineDistance(pre, cur);
		return distance > Configs.UPDATE_LOCATION_THRESHOLD;
	}
}
