package cn.ctibet.tibetriding.util;

import java.io.File;

import android.content.Context;
import android.os.Environment;

/**
 * Sdcard私有（应用卸载后会被删除）文件操作类。注：此类操作的文件全部位于“/sdcard/Android/data/应用报名/files/”目录下。
 * 
 * @author liuzhao
 * @since 2015-2-4
 */
public class FileUtils {
	/**
	 * Sdcard是否可用
	 *
	 * @return true为可用, 反之不可用
	 */
	public static boolean isAvaiable() {
		return Environment.MEDIA_MOUNTED.equals(Environment
				.getExternalStorageState());
	}

	/**
	 * 在Sdcard上创建目录，此目录位于"/sdcard/Android/data/应用报名/files/"下。
	 *
	 * @param context
	 *            上下文
	 * @param dir
	 *            目录名，""或null表示不创建子目录
	 * @return
	 */
	public static File createDirectory(Context context, String dir) {
		File file = null;

		if (isAvaiable()) {
			if (null != dir && !"".equals(dir)) {
				dir = dir.startsWith("/") ? dir : String.format("/%s", dir);
			}

			if (null == dir || "".equals(dir)) {
				dir = null;
			}

			file = context.getExternalFilesDir(dir);
		}

		return file;
	}

	/**
	 * 获取文件的File
	 * 
	 * @param context
	 *            上下文
	 * @param filePath
	 *            文件路径，如"dir/demo.txt"
	 * @return File
	 */
	public static File getFile(Context context, String filePath) {
		String type = null;
		String filename = null;

		if (null != filePath) {
			filePath = filePath.startsWith("/") ? filePath : String.format(
					"/%s", filePath);

			if (filePath.contains(".")) {
				type = filePath.substring(0, filePath.lastIndexOf("/"));
				filename = filePath.substring(filePath.lastIndexOf("/") + 1);
			} else {
				type = filePath;
			}

			if ("".equals(type)) {
				type = null;
			}
		}

		File file = context.getExternalFilesDir(type);

		if (null != filename) {
			file = new File(file, filename);
		}
		return file;
	}

	/**
	 * 获取文件的存储路径
	 * 
	 * @param context
	 *            上下文
	 * @param filePath
	 *            文件路径，如“dir/demo.txt”
	 * @return 存储路径
	 */
	public static String getAbsolutePath(Context context, String filePath) {
		File file = getFile(context, filePath);

		String result = null;
		if (null != file) {
			result = file.getAbsolutePath();
		}
		return result;
	}

	/**
	 * 删除文件。若是目录，需要先删除目录下的所有子文件；若是文件，则直接删除。
	 * 
	 * @param context
	 *            上下文
	 * @param filePath
	 *            文件路径,如“dir/demo.txt”或“dir”
	 */
	public static void delete(Context context, String filePath) {
		File file = getFile(context, filePath);

		if (null != file && file.exists()) {
			if (file.isDirectory()) {
				// 若是目录
				deleteDir(file);
			} else {
				file.delete();
			}
		}
	}

	/**
	 * 删除目录
	 * 
	 * @param dir
	 *            目录
	 */
	private static void deleteDir(File dir) {
		if (null != dir && dir.exists()) {
			for (File file : dir.listFiles()) {
				if (file.isDirectory()) {
					deleteDir(file);
				} else {
					file.delete();
				}
			}
		}
	}

	public static void deleteFile(String path) {
		File file = new File(path);
		if (file.exists()) { // 判断文件是否存在
			if (file.isFile()) { // 判断是否是文件
				file.delete(); // delete()方法 你应该知道 是删除的意思;
			}
		}
	}

}
