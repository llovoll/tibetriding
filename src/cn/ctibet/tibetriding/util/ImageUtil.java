package cn.ctibet.tibetriding.util;

import android.graphics.Bitmap;
import android.graphics.Bitmap.CompressFormat;
import android.graphics.BitmapFactory;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.io.InputStream;
import java.net.HttpURLConnection;
import java.net.URL;

public class ImageUtil {
	/**
	 * 读取输入流
	 * 
	 * @Title: readStream
	 * @Description: 通过输入流返回读取的字节数组
	 * @param is
	 *            输入流
	 * @return byte[] 读取的字节数组
	 */
	private static byte[] readStream(InputStream is) {
		ByteArrayOutputStream bos = new ByteArrayOutputStream();
		byte[] buff = new byte[1024];
		int len = -1;
		try {
			while ((len = is.read(buff)) != -1) {
				bos.write(buff, 0, len);

			}
			bos.close();
			is.close();
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return bos.toByteArray();
	}

	/**
	 * 获取网络图片字节数组
	 * 
	 * @Title: getImage
	 * @Description: 获取网络图片字节数组
	 * @param path
	 *            图片路径
	 * @return byte[] 读取的图片数组
	 * @throws
	 */

	private static byte[] getImage(String path) {
		URL url;
		HttpURLConnection conn = null;
		try {
			url = new URL(path);
			conn = (HttpURLConnection) url.openConnection();
			conn.setRequestMethod("GET");
			conn.setConnectTimeout(5 * 1000);
			conn.setReadTimeout(5 * 1000);
			if (conn.getResponseCode() == HttpURLConnection.HTTP_OK) {
				InputStream is = conn.getInputStream();
				return readStream(is);
			}

		} catch (Exception e) {
		}
		return null;
	}

	/**
	 * 获取网络图片
	 * 
	 * @Title: getHttpImage
	 * @Description: 获取网络图片
	 * @param @param path 路径
	 * @param @return
	 * @return Bitmap 返回图片
	 * @throws
	 */
	public static Bitmap getHttpImage(String path) {
		byte[] data = getImage(path);
		if (data != null) {
			Bitmap bitmap = BitmapFactory.decodeByteArray(data, 0, data.length);
			// System.out.println("网络图片== " + bitmap);
			return bitmap;
		}
		return null;
	}

	/**
	 * 保存图片到SD卡指定路径
	 * 
	 * @Title: saveBitmapToFile
	 * @Description: TODO
	 * @param @param bitmap
	 * @param @param _file 保存路径
	 * @param @param name 图片名
	 * @param @throws Exception
	 * @return void
	 */
	public static String saveBitmapToFile(Bitmap bitmap, String _file,
			String name) throws Exception {
		File file = new File(_file);
		if (!file.exists()) {
			file.mkdir();
		}
		File imageFile = new File(file + "/", name);
		File directory = imageFile.getParentFile();
		if (!directory.exists() && !directory.mkdirs()) {
			return "";
		}
		imageFile.createNewFile();
		FileOutputStream fos = new FileOutputStream(imageFile);
		boolean falg = bitmap.compress(CompressFormat.JPEG, 50, fos);
		System.out.println("========falg==============="+falg+"===="+name);
		fos.flush();
		fos.close();
		return file.getAbsolutePath();
	}

	/**
	 * 递归删除文件和文件夹
	 * 
	 * @param file
	 *            要删除的根目录
	 */
	public static void RecursionDeleteFile(File file) {
		if (file.isFile()) {
			file.delete();
			return;
		}
		if (file.isDirectory()) {
			File[] childFile = file.listFiles();
			if (null == childFile || childFile.length == 0) {
				file.delete();
				return;
			}
			for (File f : childFile) {
				RecursionDeleteFile(f);
			}
			file.delete();
		}
	}

}
