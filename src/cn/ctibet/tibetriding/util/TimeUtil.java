package cn.ctibet.tibetriding.util;


import java.io.UnsupportedEncodingException;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

/**
 * Created by Administrator on 2015/2/2.
 */
public class TimeUtil {
    /**
     * 获取当前时间的时间戳
     *
     * @return string
     */
    public static String getTime() {
        Long tsLong = System.currentTimeMillis() / 1000;
        return tsLong.toString();
    }

    /**
     * 获取密钥字段
     *
     * @return
     */
    public static String getKeyStr() {
        String time = getTime();
        String tiem1_5 = time.substring(0, 5);
        String time6_10 = time.substring(5, 10);
        String key = "8es9w98yw&^%eq98";
        System.out.println("时间戳1-5=" + tiem1_5 + "6-10=" + time6_10);
        return "&key=" + md5(tiem1_5 + key + time6_10) + "&time=" + time;
    }

    /**
     * 获取KEY
     *
     * @return
     */
    public static String getKey() {
        String time = getTime();
        String tiem1_5 = time.substring(0, 5);
        String time6_10 = time.substring(5, 10);
        String key = "8es9w98yw&^%eq98";
        System.out.println("时间戳1-5=" + tiem1_5 + "6-10=" + time6_10);
        return md5(tiem1_5 + key + time6_10);
    }


    /**
     * MD5加密
     *
     * @param string
     * @return
     */
    public static String md5(String string) {
        byte[] hash;
        try {
            hash = MessageDigest.getInstance("MD5").digest(string.getBytes("UTF-8"));
        } catch (NoSuchAlgorithmException e) {
            throw new RuntimeException("Huh, MD5 should be supported?", e);
        } catch (UnsupportedEncodingException e) {
            throw new RuntimeException("Huh, UTF-8 should be supported?", e);
        }

        StringBuilder hex = new StringBuilder(hash.length * 2);
        for (byte b : hash) {
            if ((b & 0xFF) < 0x10) hex.append("0");
            hex.append(Integer.toHexString(b & 0xFF));
        }
        return hex.toString();
    }

    /**
     * 获取时间的差值
     *
     * @param startTime 开始时间  10位时间戳
     * @param endTime   结束时间  10位时间戳
     * @return 小时数
     */
    public static String getTimePoor(String startTime, String endTime) {
        int timePoor = (int) (Long.valueOf(endTime) - Long.valueOf(startTime)) / 3600;
        return timePoor + "";
    }

    /**
     * 时间戳转日期字符串
     *
     * @param time 10位时间戳
     * @return
     */
    public static String timeToStr(long time) {
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");  // hh:mm:ss
        String date = sdf.format(new Date(time * 1000));
        return date;
    }

    public static String timeToStr1(long time) {
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd hh:mm");  // hh:mm:ss
        String date = sdf.format(new Date(time * 1000));
        return date;
    }

    /**
     * 日期字符串转时间戳
     *
     * @param user_time 日期
     * @return
     */

    public static String getTime(String user_time) {
        String re_time = null;
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
        Date d = null;
        try {
            d = sdf.parse(user_time);
        } catch (ParseException e) {
            e.printStackTrace();
        }

        long l = d.getTime();
        String str = String.valueOf(l);
        re_time = str.substring(0, 10);

        return re_time;
    }

    public static List<String> getEveryDay(String startTime, String endTime) {
        long startt = Long.valueOf(getTime(startTime));
        long endt = Long.valueOf(getTime(endTime));
        long poorT = endt - startt;
        int day = (int) poorT / (3600 * 24);
        SysPrintUtil.pt("日期计算为", startt + "==" + endt + "==" + poorT + "==" + day);
        List<String> list = new ArrayList<String>();
        list.add(startTime);
        for (int i = 1; i < day; i++) {
            SysPrintUtil.pt("日期计算为====", timeToStr(startt + 3600 * 24 * i));
            list.add(timeToStr(startt + 3600 * 24 * i));
        }
        list.add(endTime);
        return list;
    }

    public static String getCurrTime() {
        SimpleDateFormat formatter = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        Date curDate = new Date(System.currentTimeMillis());//获取当前时间
        return formatter.format(curDate);
    }
}
