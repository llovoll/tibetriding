package cn.ctibet.tibetriding.util;

import android.app.Activity;
import android.content.Context;
import android.util.DisplayMetrics;
import cn.ctibet.tibetriding.bean.ScreenSize;

/**
 * Created by Administrator on 2014/7/24.
 */
public class ScreenSizeUtil {
	private static DisplayMetrics dm;

	public static ScreenSize getScreenSize(Context context) {
		ScreenSize screenSize = new ScreenSize();
		dm = new DisplayMetrics();
		((Activity) context).getWindowManager().getDefaultDisplay()
				.getMetrics(dm);
		screenSize.screenW = dm.widthPixels;
		screenSize.screenH = dm.heightPixels;
		return screenSize;
	}
}
