package cn.ctibet.tibetriding.util;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface.OnCancelListener;

/**
 * Created by LONG on 14-1-13.
 */
public class DialogShowUtil {
	private Context context;
	private String content;
	private ProgressDialog progressDialog;

	public DialogShowUtil(Context context, String content) {
		this.context = context;
		this.content = content;
	}

	public void dialogShow() {
		progressDialog = new ProgressDialog(context);
		progressDialog.setMessage(content);
		progressDialog.setCancelable(true);
		progressDialog.show();
		progressDialog.setProgressStyle(ProgressDialog.STYLE_SPINNER);
	}

	public void dialogDismiss() {
		if (null != progressDialog) {
			progressDialog.dismiss();
			progressDialog = null;
		}
	}

	public void setOnCancelListener(OnCancelListener l) {
		if (null != progressDialog) {
			progressDialog.setOnCancelListener(l);
		}
	}
}
