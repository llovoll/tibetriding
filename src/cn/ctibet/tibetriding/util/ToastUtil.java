package cn.ctibet.tibetriding.util;

import android.content.Context;
import android.widget.Toast;

/**
 * Created by Administrator on 2014/11/26.
 */
public class ToastUtil {

    private  static  Toast mToast;

    public static void showToast(Context context, String msg, int duration) {
        // if (mToast != null) {
        // mToast.cancel();
        // }
        // mToast = Toast.makeText(context, msg, Toast.LENGTH_SHORT);
        if (null == mToast) {
            mToast = Toast.makeText(context, msg, duration);
        } else {
            mToast.setText(msg);
        }
        mToast.show();
    }
}
