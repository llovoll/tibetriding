package cn.ctibet.tibetriding.util;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * Created by LONG on 14-2-11.
 */
public class CheckFormatUtil {

    public static boolean checkEmail(String email) {// 验证邮箱的正则表达式

        String str = "^([a-zA-Z0-9_\\-\\.]+)@((\\[[0-9]{1,3}\\.[0-9]{1,3}\\.[0-9]{1,3}\\.)|(([a-zA-Z0-9\\-]+\\.)+))([a-zA-Z]{2,4}|[0-9]{1,3})(\\]?)$";
        Pattern p = Pattern.compile(str);
        Matcher m = p.matcher(email);
        return m.matches();
    }

    public static boolean checkPhone(String phone) {
        Pattern pattern = Pattern.compile("^(13|15|18)\\d{9}$");
        Matcher matcher = pattern.matcher(phone);

        if (matcher.matches()) {
            return true;
        }
        return false;
    }
}
