package cn.ctibet.tibetriding.util;

import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Matrix;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.Drawable;
import android.util.Base64;
import android.util.Log;

import java.io.*;
import java.util.ArrayList;
import java.util.List;

/**
 * TODO: 图像转换工具类
 */
public class BitmapTransformUtil {
    public static Drawable stringToDrawable(String string) {
        //将字符串转换成Bitmap类型
        Bitmap bitmap = null;
        try {
            byte[] bitmapArray;
            bitmapArray = Base64.decode(string, Base64.DEFAULT);
            bitmap = BitmapFactory.decodeByteArray(bitmapArray, 0, bitmapArray.length);
        } catch (Exception e) {
            e.printStackTrace();
        }
        BitmapDrawable bd = new BitmapDrawable(bitmap);
        return bd;
    }

    public static Bitmap base64ToBitmap(String s) {
        byte[] c = null;
        try {
            // 将得到的String字符串通过Base64转为字节数组
            c = Base64.decode(s, Base64.DEFAULT);
        } catch (Exception e) {
            Log.d("37==: ImageTransformUtil Exception", e.toString());
            return null;
        }

        // 根据byte数组创建Bitmap
        Bitmap bitmap = BitmapFactory.decodeByteArray(c, 0,
                c.length);
        return bitmap;
    }


    public static String DrawableToBase64(Drawable drawable) {
        BitmapDrawable bd = (BitmapDrawable) drawable;
        Bitmap bitmap = bd.getBitmap();
        //将Bitmap转换成字符串
        String string = null;
        ByteArrayOutputStream bStream = new ByteArrayOutputStream();
        bitmap.compress(Bitmap.CompressFormat.PNG, 80, bStream);
        byte[] bytes = bStream.toByteArray();
        string = Base64.encodeToString(bytes, Base64.DEFAULT);
        return string;
    }

    /**
     * 利用BASE64Encoder对图片进行base64转码将图片转为string
     *
     * @param imgFile 文件路径
     */
    public static String f_imageToBase64(String imgFile) {
        if (imgFile == null || imgFile == "") return "";
        File file = new File(imgFile);
        if (!file.exists()) {
            return "";
        }

        InputStream in = null;
        byte[] data = null;
        // 读取图片字节数组
        try {
            in = new FileInputStream(imgFile);
            data = new byte[in.available()];
            in.read(data);
//            data = getByte(in);  //可替换上面俩句
            in.close();
        } catch (IOException e) {
            e.printStackTrace();
        }
        // 返回Base64编码过的字节数组字符串
        String str = new String(Base64.encode(data, Base64.DEFAULT)); //null？！
        return str;
    }

    //法2
    public static String f_imageToStr(String imgFile) {
        if (imgFile == null || imgFile == "") return "";
        String uploadBuffer = null;
        try {
            FileInputStream fis = new FileInputStream(imgFile);
            ByteArrayOutputStream baos = new ByteArrayOutputStream();
            byte[] buffer = new byte[1024];
            int count = 0;
            while ((count = fis.read(buffer)) >= 0) {
                baos.write(buffer, 0, count);
            }
            uploadBuffer = new String(Base64.encode(baos.toByteArray(), Base64.DEFAULT));
        } catch (Exception e) {
            e.printStackTrace();
        }
        return uploadBuffer;
    }

    //法3

    /**
     * 转base64 大于某长度就压缩
     *
     * @param imgFilePath 文件路径
     * @param scale_width  限定最长边最大长度
     *
     * @return base64转码后
     */
    public static String getImageString(String imgFilePath, int scale_width) {
        String imageString = "";
        Bitmap mBitmap = null;
        try {
            mBitmap = BitmapFactory.decodeFile(imgFilePath);  //OOM-第10张图报

        } catch (OutOfMemoryError e) {
            System.gc();
            Log.d("Exception-getImageString", e.toString());
            return "";
        }


        if (mBitmap != null) {
            Matrix matrix = new Matrix();
            int mWidth = mBitmap.getWidth();
            int mHeight = mBitmap.getHeight();

            float scaleWidth = 1;
            if (mWidth > mHeight) {
                scaleWidth = (float) scale_width / mWidth;
            } else {
                scaleWidth = (float) scale_width / mHeight;
            }
//            float scaleHeight = (float) 150 / mHeight;
//            Log.i("scale", scaleWidth + "++++++++++++" + scaleHeight);
            Bitmap newBitmap = null;
            if (scaleWidth < 1) {
                matrix.postScale(scaleWidth, scaleWidth);
            } else {
                matrix.postScale(1, 1);
            }
            newBitmap = Bitmap.createBitmap(mBitmap, 0, 0, mBitmap.getWidth(), mBitmap.getHeight(), matrix, true);
            ByteArrayOutputStream out = new ByteArrayOutputStream();
            newBitmap.compress(Bitmap.CompressFormat.JPEG, 100, out);
            byte[] bytes = out.toByteArray();
            imageString = Base64.encodeToString(bytes, Base64.DEFAULT);
            System.out.println(imageString);
        }
        return imageString;
    }

    public static String bitmaptoString_scale(Bitmap mBitmap, int scale_width) {
        String imageString = "";
        if (mBitmap != null) {
            Matrix matrix = new Matrix();
            int mWidth = mBitmap.getWidth();
            int mHeight = mBitmap.getHeight();
            float scaleWidth = (float) scale_width / mWidth;
//            float scaleHeight = (float) 150 / mHeight;
//            Log.i("scale", scaleWidth + "++++++++++++" + scaleHeight);
            Bitmap newBitmap = null;
            if (scaleWidth < 1) {
                matrix.postScale(scaleWidth, scaleWidth);
            } else {
                matrix.postScale(1, 1);
            }
            newBitmap = Bitmap.createBitmap(mBitmap, 0, 0, mBitmap.getWidth(), mBitmap.getHeight(), matrix, true);
            ByteArrayOutputStream out = new ByteArrayOutputStream();
            newBitmap.compress(Bitmap.CompressFormat.JPEG, 100, out);
            byte[] bytes = out.toByteArray();
            imageString = Base64.encodeToString(bytes, Base64.DEFAULT);
            System.out.println(imageString);
        }
        return imageString;
    }

    public static String getImgStringCompress_Base64(String imgFilePath) {
        String imageString = "";
        Bitmap mBitmap = Utils.compressImage(imgFilePath);

        imageString = bitmaptoBase64(mBitmap);

        return imageString;
    }

    /**
     * 通过BASE64Decoder解码，并生成图片
     *
     * @param imgStr 解码后的string
     */
    public static boolean f_stringToImage(String imgStr, String imgFilePath) {
        // 对字节数组字符串进行Base64解码并生成图片
        if (imgStr == null)
            return false;
        try {
            // Base64解码
            byte[] b = Base64.decode(imgStr, Base64.DEFAULT);
            for (int i = 0; i < b.length; ++i) {
                if (b[i] < 0) {
                    b[i] += 256; // 调整异常数据
                }
            }
            // 生成jpeg图片
            OutputStream out = new FileOutputStream(imgFilePath);
            out.write(b);
            out.flush();
            out.close();
            return true;
        } catch (Exception e) {
            return false;
        }
    }


    public static byte[] getByte(InputStream in) {
        if (in == null) {
            return null;
        }
        int sumSize = 0;
        List<byte[]> totalBytes = new ArrayList<byte[]>();
        byte[] buffer = new byte[1024];
        int length = -1;
        try {
            while ((length = in.read(buffer)) != -1) {
                sumSize += length;
                byte[] tmp = new byte[length];
                System.arraycopy(buffer, 0, tmp, 0, length);
                totalBytes.add(tmp);
            }
            byte[] data = new byte[sumSize];
            int start = 0;
            for (byte[] tmp : totalBytes) {
                System.arraycopy(tmp, 0, data, start, tmp.length);
                start += tmp.length;
            }
            return data;
        } catch (IOException e) {
            e.printStackTrace();
        }
        return null;
    }

    // 将Bitmap转换成字符串
    public static String bitmaptoBase64(Bitmap bitmap) {
        String string = null;

        ByteArrayOutputStream bStream = new ByteArrayOutputStream();

        bitmap.compress(Bitmap.CompressFormat.PNG, 100, bStream);

        byte[] bytes = bStream.toByteArray();

        string = Base64.encodeToString(bytes, Base64.DEFAULT);

        return string;

    }
}
