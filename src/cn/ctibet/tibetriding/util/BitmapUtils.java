package cn.ctibet.tibetriding.util;

import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;
import android.graphics.PixelFormat;
import android.graphics.drawable.Drawable;
import android.os.Environment;
import android.os.StatFs;

import java.io.*;

/**
 * @author 张凯
 * @Copyright: nd.com (c) 2015 All rights reserved.
 * @ClassName: ${TYPE_NAME}
 * @Description: 图片处理工具累
 * @Package com.inwhoop.lz.util
 * @Date: 2015/01/07 17:25
 * @version: 1.0
 */
public class BitmapUtils {
    /**
     * 将bigmap转成byte[]
     *
     * @param bitmap 位图对象
     *
     * @return
     */
    public static byte[] getBitmapByte(Bitmap bitmap) {
        ByteArrayOutputStream out = new ByteArrayOutputStream();
        bitmap.compress(Bitmap.CompressFormat.JPEG, 100, out);
        try {
            out.flush();
            out.close();
        } catch (IOException e) {
            e.printStackTrace();
        }
        return out.toByteArray();
    }

    /**
     * 将byte[]转成bigmap
     *
     * @param temp byte[]
     *
     * @return
     */
    public static Bitmap getBitmapFromByte(byte[] temp) {
        if (temp != null) {
            Bitmap bitmap = BitmapFactory.decodeByteArray(temp, 0, temp.length);
            return bitmap;
        } else {
            return null;
        }
    }

    /**
     * 将drawable转为bitmap
     * @param drawable   drawable
     * @return
     */
    public static Bitmap drawableToBitmap(Drawable drawable) {
        int width = drawable.getIntrinsicWidth();
        int height = drawable.getIntrinsicHeight();
        Bitmap bitmap = Bitmap.createBitmap(width, height,
                drawable.getOpacity() != PixelFormat.OPAQUE ? Bitmap.Config.ARGB_8888
                        : Bitmap.Config.RGB_565);
        Canvas canvas = new Canvas(bitmap);
        drawable.setBounds(0, 0, width, height);
        drawable.draw(canvas);
        return bitmap;
    }

    private static int FREE_SD_SPACE_NEEDED_TO_CACHE = 1;
    private static int MB = 1024 * 1024;

    /**
     * 保存bitmap到sdcard
     * 注意：不要用线程，要使用请自己另开线程。在我要爆料时要依次序执行其他方法，使用线程会有问题
     * @param dir 文件的跟目录路径
     * @param bm
     */
    public static void saveBitmap(final Bitmap bm, final String dir, final String filename) {
//        new Thread(new Runnable() {
//            @Override
//            public void run() {
                if (bm == null) {
                    //LogUtil.e("saveBitmap，bitmap空");
                    return;
                }

                if (FREE_SD_SPACE_NEEDED_TO_CACHE > freeSpaceOnSd()) {
                    bm.recycle();
                    //LogUtil.e("Exception,内存超了。图片未保存成功：path：" + filename);
                    return;
                }

                File dirPath = new File(dir);
                if (!exists(dir))
                    dirPath.mkdirs();

                String thidDir = dir;
                if (!thidDir.endsWith(File.separator))
                    thidDir += File.separator;

                File f = new File(thidDir + filename);
                try {
                    f.createNewFile();
                    FileOutputStream out = new FileOutputStream(f);
                    bm.compress(Bitmap.CompressFormat.JPEG, 90, out);
                    out.flush();
                    out.close();
                   // LogUtil.i("saveBitmap，已经保存");
                } catch (FileNotFoundException e) {
                   // LogUtil.e("FileNotFoundException,图片未保存成功：path：" + filename);
                    e.printStackTrace();
                } catch (IOException e) {
                   // LogUtil.e("IOException,图片未保存成功：path：" + filename);
                    e.printStackTrace();
                }
//            }
//        }).start();
    }

    /**
     * 验证文件是否存在
     * @param url
     */
    public static boolean exists(String url) {
        File file = new File(url);
        return file.exists();
    }

    /**
     * 检测sdcard可用空间
     */
    public static int freeSpaceOnSd() {
        StatFs stat = new StatFs(Environment.getExternalStorageDirectory()
                .getPath());
        double sdFreeMB = ((double) stat.getAvailableBlocks() * (double) stat
                .getBlockSize()) / MB;
        return (int) sdFreeMB;
    }
}
