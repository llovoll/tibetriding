package cn.ctibet.tibetriding.util;

import java.util.ArrayList;
import java.util.List;

import android.text.TextUtils;
import cn.ctibet.tibetriding.bean.LLAEntity;

/**
 * LLA操作类
 * 
 * @author liuzhao
 * @since 2015-2-2
 */
public class LLAUtils {

	/**
	 * 解析轨迹数据
	 * 
	 * @param
	 *
	 * @return 轨迹列表
	 */
	public static List<LLAEntity> parseTrackset(String params) {
		List<LLAEntity> result = new ArrayList<LLAEntity>();

		if (!TextUtils.isEmpty(params)) {
			String[] dataArray = params.split("\\|");
			for (String str : dataArray) {
				result.add(new LLAEntity(Double.parseDouble(str.split(",")[0]),
						Double.parseDouble(str.split(",")[1]), Double
								.parseDouble(str.split(",")[2])));
			}
		}

		return result;
	}
    /**
     * 解析轨迹数据
     *
     * @param
     *
     * @return 轨迹列表  经度,纬度|
     */
    public static List<LLAEntity> parseTrackset1(String params) {
        List<LLAEntity> result = new ArrayList<LLAEntity>();

        if (!TextUtils.isEmpty(params)) {
            String[] dataArray = params.split("\\|");
            for (String str : dataArray) {
                result.add(new LLAEntity(Double.parseDouble(str.split(",")[0]),
                        Double.parseDouble(str.split(",")[1])));
            }
        }

        return result;
    }

	/**
	 * 解析单个LLA串，串结构如“经度,纬度,海拔”
	 * 
	 * @return LLAEntity对象
	 */
	public static LLAEntity parseLLAStr(String params) {
		LLAEntity result = new LLAEntity();

		if (!TextUtils.isEmpty(params)) {
			String[] dataArray = params.split(",");
			if (null != dataArray) {
				result.setLng(Double.parseDouble(dataArray[0]));
				result.setLat(Double.parseDouble(dataArray[1]));
				result.setAlt(Double.parseDouble(dataArray[2]));
			}
		}

		return result;
	}

	/**
	 * 封装轨迹数据
	 * 
	 * @param
	 *
	 * @return 轨迹数据
	 */
	public static String wrappedTrackset(List<LLAEntity> params) {
		StringBuilder result = new StringBuilder();

		if (null != params && !params.isEmpty()) {
			for (LLAEntity entity : params) {
				result.append("|");
				result.append(entity.toString());
			}

			result.delete(0, 1);
		}

		return result.toString();
	}

	/**
	 * 封装单个轨迹数据
	 * 
	 * @param params
	 *            LLAEntity对象
	 * @return 轨迹串，结构如“经度,纬度,海拔”
	 */
	public static String wrappedSingleTrack(LLAEntity params) {
		return null != params ? params.toString() : "";
	}
}
