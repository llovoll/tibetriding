package cn.ctibet.tibetriding.app;

import android.app.Activity;
import android.app.Application;
import cn.ctibet.tibetriding.util.UserInfoUtil;

import java.lang.ref.WeakReference;
import java.util.LinkedList;

public class ExitApplication extends Application {
    private static ExitApplication instance;


    public static ExitApplication getInstance() {
        if (instance == null) {
            instance = new ExitApplication();
        }
        return instance;
    }

    private LinkedList<WeakReference<Activity>> activityList = new LinkedList<WeakReference<Activity>>();

    public void addActivity(Activity activity) {
        activityList.add(new WeakReference<Activity>(activity));
    }

    public void exit() {
        System.out.println("---------activityList----------size--:" + activityList.size());
        for (WeakReference<Activity> actRef : activityList) {

            if (actRef.get() != null) {
                actRef.get().finish();
            } else {
            }
        }
        System.exit(0);

    }
}