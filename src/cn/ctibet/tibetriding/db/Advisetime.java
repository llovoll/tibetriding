package cn.ctibet.tibetriding.db;

import java.util.ArrayList;
import java.util.List;

import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import cn.ctibet.tibetriding.bean.AdvisetimeBean;

/**
 * “建议出行时间”表操作类
 * 
 * @author liuzhao
 * @since 2015-2-5
 */
public class Advisetime {
	private DatabaseDBhelper dbhelper = null;
	private SQLiteDatabase db;

	public Advisetime(Context context) {
		dbhelper = new DatabaseDBhelper(context);
	}

	/**
	 * 保存线路的建议出行时间
	 * 
	 * @param wayid
	 *            线路ID
	 * @param datas
	 *            线路的所有建议出行时间
	 */
	public void save(int wayid, List<AdvisetimeBean> datas) {
		db = dbhelper.getWritableDatabase();

		if (null != db && db.isOpen()) {
			db.beginTransaction();
			try {
				boolean existence = exist(db, wayid);

				if (existence) {
					// 若存在，则先删除所有建议出行时间
					delete(db, wayid);
				}

				// 添加
				for (AdvisetimeBean data : datas) {
					db.execSQL(
							"INSERT INTO advisetime (waytimeid,wayid,begintime,endtime,month,price,cutprice) VALUES (?,?,?,?,?,?,?)",
							new Object[] { data.Waytimeid, data.Wayid,
									data.Begintime, data.Endtime, data.Month,
									data.Price, data.Cutprice });
				}
				db.setTransactionSuccessful();
			} finally {
				db.endTransaction();
			}
		}
		if (null != db && db.isOpen()) {
			db.close();
		}
	}

	/**
	 * 获取线路的所有建议出行时间
	 * 
	 * @param wayid
	 *            线路ID
	 */
	public List<AdvisetimeBean> getAllAdvisetimes(int wayid) {
		List<AdvisetimeBean> result = new ArrayList<AdvisetimeBean>();
		db = dbhelper.getWritableDatabase();

		if (null != db && db.isOpen()) {
			Cursor cursor = db
					.rawQuery(
							"SELECT * FROM advisetime WHERE wayid=? ORDER BY waytimeid DESC",
							new String[] { String.valueOf(wayid) });

			AdvisetimeBean entity = null;
			while (cursor.moveToNext()) {
				entity = new AdvisetimeBean();
				entity.Waytimeid = cursor.getInt(cursor
						.getColumnIndex("waytimeid"));
				entity.Wayid = cursor.getInt(cursor.getColumnIndex("wayid"));
				entity.Begintime = cursor.getString(cursor
						.getColumnIndex("begintime"));
				entity.Endtime = cursor.getString(cursor
						.getColumnIndex("endtime"));
				entity.Month = cursor.getInt(cursor.getColumnIndex("month"));
				entity.Price = cursor.getFloat(cursor.getColumnIndex("price"));
				entity.Cutprice = cursor.getFloat(cursor
						.getColumnIndex("cutprice"));

				result.add(entity);
			}

			if (!cursor.isClosed()) {
				cursor.close();
			}
		}

		if (null != db && db.isOpen()) {
			db.close();
		}

		return result;

	}

	/**
	 * 删除线路的所有建议出行时间
	 * 
	 * @param db
	 *            SQLiteDatabase对象。注意：为了降低打开和关闭该对象是的开销，此对象需要在调用者中打开和关闭。
	 * @param wayid
	 *            线路ID
	 */
	private void delete(SQLiteDatabase db, int wayid) {
		if (null != db && db.isOpen()) {
			db.execSQL("DELETE FROM advisetime WHERE wayid=?",
					new Object[] { wayid });
		}
	}

	/**
	 * 线路是否存在建议出行时间
	 *
	 * @param db
	 *            SQLiteDatabase对象。注意：为了降低打开和关闭该对象是的开销，此对象需要在调用者中打开和关闭。
	 * @param wayid
	 *            线路ID
	 * @return 存在为true, 反之false
	 */
	private boolean exist(SQLiteDatabase db, int wayid) {
		boolean result = true;

		if (null != db && db.isOpen()) {
			Cursor cursor = db.rawQuery(
					"SELECT id FROM advisetime WHERE wayid=?",
					new String[] { String.valueOf(wayid) });
			if (null != cursor) {
				result = (cursor.getCount() > 0);

				if (!cursor.isClosed()) {
					cursor.close();
				}
			}
		}

		return result;
	}
}
