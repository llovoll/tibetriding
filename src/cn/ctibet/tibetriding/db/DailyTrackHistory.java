package cn.ctibet.tibetriding.db;

import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import cn.ctibet.tibetriding.bean.DailyTrackHistoryBean;
import cn.ctibet.tibetriding.bean.EnumRidingStatus;

/**
 * 日常锻炼操作类
 *
 * @author liuzhao
 * @since 2015-2-3
 */
public class DailyTrackHistory {
    private DatabaseDBhelper dbhelper = null;
    private SQLiteDatabase db;

    public DailyTrackHistory(Context context) {
        dbhelper = new DatabaseDBhelper(context);
    }

    /**
     * 处理状态
     *
     * @param
     */
    public void handleStatus(DailyTrackHistoryBean data) {
        db = dbhelper.getWritableDatabase();

        if (null != db && db.isOpen()) {
            db.execSQL(
                    "UPDATE dailytrackhistory SET smallimg=?,trackset=?,begintime=?,endtime=?,casttime=?,totalmileage=?,status=? WHERE trackhistoryid=?",
                    new Object[]{data.Smallimg, data.Trackset, data.Begintime, data.Endtime,
                            data.Casttime, data.Totalmileage, data.Status,
                            data.Trackhistoryid});
        }

        if (null != db && db.isOpen()) {
            db.close();
        }
    }

    /**
     * 日常锻炼是否存在
     *
     * @param trackhistoryid 历史轨迹ID
     * @return 存在为true，反之false
     */
    public boolean exist(int trackhistoryid) {
        boolean result = true;
        db = dbhelper.getWritableDatabase();

        if (null != db && db.isOpen()) {
            Cursor cursor = db.rawQuery(
                    "SELECT id FROM dailytrackhistory WHERE trackhistoryid=?",
                    new String[]{String.valueOf(trackhistoryid)});
            if (null != cursor) {
                result = (cursor.getCount() > 0);

                if (!cursor.isClosed()) {
                    cursor.close();
                }
            }
        }

        if (null != db && db.isOpen()) {
            db.close();
        }

        return result;
    }

    /**
     * 保存数据
     *
     * @param data 历史轨迹数据
     */
    public void save(DailyTrackHistoryBean data) {
        db = dbhelper.getWritableDatabase();

        if (null != db && db.isOpen()) {
            // =======判断历史轨迹是否已存在========
            boolean existence = exist(db, data.Trackhistoryid);
            if (existence) {
                // 修改
                db.execSQL(
                        "UPDATE dailytrackhistory SET dailytrackhistoryid=?,userid=?,smallimg=?,biketype=?,trackset=?,status=?,totalmileage=?,casttime=?,begintime=?,endtime=? WHERE trackhistoryid=?",
                        new Object[]{data.Dailytrackhistoryid, data.Userid,
                                data.Smallimg, data.Biketype, data.Trackset,
                                data.Status, data.Totalmileage, data.Casttime,
                                data.Begintime, data.Endtime,
                                data.Trackhistoryid});
            } else {
                // 添加
                db.execSQL(
                        "INSERT INTO dailytrackhistory (dailytrackhistoryid,trackhistoryid,userid,smallimg,biketype,trackset,status,totalmileage,casttime,begintime,endtime,addtime) VALUES (?,?,?,?,?,?,?,?,?,?,?,?)",
                        new Object[]{data.Dailytrackhistoryid,
                                data.Trackhistoryid, data.Userid,
                                data.Smallimg, data.Biketype, data.Trackset,
                                data.Status, data.Totalmileage, data.Casttime,
                                data.Begintime, data.Endtime, data.Addtime});
            }
        }
        if (null != db && db.isOpen()) {
            db.close();
        }
    }

    /**
     * 删除日常锻炼数据
     *
     * @param trackhistoryid 历史轨迹ID
     */
    public void delete(int trackhistoryid) {
        db = dbhelper.getWritableDatabase();

        if (null != db && db.isOpen()) {
            db.execSQL("DELETE FROM dailytrackhistory WHERE trackhistoryid=?",
                    new Object[]{trackhistoryid});
        }
        if (null != db && db.isOpen()) {
            db.close();
        }
    }

    /**
     * 获取轨迹数据，结构如“经度1,纬度1,海拔1|经度2,纬度2,海拔2|经度3,纬度3,海拔3...”
     *
     * @param trackhistoryid 历史轨迹ID
     * @return 轨迹数据，结构如“经度1,纬度1,海拔1|经度2,纬度2,海拔2|经度3,纬度3,海拔3...”
     */
    public String getTrackSet(int trackhistoryid) {
        String result = "";
        db = dbhelper.getWritableDatabase();

        if (null != db && db.isOpen()) {
            Cursor cursor = db
                    .rawQuery(
                            "SELECT trackset FROM dailytrackhistory WHERE trackhistoryid=?",
                            new String[]{String.valueOf(trackhistoryid)});
            if (null != cursor) {
                if (cursor.moveToFirst()) {
                    result = cursor.getString(0);
                }

                if (!cursor.isClosed()) {
                    cursor.close();
                }
            }
        }

        if (null != db && db.isOpen()) {
            db.close();
        }

        return result;
    }

    /**
     * 获取历史轨迹
     *
     * @param trackhistoryid 历史轨迹ID
     * @return 历史轨迹数据
     */
    public DailyTrackHistoryBean getDailyTrackHistoryBean(
            String trackhistoryid,String userId) {
        DailyTrackHistoryBean result = new DailyTrackHistoryBean();
        db = dbhelper.getWritableDatabase();

        if (null != db && db.isOpen()) {
            Cursor cursor = db.rawQuery(
                    "SELECT * FROM dailytrackhistory WHERE trackhistoryid=? and userid=?",
                    new String[]{trackhistoryid,userId});

            if (null != cursor) {
                while (cursor.moveToFirst()) {
                    result.Dailytrackhistoryid = cursor.getInt(cursor
                            .getColumnIndex("dailytrackhistoryid"));
                    result.Trackhistoryid = cursor.getString(cursor
                            .getColumnIndex("trackhistoryid"));
                    result.Userid = cursor.getString(cursor
                            .getColumnIndex("userid"));
                    result.Smallimg = cursor.getString(cursor
                            .getColumnIndex("smallimg"));
                    result.Biketype = cursor.getInt(cursor
                            .getColumnIndex("biketype"));
                    result.Trackset = cursor.getString(cursor
                            .getColumnIndex("trackset"));
                    result.Status = cursor.getInt(cursor
                            .getColumnIndex("status"));
                    result.Totalmileage = cursor.getFloat(cursor
                            .getColumnIndex("totalmileage"));
                    result.Casttime = cursor.getString(cursor
                            .getColumnIndex("casttime"));
                    result.Begintime = cursor.getString(cursor
                            .getColumnIndex("begintime"));
                    result.Endtime = cursor.getString(cursor
                            .getColumnIndex("endtime"));
                    result.Addtime = cursor.getString(cursor
                            .getColumnIndex("addtime"));
                }

                if (!cursor.isClosed()) {
                    cursor.close();
                }
            }
        }

        if (null != db && db.isOpen()) {
            db.close();
        }

        return result;
    }

    /**
     * 获取用户“暂停”后的日常锻炼。注意：为了降低开销，此方法返回的结果实体中有效数据仅为“历史轨迹ID”和“骑行类型”。
     *
     * @param userid 用户ID
     * @return DailyTrackHistorySecond对象
     */
    public DailyTrackHistoryBean getPausedDailyTrackHistory(String userid) {
        DailyTrackHistoryBean result = new DailyTrackHistoryBean();
        db = dbhelper.getWritableDatabase();

        if (null != db && db.isOpen()) {
            Cursor cursor = db
                    .rawQuery(
                            "SELECT trackhistoryid,biketype,dailytrackhistoryid FROM dailytrackhistory WHERE userid=? AND status=?",
                            new String[]{
                                    String.valueOf(userid),
                                    String.valueOf(EnumRidingStatus.PAUSED
                                            .getValue())});

            if (null != cursor) {
                while (cursor.moveToNext()) {
                    result.Trackhistoryid = cursor.getString(cursor
                            .getColumnIndex("trackhistoryid"));
                    result.Biketype = cursor.getInt(cursor
                            .getColumnIndex("biketype"));
                    result.Dailytrackhistoryid = cursor.getInt(cursor
                            .getColumnIndex("dailytrackhistoryid"));
                }

                if (!cursor.isClosed()) {
                    cursor.close();
                }
            }
        }

        if (null != db && db.isOpen()) {
            db.close();
        }

        return result;
    }

    /**
     * 日常锻炼是否存在。
     *
     * @param db             SQLiteDatabase对象。注意：为了降低打开和关闭该对象是的开销，此对象需要在调用者中打开和关闭。
     * @param trackhistoryid 历史轨迹ID
     * @return 存在为true, 反之false
     */
    private boolean exist(SQLiteDatabase db, String trackhistoryid) {
        boolean result = true;

        if (null != db && db.isOpen()) {
            Cursor cursor = db.rawQuery(
                    "SELECT id FROM dailytrackhistory WHERE trackhistoryid=?",
                    new String[]{String.valueOf(trackhistoryid)});
            if (null != cursor) {
                result = (cursor.getCount() > 0);

                if (!cursor.isClosed()) {
                    cursor.close();
                }
            }
        }

        return result;
    }
}
