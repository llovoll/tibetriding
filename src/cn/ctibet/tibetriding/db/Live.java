package cn.ctibet.tibetriding.db;

import java.util.ArrayList;
import java.util.List;

import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import cn.ctibet.tibetriding.bean.LiveBean;

/**
 * "直播内容表"操作类
 * 
 * @author liuzhao
 * @since 2015-2-2
 */
public class Live {
	private DatabaseDBhelper dbhelper = null;
	private SQLiteDatabase db;

	public Live(Context context) {
		dbhelper = new DatabaseDBhelper(context);
	}

	/**
	 * 
	 * 保存数据
	 * 
	 * @param data
	 *            直播数据
	 */
	public void save(LiveBean data) {
		db = dbhelper.getWritableDatabase();

		if (null != db && db.isOpen()) {
			db.execSQL(
					"INSERT INTO live (liveid,trackhistoryid,livelat,livelng) VALUES (?,?,?,?)",
					new Object[] { data.getLiveid(), data.getTrackhistoryid(),
							data.getLivelat(), data.getLivelng() });
		}
		if (null != db && db.isOpen()) {
			db.close();
		}
	}

	/**
	 * 获取某条历史轨迹的所有直播数据
	 * 
	 * @param trackhistoryid
	 *            历史轨迹ID
	 * @return 所有直播数据
	 */
	public List<LiveBean> getAllLives(int trackhistoryid) {
		List<LiveBean> result = new ArrayList<LiveBean>();
		db = dbhelper.getWritableDatabase();

		if (null != db && db.isOpen()) {
			Cursor cursor = db
					.rawQuery(
							"SELECT liveid,trackhistoryid,livelat,livelng FROM live WHERE trackhistoryid=? ORDER BY liveid ASC",
							new String[] { String.valueOf(trackhistoryid) });
			if (null != cursor) {
				LiveBean entity = null;
				while (cursor.moveToNext()) {
					entity = new LiveBean();
					entity.setLiveid(cursor.getInt(cursor
							.getColumnIndex("liveid")));
					entity.setTrackhistoryid(cursor.getInt(cursor
							.getColumnIndex("trackhistoryid")));
					entity.setLivelat(cursor.getString(cursor
							.getColumnIndex("livelat")));
					entity.setLivelng(cursor.getString(cursor
							.getColumnIndex("livelng")));

					result.add(entity);
				}

				if (!cursor.isClosed()) {
					cursor.close();
				}
			}
		}
		if (null != db && db.isOpen()) {
			db.close();
		}

		return result;
	}
}
