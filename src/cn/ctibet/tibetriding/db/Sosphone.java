package cn.ctibet.tibetriding.db;

import java.util.ArrayList;
import java.util.List;

import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import cn.ctibet.tibetriding.bean.EnumIsDeleted;
import cn.ctibet.tibetriding.bean.SosphoneBean;

/**
 * “求助电话”表操作类
 * 
 * @author liuzhao
 * @since 2015-2-7
 */
public class Sosphone {
	private DatabaseDBhelper dbhelper = null;
	private SQLiteDatabase db;

	public Sosphone(Context context) {
		dbhelper = new DatabaseDBhelper(context);
	}

	/**
	 * 保存求助电话
	 * 
	 * @param data
	 *            求助电话
	 */
	public void save(SosphoneBean data) {
		db = dbhelper.getWritableDatabase();

		if (null != db && db.isOpen()) {
			// =======判断求助电话是否已存在========
			boolean existence = exist(db, data.sosphoneid);
			if (existence) {
				// 修改
				db.execSQL(
						"UPDATE sosphone SET wayid,name,telephone,isdelete,addtime WHERE sosphoneid=?",
						new Object[] { data.wayid, data.name, data.telephone,
								data.isdelete, data.addtime, data.sosphoneid });
			} else {
				// 添加
				db.execSQL(
						"INSERT INTO sosphone (sosphoneid,wayid,name,telephone,isdelete,addtime) VALUES (?,?,?,?,?,?)",
						new Object[] { data.sosphoneid, data.wayid, data.name,
								data.telephone, data.isdelete, data.addtime });
			}
		}
		if (null != db && db.isOpen()) {
			db.close();
		}
	}

	/**
	 * 获取某条线路的所有求助电话
	 * 
	 * @param wayid
	 *            线路ID
	 * @return 求助电话
	 */
	public List<SosphoneBean> getAllSosphones(int wayid) {
		List<SosphoneBean> result = new ArrayList<SosphoneBean>();
		db = dbhelper.getWritableDatabase();

		if (null != db && db.isOpen()) {
			Cursor cursor = db
					.rawQuery(
							"SELECT * FROM sosphone WHERE isdelete=? AND wayid=? ORDER BY sosphoneid DESC",
							new String[] {
									String.valueOf(EnumIsDeleted.UNDELETED
											.getValue()), String.valueOf(wayid) });

			SosphoneBean entity = null;
			while (cursor.moveToNext()) {
				entity = new SosphoneBean();
				entity.sosphoneid = cursor.getInt(cursor
						.getColumnIndex("sosphoneid"));
				entity.wayid = cursor.getInt(cursor.getColumnIndex("wayid"));
				entity.name = cursor.getString(cursor.getColumnIndex("name"));
				entity.telephone = cursor.getString(cursor
						.getColumnIndex("telephone"));
				entity.isdelete = cursor.getInt(cursor
						.getColumnIndex("isdelete"));
				entity.addtime = cursor.getString(cursor
						.getColumnIndex("addtime"));

				result.add(entity);
			}

			if (!cursor.isClosed()) {
				cursor.close();
			}
		}

		if (null != db && db.isOpen()) {
			db.close();
		}

		return result;
	}

	/**
	 * 
	 * 求助电话是否存在
	 * 
	 * @param db
	 *            SQLiteDatabase对象。注意：为了降低打开和关闭该对象是的开销，此对象需要在调用者中打开和关闭。
	 * @param wayid
	 *            求助电话ID
	 * @return 存在为true, 反之false
	 */
	private boolean exist(SQLiteDatabase db, int sosphoneid) {
		boolean result = true;

		if (null != db && db.isOpen()) {
			Cursor cursor = db.rawQuery(
					"SELECT id FROM sosphone WHERE sosphoneid=?",
					new String[] { String.valueOf(sosphoneid) });
			if (null != cursor) {
				result = (cursor.getCount() > 0);

				if (!cursor.isClosed()) {
					cursor.close();
				}
			}
		}

		return result;
	}
}
