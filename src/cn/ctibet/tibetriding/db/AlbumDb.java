package cn.ctibet.tibetriding.db;

import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import cn.ctibet.tibetriding.bean.CyclingImgBean;
import cn.ctibet.tibetriding.bean.UserInfo;
import cn.ctibet.tibetriding.util.UserInfoUtil;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Administrator on 2015/1/20.
 */
public class AlbumDb {
    private DatabaseDBhelper dbhelper = null;
    private UserInfo userInfo;
    private SQLiteDatabase db;

    public AlbumDb(Context context) {
        dbhelper = new DatabaseDBhelper(context);
        userInfo = UserInfoUtil.getUserInfo(context);
    }

    /**
     * 保存骑行相册信息
     *
     * @param bean
     */
    public void saveAlbum(CyclingImgBean bean) {
        db = dbhelper.getWritableDatabase();

        if (null != db && db.isOpen()) {
            db.execSQL(
                    "INSERT INTO albumdb (location,haiba,wendu,time,imgname,userid) VALUES (?,?,?,?,?,?)",
                    new Object[]{bean.location, bean.haiba, bean.wendu, bean.time, bean.imgname,
                            userInfo.userId});
        }
        if (null != db && db.isOpen()) {
            db.close();
        }
    }

    /**
     * 获取骑行相册
     *
     * @param userId 用户ID
     * @return
     */
    public List<CyclingImgBean> getAlbumData(String userId) {
        db = dbhelper.getReadableDatabase();
        List<CyclingImgBean> list = new ArrayList<CyclingImgBean>();
        CyclingImgBean bean;
        Cursor cursor = db.rawQuery("select *  from  albumdb where userid=? ORDER by id desc",
                new String[]{userId});
        if (cursor.moveToFirst()) {
            for (int i = 0; i < cursor.getCount(); i++) {
                bean = new CyclingImgBean();
                bean.id = cursor.getInt(cursor.getColumnIndex("id"));
                bean.location = cursor.getString(cursor.getColumnIndex("location"));
                bean.haiba = cursor.getString(cursor.getColumnIndex("haiba"));
                bean.wendu = cursor.getString(cursor.getColumnIndex("wendu"));
                bean.time = cursor.getString(cursor.getColumnIndex("time"));
                bean.imgname = cursor.getString(cursor.getColumnIndex("imgname"));
                list.add(bean);
                cursor.moveToNext();
            }
        }
        cursor.close();
        db.close();
        return list;
    }

    /**
     * 获取骑行相册时间分组数据ID
     *
     * @param userId 用户ID
     * @return
     */
    public List<CyclingImgBean> getAlbumGroupIdData(String userId) {
        db = dbhelper.getReadableDatabase();
        List<CyclingImgBean> list = new ArrayList<CyclingImgBean>();
        CyclingImgBean bean;
        Cursor cursor = db.rawQuery("select *  from  albumdb where userid=? group by time",
                new String[]{userId});
        if (cursor.moveToFirst()) {
            for (int i = 0; i < cursor.getCount(); i++) {
                bean = new CyclingImgBean();
                bean.id = cursor.getInt(cursor.getColumnIndex("id"));
                bean.location = cursor.getString(cursor.getColumnIndex("location"));
                bean.haiba = cursor.getString(cursor.getColumnIndex("haiba"));
                bean.wendu = cursor.getString(cursor.getColumnIndex("wendu"));
                bean.time = cursor.getString(cursor.getColumnIndex("time"));
                bean.imgname = cursor.getString(cursor.getColumnIndex("imgname"));
                list.add(bean);
                cursor.moveToNext();
            }
        }
        cursor.close();
        db.close();
        return list;
    }

    /**
     * 获取骑行相册时间分组数据
     *
     * @param userId 用户ID
     * @return
     */
    public List<CyclingImgBean> getAlbumGroupData(String userId, String time) {
        db = dbhelper.getReadableDatabase();
        List<CyclingImgBean> list = new ArrayList<CyclingImgBean>();
        CyclingImgBean bean;
        Cursor cursor = db.rawQuery("select *  from  albumdb where userid=? AND time=? ORDER by id desc",
                new String[]{userId, time});
        if (cursor.moveToFirst()) {
            for (int i = 0; i < cursor.getCount(); i++) {
                bean = new CyclingImgBean();
                bean.id = cursor.getInt(cursor.getColumnIndex("id"));
                bean.location = cursor.getString(cursor.getColumnIndex("location"));
                bean.haiba = cursor.getString(cursor.getColumnIndex("haiba"));
                bean.wendu = cursor.getString(cursor.getColumnIndex("wendu"));
                bean.time = cursor.getString(cursor.getColumnIndex("time"));
                bean.imgname = cursor.getString(cursor.getColumnIndex("imgname"));
                list.add(bean);
                cursor.moveToNext();
            }
        }
        cursor.close();
        db.close();
        return list;
    }

    /**
     * 根据文件名称删除该条数据
     * @param imgName   图片名称
     * @param userId  用户ID
     */
    public void delAlbumItemData(String imgName, String userId) {
        db = dbhelper.getWritableDatabase();

        if (null != db && db.isOpen()) {
            db.execSQL("delete from albumdb where imgname=? and userid=?", new Object[]{imgName, userId});
        }
        if (null != db && db.isOpen()) {
            db.close();
        }
    }
}
