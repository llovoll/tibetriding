package cn.ctibet.tibetriding.db;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

/**
 * @author dyong199046@163.com 代勇
 * @version V1.0
 * @Project: DDCJ
 * @Title: DatabaseDBhelper.java
 * @Package com.inwhoop.ddcj.db
 * @Description: TODO
 * @date 2014-10-28 下午7:08:47
 * @Copyright: 2014 呐喊信息技术 All rights reserved.
 */
public class DatabaseDBhelper extends SQLiteOpenHelper {

	private static final String name = "xzqx"; // 数据库名称
	private static final int version = 1; // 数据库版本

	public DatabaseDBhelper(Context context) {
		super(context, name, null, version);
	}

	@Override
	public void onCreate(SQLiteDatabase db) {
		db.execSQL("CREATE TABLE IF NOT EXISTS albumdb (id integer primary key autoincrement,location varchar(20)"
				+ ",haiba varchar(10),wendu varchar(10),time varchar(10),imgname varchar(20),userid varchar(10))");
		db.execSQL("CREATE TABLE IF NOT EXISTS dailytrackhistory (id integer primary key autoincrement,dailytrackhistoryid int,trackhistoryid int"
				+ ",userid int,smallimg varchar(250),biketype int,trackset text,status int,totalmileage float,casttime varchar(255),begintime char(15),endtime char(15),addtime char(15))");
		db.execSQL("CREATE TABLE IF NOT EXISTS live (id integer primary key autoincrement,liveid int,trackhistoryid int"
				+ ",livelat char(20),livelng char(20))");
		db.execSQL("CREATE TABLE IF NOT EXISTS way (id integer primary key autoincrement,wayid int,wayname char(20)"
				+ ",wayimg char(50),waydesc char(255),maxday int,minday int,addtime char(15),waytype int,isdelete int)");
		db.execSQL("CREATE TABLE IF NOT EXISTS advisetime (id integer primary key autoincrement,waytimeid int,wayid int"
				+ ",begintime char(15),endtime char(15),month int,price float,cutprice float)");
		db.execSQL("CREATE TABLE IF NOT EXISTS route (id integer primary key autoincrement,routeid int,wayid int,beginprovince varchar(10),begincity varchar(10),beginroutename varchar(20)"
				+ ",beginlng char(20),beginlat char(20),daynum char(20),parentid int,addtime char(15),endlng char(20),endlat char(20),trackset text,endprovince varchar(10),endcity varchar(10),endroutename varchar(20))");
		db.execSQL("CREATE TABLE IF NOT EXISTS routedesc (id integer primary key autoincrement,routedescid int,routeid int"
				+ ",title char(50),content text,addtime char(15))");
		db.execSQL("CREATE TABLE IF NOT EXISTS company (id integer primary key autoincrement,companyid int,companylng char(20)"
				+ ",companylat char(20),companyname char(50),companytype char(10),companytel char(11),companyaddr char(100),companydesc char(255),addtime char(15),isdelete int,province varchar(20),city varchar(20),xian varchar(20))");
		db.execSQL("CREATE TABLE IF NOT EXISTS sosphone (id integer primary key autoincrement,sosphoneid int,wayid int,name char(20),telephone char(20),isdelete int,addtime char(15))");
	}

	@Override
	public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
		db.execSQL("DROP TABLE IF EXISTS albumdb");
		db.execSQL("DROP TABLE IF EXISTS dailytrackhistory");
		db.execSQL("DROP TABLE IF EXISTS live");
		db.execSQL("DROP TABLE IF EXISTS way");
		db.execSQL("DROP TABLE IF EXISTS advisetime");
		db.execSQL("DROP TABLE IF EXISTS route");
		db.execSQL("DROP TABLE IF EXISTS routedesc");
		db.execSQL("DROP TABLE IF EXISTS company");
		db.execSQL("DROP TABLE IF EXISTS sosphone");
		onCreate(db);
	}

}
