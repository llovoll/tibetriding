package cn.ctibet.tibetriding.db;

import java.util.ArrayList;
import java.util.List;

import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import cn.ctibet.tibetriding.bean.DbRouteBean;

/**
 * “行程表”操作类
 * 
 * @author liuzhao
 * @since 2015-2-5
 */
public class DbRoute {
	private DatabaseDBhelper dbhelper = null;
	private SQLiteDatabase db;

	public DbRoute(Context context) {
		dbhelper = new DatabaseDBhelper(context);
	}

	/**
	 * 保存线路
	 * 
	 * @param data
	 *            线路
	 */
	public void save(DbRouteBean data) {
		db = dbhelper.getWritableDatabase();

		if (null != db && db.isOpen()) {
			// =======判断历史轨迹是否已存在========
			boolean existence = exist(db, data.Routeid);
			if (existence) {
				// 修改
				db.execSQL(
						"UPDATE route SET wayid=?,beginprovince=?,begincity=?,beginroutename=?,beginlng=?,beginlat=?,daynum=?,parentid=?,addtime=?,endlng=?,endlat=?,trackset=?,endprovince=?,endcity=?,endroutename=? WHERE routeid=?",
						new Object[] { data.Wayid, data.Beginprovince,
								data.Begincity, data.Beginroutename,
								data.Beginlng, data.Beginlat, data.Daynum,
								data.Parentid, data.Addtime, data.Endlng,
								data.Endlat, data.Trackset, data.Endprovince,
								data.Endcity, data.Endroutename, data.Routeid });
			} else {
				// 添加
				db.execSQL(
						"INSERT INTO route (routeid,wayid,beginprovince,begincity,beginroutename,beginlng,beginlat,daynum,parentid,addtime,endlng,endlat,trackset,endprovince,endcity,endroutename) VALUES (?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)",
						new Object[] { data.Routeid, data.Wayid,
								data.Beginprovince, data.Begincity,
								data.Beginroutename, data.Beginlng,
								data.Beginlat, data.Daynum, data.Parentid,
								data.Addtime, data.Endlng, data.Endlat,
								data.Trackset, data.Endprovince, data.Endcity,
								data.Endroutename });
			}
		}
		if (null != db && db.isOpen()) {
			db.close();
		}
	}

	/**
	 * 根据上级行程获取下级行程
	 * 
	 * @param wayid
	 *            线路ID
	 * @param parentid
	 *            上级行程ID，若是第一项则为0
	 * @return 行程数据
	 */
	public DbRouteBean getDbRouteBean(int wayid, int parentid) {
		DbRouteBean result = new DbRouteBean();
		db = dbhelper.getWritableDatabase();

		if (null != db && db.isOpen()) {
			Cursor cursor = db.rawQuery(
					"SELECT * FROM route WHERE wayid=? AND parentid=?",
					new String[] { String.valueOf(wayid),
							String.valueOf(parentid) });
			if (null != cursor) {
				while (cursor.moveToFirst()) {
					result.Routeid = cursor.getInt(cursor
							.getColumnIndex("routeid"));
					result.Wayid = cursor
							.getInt(cursor.getColumnIndex("wayid"));
					result.Beginprovince = cursor.getString(cursor
							.getColumnIndex("beginprovince"));
					result.Begincity = cursor.getString(cursor
							.getColumnIndex("begincity"));
					result.Beginroutename = cursor.getString(cursor
							.getColumnIndex("beginroutename"));
					result.Beginlng = cursor.getString(cursor
							.getColumnIndex("beginlng"));
					result.Beginlat = cursor.getString(cursor
							.getColumnIndex("beginlat"));
					result.Daynum = cursor.getString(cursor
							.getColumnIndex("daynum"));
					result.Parentid = cursor.getInt(cursor
							.getColumnIndex("parentid"));
					result.Addtime = cursor.getString(cursor
							.getColumnIndex("addtime"));
					result.Endlng = cursor.getString(cursor
							.getColumnIndex("endlng"));
					result.Endlat = cursor.getString(cursor
							.getColumnIndex("endlat"));
					result.Trackset = cursor.getString(cursor
							.getColumnIndex("trackset"));
					result.Endprovince = cursor.getString(cursor
							.getColumnIndex("endprovince"));
					result.Endcity = cursor.getString(cursor
							.getColumnIndex("endcity"));
					result.Endroutename = cursor.getString(cursor
							.getColumnIndex("endroutename"));
				}

				if (!cursor.isClosed()) {
					cursor.close();
				}
			}
		}

		if (null != db && db.isOpen()) {
			db.close();
		}

		return result;
	}

	/**
	 * 获取更新线路路书的时间
	 * 
	 * @return 时间串，没有获取到则返回空。
	 */
	public String getRouteUpdatetime(int wayid) {
		String result = "";
		db = dbhelper.getWritableDatabase();

		if (null != db && db.isOpen()) {
			Cursor cursor = db
					.rawQuery(
							"SELECT addtime FROM route WHERE wayid=? ORDER BY addtime DESC LIMIT 0,1",
							new String[] { String.valueOf(wayid) });
			if (null != cursor) {
				while (cursor.moveToFirst()) {
					result = cursor.getString(0);
				}

				if (!cursor.isClosed()) {
					cursor.close();
				}
			}
		}

		if (null != db && db.isOpen()) {
			db.close();
		}

		return result;
	}

	/**
	 * 获取某条线路的所有行程数据
	 * 
	 * @param wayid
	 *            线路ID
	 * @return 行程数据
	 */
	public List<DbRouteBean> getAllRoutesByWayid(int wayid) {
		List<DbRouteBean> result = new ArrayList<DbRouteBean>();
		db = dbhelper.getWritableDatabase();

		if (null != db && db.isOpen()) {
			addDbRouteBeanToList(db, result, wayid, 0);
		}

		if (null != db && db.isOpen()) {
			db.close();
		}

		return result;
	}

	/**
	 * 行程是否存在。
	 *
	 * @param db
	 *            SQLiteDatabase对象。注意：为了降低打开和关闭该对象是的开销，此对象需要在调用者中打开和关闭。
	 * @param routeid
	 *            行程ID
	 * @return 存在为true, 反之false
	 */
	private boolean exist(SQLiteDatabase db, int routeid) {
		boolean result = true;

		if (null != db && db.isOpen()) {
			Cursor cursor = db.rawQuery("SELECT id FROM route WHERE routeid=?",
					new String[] { String.valueOf(routeid) });
			if (null != cursor) {
				result = (cursor.getCount() > 0);

				if (!cursor.isClosed()) {
					cursor.close();
				}
			}
		}

		return result;
	}

	/**
	 * 填充行程列表
	 * 
	 * @param db
	 *            SQLiteDatabase对象。注意：为了降低打开和关闭该对象是的开销，此对象需要在调用者中打开和关闭。
	 * @param list
	 *            填充行程数据的列表
	 * @param wayid
	 *            线路ID
	 * @param parentid
	 *            上级行程的ID，若是第一项则为0
	 */
	private void addDbRouteBeanToList(SQLiteDatabase db,
			List<DbRouteBean> list, int wayid, int parentid) {
		DbRouteBean entity = null;

		if (null != db && db.isOpen()) {
			Cursor cursor = db.rawQuery(
					"SELECT * FROM route WHERE wayid=? AND parentid=?",
					new String[] { String.valueOf(wayid),
							String.valueOf(parentid) });
			if (null != cursor) {
				while (cursor.moveToFirst()) {
					entity = new DbRouteBean();
					entity.Routeid = cursor.getInt(cursor
							.getColumnIndex("routeid"));
					entity.Wayid = cursor
							.getInt(cursor.getColumnIndex("wayid"));
					entity.Beginprovince = cursor.getString(cursor
							.getColumnIndex("beginprovince"));
					entity.Begincity = cursor.getString(cursor
							.getColumnIndex("begincity"));
					entity.Beginroutename = cursor.getString(cursor
							.getColumnIndex("beginroutename"));
					entity.Beginlng = cursor.getString(cursor
							.getColumnIndex("beginlng"));
					entity.Beginlat = cursor.getString(cursor
							.getColumnIndex("beginlat"));
					entity.Daynum = cursor.getString(cursor
							.getColumnIndex("daynum"));
					entity.Parentid = cursor.getInt(cursor
							.getColumnIndex("parentid"));
					entity.Addtime = cursor.getString(cursor
							.getColumnIndex("addtime"));
					entity.Endlng = cursor.getString(cursor
							.getColumnIndex("endlng"));
					entity.Endlat = cursor.getString(cursor
							.getColumnIndex("endlat"));
					entity.Trackset = cursor.getString(cursor
							.getColumnIndex("trackset"));
					entity.Endprovince = cursor.getString(cursor
							.getColumnIndex("endprovince"));
					entity.Endcity = cursor.getString(cursor
							.getColumnIndex("endcity"));
					entity.Endroutename = cursor.getString(cursor
							.getColumnIndex("endroutename"));
				}

				if (!cursor.isClosed()) {
					cursor.close();
				}
			}
		}

		if (null != entity) {
			// 递归填充
			list.add(entity);
			addDbRouteBeanToList(db, list, wayid, entity.Routeid);
		}
	}
}
