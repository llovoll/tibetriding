package cn.ctibet.tibetriding.db;

import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import cn.ctibet.tibetriding.bean.RoutedescBean;

/**
 * “行程描述”表操作类
 * 
 * @author liuzhao
 * @since 2015-2-6
 */
public class Routedesc {
	private DatabaseDBhelper dbhelper = null;
	private SQLiteDatabase db;

	public Routedesc(Context context) {
		dbhelper = new DatabaseDBhelper(context);
	}

	/**
	 * 保存行程描述
	 * 
	 * @param routeid
	 *            行程ID
	 * @param data
	 *            行程数据
	 */
	public void save(RoutedescBean data) {
		db = dbhelper.getWritableDatabase();

		if (null != db && db.isOpen()) {
			db.beginTransaction();
			try {
				boolean existence = exist(db, data.Routeid);

				if (existence) {
					// 若存在，则先删除原来的行程描述
					delete(db, data.Routeid);
				}

				db.execSQL(
						"INSERT INTO routedesc (routedescid,routeid,title,content,addtime) VALUES (?,?,?,?,?)",
						new Object[] { data.Routedescid, data.Routeid,
								data.Title, data.Content, data.Addtime });
				db.setTransactionSuccessful();
			} finally {
				db.endTransaction();
			}
		}

		if (null != db && db.isOpen()) {
			db.close();
		}
	}

	/**
	 * 获取行程描述
	 * 
	 * @param routeid
	 *            行程ID
	 * @return 行程描述
	 */
	public RoutedescBean getRoutedescBean(int routeid) {
		RoutedescBean result = new RoutedescBean();
		db = dbhelper.getWritableDatabase();

		if (null != db && db.isOpen()) {
			Cursor cursor = db.rawQuery(
					"SELECT * FROM routedesc WHERE routeid=?",
					new String[] { String.valueOf(routeid) });

			while (cursor.moveToFirst()) {
				result.Routedescid = cursor.getInt(cursor
						.getColumnIndex("routedescid"));
				result.Routeid = cursor
						.getInt(cursor.getColumnIndex("routeid"));
				result.Title = cursor.getString(cursor.getColumnIndex("title"));
				result.Content = cursor.getString(cursor
						.getColumnIndex("content"));
				result.Addtime = cursor.getString(cursor
						.getColumnIndex("addtime"));
			}

			if (!cursor.isClosed()) {
				cursor.close();
			}
		}

		if (null != db && db.isOpen()) {
			db.close();
		}

		return result;
	}

	/**
	 * 删除行程的描述信息
	 * 
	 * @param db
	 *            SQLiteDatabase对象。注意：为了降低打开和关闭该对象是的开销，此对象需要在调用者中打开和关闭。
	 * @param routeid
	 *            行程ID
	 */
	private void delete(SQLiteDatabase db, int routeid) {
		if (null != db && db.isOpen()) {
			db.execSQL("DELETE FROM routedesc WHERE routeid=?",
					new Object[] { routeid });
		}
	}

	/**
	 * 行程是否存在行程描述
	 *
	 * @param db
	 *            SQLiteDatabase对象。注意：为了降低打开和关闭该对象是的开销，此对象需要在调用者中打开和关闭。
	 * @param routeid
	 *            行程ID
	 * @return 存在为true, 反之false
	 */
	private boolean exist(SQLiteDatabase db, int routeid) {
		boolean result = true;

		if (null != db && db.isOpen()) {
			Cursor cursor = db.rawQuery(
					"SELECT id FROM routedesc WHERE routeid=?",
					new String[] { String.valueOf(routeid) });
			if (null != cursor) {
				result = (cursor.getCount() > 0);

				if (!cursor.isClosed()) {
					cursor.close();
				}
			}
		}

		return result;
	}
}
