package cn.ctibet.tibetriding.db;

import java.util.ArrayList;
import java.util.List;

import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import cn.ctibet.tibetriding.bean.EnumIsDeleted;
import cn.ctibet.tibetriding.bean.EnumWaytype;
import cn.ctibet.tibetriding.bean.WayBean;

/**
 * “线路”表操作类
 * 
 * @author liuzhao
 * @since 2015-2-5
 */
public class Way {
	private DatabaseDBhelper dbhelper = null;
	private SQLiteDatabase db;

	public Way(Context context) {
		dbhelper = new DatabaseDBhelper(context);
	}

	/**
	 * 保存线路
	 * 
	 * @param data
	 *            线路
	 */
	public void save(WayBean data) {
		db = dbhelper.getWritableDatabase();

		if (null != db && db.isOpen()) {
			// =======判断历史轨迹是否已存在========
			boolean existence = exist(db, data.Wayid);
			if (existence) {
				// 修改
				db.execSQL(
						"UPDATE way SET wayname=?,wayimg=?,waydesc=?,maxday=?,minday=?,addtime=?,waytype=?,isdelete=? WHERE wayid=?",
						new Object[] { data.Wayname, data.Wayimg, data.Waydesc,
								data.Maxday, data.Minday, data.Addtime,
								data.Waytype, data.Isdelete, data.Wayid });
			} else {
				// 添加
				db.execSQL(
						"INSERT INTO way (wayid,wayname,wayimg,waydesc,maxday,minday,addtime,waytype,isdelete) VALUES (?,?,?,?,?,?,?,?,?)",
						new Object[] { data.Wayid, data.Wayname, data.Wayimg,
								data.Waydesc, data.Maxday, data.Minday,
								data.Addtime, data.Waytype, data.Isdelete });
			}
		}
		if (null != db && db.isOpen()) {
			db.close();
		}
	}

	/**
	 * 获取更新骑行线路的时间
	 * 
	 * @return 时间串，没有获取到则返回空。
	 */
	public String getRidingLineUpdatetime() {
		String result = "";
		db = dbhelper.getWritableDatabase();

		if (null != db && db.isOpen()) {
			Cursor cursor = db
					.rawQuery(
							"SELECT addtime FROM way WHERE waytype=? ORDER BY addtime DESC LIMIT 0,1",
							new String[] { String
									.valueOf(EnumWaytype.LINE_RIDING.getValue()) });
			if (null != cursor) {
				while (cursor.moveToFirst()) {
					result = cursor.getString(0);
				}

				if (!cursor.isClosed()) {
					cursor.close();
				}
			}
		}

		if (null != db && db.isOpen()) {
			db.close();
		}

		return result;
	}

	/**
	 * 获取所有缓存的骑行线路
	 * 
	 * @return 线路列表
	 */
	public List<WayBean> getAllRidingLines() {
		List<WayBean> result = new ArrayList<WayBean>();
		db = dbhelper.getWritableDatabase();

		if (null != db && db.isOpen()) {
			Cursor cursor = db
					.rawQuery(
							"SELECT * FROM way WHERE isdelete=? AND waytype=? ORDER BY wayid DESC",
							new String[] {
									String.valueOf(EnumIsDeleted.UNDELETED
											.getValue()),
									String.valueOf(EnumWaytype.LINE_RIDING
											.getValue()) });

			WayBean entity = null;
			while (cursor.moveToNext()) {
				entity = new WayBean();
				entity.Addtime = cursor.getString(cursor
						.getColumnIndex("addtime"));
				entity.Isdelete = cursor.getInt(cursor
						.getColumnIndex("isdelete"));
				entity.Maxday = cursor.getInt(cursor.getColumnIndex("maxday"));
				entity.Minday = cursor.getInt(cursor.getColumnIndex("minday"));
				entity.Waydesc = cursor.getString(cursor
						.getColumnIndex("waydesc"));
				entity.Wayid = cursor.getInt(cursor.getColumnIndex("wayid"));
				entity.Wayimg = cursor.getString(cursor
						.getColumnIndex("wayimg"));
				entity.Wayname = cursor.getString(cursor
						.getColumnIndex("wayname"));
				entity.Waytype = cursor
						.getInt(cursor.getColumnIndex("waytype"));

				result.add(entity);
			}

			if (!cursor.isClosed()) {
				cursor.close();
			}
		}

		if (null != db && db.isOpen()) {
			db.close();
		}

		return result;
	}

	/**
	 * 线路是否存在。
	 *
	 * @param db
	 *            SQLiteDatabase对象。注意：为了降低打开和关闭该对象是的开销，此对象需要在调用者中打开和关闭。
	 * @param wayid
	 *            线路ID
	 * @return 存在为true, 反之false
	 */
	private boolean exist(SQLiteDatabase db, int wayid) {
		boolean result = true;

		if (null != db && db.isOpen()) {
			Cursor cursor = db.rawQuery("SELECT id FROM way WHERE wayid=?",
					new String[] { String.valueOf(wayid) });
			if (null != cursor) {
				result = (cursor.getCount() > 0);

				if (!cursor.isClosed()) {
					cursor.close();
				}
			}
		}

		return result;
	}
}
