package cn.ctibet.tibetriding.db;

import java.util.ArrayList;
import java.util.List;

import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import cn.ctibet.tibetriding.bean.CompanyBean;

/**
 * “商家”表操作类
 * 
 * @author liuzhao
 * @since 2015-2-6
 */
public class Company {
	private DatabaseDBhelper dbhelper = null;
	private SQLiteDatabase db;

	public Company(Context context) {
		dbhelper = new DatabaseDBhelper(context);
	}

	/**
	 * 保存商家
	 * 
	 * @param data
	 *            商家
	 */
	public void save(CompanyBean data) {
		db = dbhelper.getWritableDatabase();

		if (null != db && db.isOpen()) {
			// =======判断商家是否已存在========
			boolean existence = exist(db, data.companyid);
			if (existence) {
				// 修改
				db.execSQL(
						"UPDATE company SET companylng=?,companylat=?,companyname=?,companytype=?,companytel=?,companyaddr=?,companydesc=?,addtime=?,isdelete=?,province=?,city=?,xian=? WHERE companyid=?",
						new Object[] { data.companylng, data.companylat,
								data.companyname, data.companytype,
								data.companytel, data.companyaddr,
								data.companydesc, data.addtime, data.isdelete,
								data.province, data.city, data.xian,
								data.companyid });
			} else {
				// 添加
				db.execSQL(
						"INSERT INTO company (companyid,companylng,companylat,companyname,companytype,companytel,companyaddr,companydesc,addtime,isdelete,province,city,xian) VALUES (?,?,?,?,?,?,?,?,?,?,?,?,?)",
						new Object[] { data.companyid, data.companylng,
								data.companylat, data.companyname,
								data.companytype, data.companytel,
								data.companyaddr, data.companydesc,
								data.addtime, data.isdelete, data.province,
								data.city, data.xian });
			}
		}
		if (null != db && db.isOpen()) {
			db.close();
		}
	}

	/**
	 * 获取商家
	 * 
	 * @param province
	 *            省
	 * @param city
	 *            市
	 * @param xian
	 *            县
	 * @return 商家列表
	 */
	public List<CompanyBean> getCompanies(String province, String city,
			String xian) {
		List<CompanyBean> result = new ArrayList<CompanyBean>();
		db = dbhelper.getWritableDatabase();

		if (null != db && db.isOpen()) {
			Cursor cursor = db
					.rawQuery(
							"SELECT * FROM company WHERE province=? AND city=? AND xian=? ORDER BY companyid DESC",
							new String[] { province, city, xian });
			if (null != cursor) {
				CompanyBean entity = null;
				while (cursor.moveToNext()) {
					entity = new CompanyBean();
					entity.companyid = cursor.getInt(cursor
							.getColumnIndex("companyid"));
					entity.companylng = cursor.getString(cursor
							.getColumnIndex("companylng"));
					entity.companylat = cursor.getString(cursor
							.getColumnIndex("companylat"));
					entity.companyname = cursor.getString(cursor
							.getColumnIndex("companyname"));
					entity.companytype = cursor.getString(cursor
							.getColumnIndex("companytype"));
					entity.companytel = cursor.getString(cursor
							.getColumnIndex("companytel"));
					entity.companyaddr = cursor.getString(cursor
							.getColumnIndex("companyaddr"));
					entity.companydesc = cursor.getString(cursor
							.getColumnIndex("companydesc"));
					entity.addtime = cursor.getString(cursor
							.getColumnIndex("addtime"));
					entity.isdelete = cursor.getInt(cursor
							.getColumnIndex("isdelete"));
					entity.province = cursor.getString(cursor
							.getColumnIndex("province"));
					entity.city = cursor.getString(cursor
							.getColumnIndex("city"));
					entity.xian = cursor.getString(cursor
							.getColumnIndex("xian"));

					result.add(entity);
				}

				if (!cursor.isClosed()) {
					cursor.close();
				}
			}
		}

		if (null != db && db.isOpen()) {
			db.close();
		}

		return result;
	}

	/**
	 * 商家是否存在。
	 *
	 * @param db
	 *            SQLiteDatabase对象。注意：为了降低打开和关闭该对象是的开销，此对象需要在调用者中打开和关闭。
	 * @param companyid
	 *            商家ID
	 * @return 存在为true, 反之false
	 */
	private boolean exist(SQLiteDatabase db, int companyid) {
		boolean result = true;

		if (null != db && db.isOpen()) {
			Cursor cursor = db.rawQuery(
					"SELECT id FROM company WHERE companyid=?",
					new String[] { String.valueOf(companyid) });
			if (null != cursor) {
				result = (cursor.getCount() > 0);

				if (!cursor.isClosed()) {
					cursor.close();
				}
			}
		}

		return result;
	}
}
